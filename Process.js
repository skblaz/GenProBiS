// This file contains functions for data processing.
// main algorithms are implemented here, functions from here are called within main.js file server.


//main 3 functions to implement in order to provide frontend with sufficient data.

var zlib = require('zlib');
var exec = require('child_process').exec;
var readline = require('readline');
var fs = require('fs');
var http = require('http');
var stream = require('stream');
var fex = require('file-exists');

module.exports.initfiles = function(protein, chain, callback){

    //initDataFiles(protein,chain);

    var tmpSNP1 = 'data/SNP_DATA_PIPELINE/FinalResults/SNP/'+protein+".txt"; //'data/PDB_SNP_sur
    var finalJSON1 = new Object(); //json container of snp info within PI.

    try{
	
	var instreamSNP1 = fs.createReadStream(tmpSNP1);
	instreamSNP1.on('error',function(err){

	    console.log("File non existing..");
	    callback("ENDREQUEST");
	});
    	var outstreamSNP1 = new stream;
    	var rlSNP1 = readline.createInterface(instreamSNP1, outstreamSNP1);
    	var tmpSNPlist1 = [];
    	//var tmpSNPnames1 = [];
    	var tmpSNPnames2 = [];
	var uniqSNP = [];
	var uniqSurf = [];

    	rlSNP1.on('line', function(line3) {
	    
    	    var snpEntries3 = line3.split(/\s+/);
    	    //console.log(snpEntries);
    	    if (snpEntries3[5] == null){
    		console.log("SNPs not found for this entry.");

    	    }else if (snpEntries3[5] == chain){
    		//tukej primerja SNPcandidates z nekim stolpcem in pol vidmo a je snp notr in pol vrzemo ta lokus v json encodani obliki al neki..
		
		tmpSNPlist1.push(snpEntries3[6]);
		var tmp = snpEntries3[4]+"_"+snpEntries3[3]+"_"+snpEntries3[1];
		var tmp2 = snpEntries3[0]+"_"+snpEntries3[1].replace("_","|")+"_"+snpEntries3[2]+"_"+snpEntries3[3]+"_"+snpEntries3[4]+"_"+snpEntries3[5]+"_"+snpEntries3[6]+"_"+snpEntries3[7];		
		//console.log(tmp2)
		tmpSNPnames2.push(tmp2);
    	    }else{}
    	});
	
    	rlSNP1.on('uncaughtException', function (exception) {
    	    // handle or ignore error
    	});
	
    	rlSNP1.on('close', function(){


	    var uniqSNP = tmpSNPlist1.reduce(function(a,b){
		if (a.indexOf(b) < 0 ) a.push(b);
		return a;
	    },[]);
	    
    	    //finalJSON1.snploci = uniqSNP; //loci for intersection
    	    //finalJSON1.surface = uniqSurf; //calculated surface points
    	    //finalJSON1.snpnames = tmpSNPnames1; //names for labels
    	    finalJSON1.snpdata = tmpSNPnames2; //all data
	    finalJSON1.piSNP = []; //all data
	    //console.log(finalJSON1);
	    callback(finalJSON1);


    	});
	
    }catch(err){console.log("SNP not yet loaded..");}


}

module.exports.readData = function(outfile, callback){

    var fs       = require('fs');
    var zlib     = require('zlib');
    var readline = require('readline');

    var lineReader = readline.createInterface({
	input: fs.createReadStream(outfile).on('error',function(err){

	    console.log("No file..")
	    callback("backup")
	}).pipe(zlib.createGunzip()).on('error', function(err){
	    console.log("This doesn't work!")
	    callback("backup")
	})
    });

    var wholejson = "";
    
    lineReader.on('line', (line) => {
	
	wholejson += line;
	
    });

    lineReader.on('close', () => {

	callback(wholejson)
	
    });
    
    lineReader.on('error', (line) => {

	console.log("Couldn't read the file")
	callback("backup")
	
    });
      
}

module.exports.readPDB = function(pdbfile){
    
    var file = fs.readFileSync(pdbfile, "utf8");
    return file;

}

module.exports.readLIG = function(pdbfile){
    
    var file = fs.readFileSync(pdbfile, "utf8");
    return file;

}


module.exports.readBuffer = function(bfile){


    var zlib = require('zlib');

    zlib.gunzip(bfile, function(err, result) {
	if(err) return console.error(err);

	console.log(result);
    });
    
    var file = fs.readFileSync(bfile, "utf8");
    return file;

}

module.exports.getUniSeq = function(query){
    // this is the test algorithm

    function get_uniPDBMapping(query1){

	var obj = JSON.parse(fs.readFileSync('data/SNP_DATA_PIPELINE/FinalResults/PDB_UniMapping.json', 'utf8'));

	return obj[query1]
	
    }


    var options = {
    host: 'www.uniprot.org',
    path: '/uniprot/'+get_uniPDBMapping(query)+'.txt'
    }

    var request = http.request(options, function (res) {
	var data = '';
	var PDBseq = "";
	res.on('data', function (chunk) {
            data += chunk;
	});
	res.on('end', function () {

	    var sequence = "";
	    var range = [];
	    var rcount = false;
	    data.split("\n").forEach(function(entry){

		var splits = entry.match(/\S+/g);
		if (splits != null){
		    if (splits[0] == "DR"){
			if (splits[2].replace(";","").toLowerCase() == query){
			    if(splits.length == 7){
				range = (splits[6].substring(4,splits[6].length).replace(".","").split("-"))
			    }
			}
		    }else if (rcount == true){
			sequence += entry;
		    }else if (splits[0] == "SQ" && rcount == false){
			rcount = true;
		    }else{}
		}
	    });

	    sequence = sequence.replace("//","").match(/\S+/g).join("");

	    var aacount = 0;
	    var PDBseq = "";
	    sequence.split("").forEach(function(aa){
		aacount ++;
		if (aacount >= parseInt(range[0]) && aacount <= parseInt(range[1])){
		    PDBseq += aa;
		}
	    });	    	    	    
	    	    	    
	});
    });
    request.on('error', function (e) {
	console.log(e.message);
    });
    request.end();

}


function CalculateProperties(protein, chain){


    var file1 = 'data/PDB_SNP_surface'+'/tmp_'+protein+'_'+chain+'.pdb';
    var tmpSNP1 = 'data/PDB_SNP_surface'+'/snp_'+protein+'_'+chain+'.snp';
    var PDBname1 = "data/PDB_SNP_surface/"+protein+"_"+chain+".pdb";

    if (fex(file1)){

	if (fex(tmpSNP1)){

	    if (fex(PDBname1)){

		finally_calculate(protein, chain);
	    }

	}

    }
}

function finally_calculate(protein, chain){

   
    var tmpSNP1 = 'data/SNP_DATA_PIPELINE/FinalResults/SNP/'+protein+".txt"; //'data/PDB_SNP_sur
    var finalJSON1 = new Object(); //json container of snp info within PI.

	    try{
		var instreamSNP1 = fs.createReadStream(tmpSNP1);
    		var outstreamSNP1 = new stream;
    		var rlSNP1 = readline.createInterface(instreamSNP1, outstreamSNP1);
    		var tmpSNPlist1 = [];
    		//var tmpSNPnames1 = [];
    		var tmpSNPnames2 = [];
		var uniqSNP = [];
		var uniqSurf = [];

    		rlSNP1.on('line', function(line3) {
		    
    		    var snpEntries3 = line3.split(/\s+/);
    		    //console.log(snpEntries);
    		    if (snpEntries3[5] == null){
    			console.log("SNPs not found for this entry.");

    		    }else if (snpEntries3[5] == chain){
    			//tukej primerja SNPcandidates z nekim stolpcem in pol vidmo a je snp notr in pol vrzemo ta lokus v json encodani obliki al neki..
			
			tmpSNPlist1.push(snpEntries3[6]);
			var tmp = snpEntries3[4]+"_"+snpEntries3[3]+"_"+snpEntries3[1];			    
			var tmp2 = snpEntries3[0]+"_"+snpEntries3[1].replace("_","|")+"_"+snpEntries3[2]+"_"+snpEntries3[3]+"_"+snpEntries3[4]+"_"+snpEntries3[5]+"_"+snpEntries3[6]+"_"+snpEntries3[7];

			//console.log(tmp2)
			tmpSNPnames2.push(tmp2);
    		    }
    		});
		
    		rlSNP1.on('uncaughtException', function (exception) {
    		    // handle or ignore error
    		});
		
    		rlSNP1.on('close', function(){
		    
    		    finalJSON1.snploci = uniqSNP; //loci for intersection
    		    finalJSON1.surface = uniqSurf; //calculated surface points
    		    //finalJSON1.snpnames = tmpSNPnames1; //names for labels
    		    finalJSON1.snpdata = tmpSNPnames2; //all data
		    return finalJSON1;

    		});
	    }catch(err){console.log("SNP not yet loaded..");}
	
}


var combine = function(a, size) {
    var fn = function(n, src, got, all) {
        if (n == 0) {
            if (got.length > 0) {
                all[all.length] = got;
            }
            return;
        }
        for (var j = 0; j < src.length; j++) {
            fn(n - 1, src.slice(j + 1), got.concat([src[j]]), all);
        }
        return;
    }
    var all = [];

    fn(size, a, [], all);
    
    all.push(a);
    return all;
}

function initDatafiles(protein, chain){

    //definitions..
    var url = "http://insilab.org/api/predb?pdbid="+protein+"&chain="+chain;
    var file = 'data/PDB_SNP_surface'+'/grid_'+protein+'_'+chain+'.pdb';
    var tmp = 'data/PDB_SNP_surface'+'/tmp_'+protein+'_'+chain+'.pdb';
    var gridfile = 'data/grid_files'+'/grid_'+protein+'_'+chain+'.pdb';
    var tmpSNP = 'data/PDB_SNP_surface'+'/snp_'+protein+'_'+chain+'.snp'; 
    var urlSNP = "http://insilab.org/api/mutations?pdbid="+protein;
    var PDBname = "data/PDB_SNP_surface/"+protein+"_"+chain+".pdb";

    //pdb file.


    if (fileExists(PDBname) != true){
	exec("curl https://files.rcsb.org/view/"+protein+".pdb"+" > "+PDBname);
	console.log("file generated."+PDBname);
    }


    http.get(url, function(res){
    	var body = '';

    	res.on('data', function(chunk){
            body += chunk;
    	});
	
    	res.on('end', function(){
            var gridDATA = JSON.parse(body);
    	    //console.log(gridDATA.grid);
    	    fs.writeFile(tmp, gridDATA.grid);
	    fs.writeFile(gridfile, gridDATA.grid);
	    console.log("wrote PDB file..");
	    getSNPjson(urlSNP, tmpSNP);

	    //call calculate function..
	    console.log("calculation commencing..");
	    CalculateProperties(protein,chain);
	    
    	});
    }).on('error', function(e){
    	console.log("Got an error: ", e);
	
    });

    console.log("finished initiation.");


}


//tole bo verjetno treba od drugod!
function getSNPjson(urlSNP, tmpSNP){

    http.get(urlSNP, function(res){
	
    	var body = '';
    	res.on('data', function(chunk){
            body += chunk;
    	});

    	res.on('end', function(){
            var gridDATA = JSON.parse(body);
    	    //sleepFor(400);
    	    fs.writeFile(tmpSNP, gridDATA.snps);
	    
    	});
    }).on('error', function(e){
    	console.log("Got an error: ", e);
	
    });

}

function fileExists(filePath){
    try
    {
        return fs.statSync(filePath).isFile();
    }
    catch (err)
    {
        return false;
    }
}

function puts(error, stdout, stderr) { console.log(stdout) }


function intersect(a, b) {
    var t;
    if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
    return a.filter(function (e) {
        if (b.indexOf(e) !== -1) return true;
    });
}

function sleepFor( sleepDuration ){
    var now = new Date().getTime();
    while(new Date().getTime() < now + sleepDuration){ /* do nothing */ } 
}

module.exports.extractUniPDBlist = function(output, acc){
    console.log("Entering processing function.");
    var PDBids = [];
    var lines = output.split("\n");
    lines.forEach(function(cLine) {
	var chunks = cLine.split("\t");
	if (chunks[0] == acc){
	    //console.log(chunks[1]);
	    PDBids.push(chunks[1]);
	}
    });
    //console.log(PDBids);
    return PDBids; 
}

