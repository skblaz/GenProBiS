// const fs = require('fs');
// const zlib = require('zlib');
// const readline = require('readline');

// const gzFileInput = fs.createReadStream('data/lig_files/lig_1efv_B.json.gz');
// const gunzip = zlib.createGunzip();

// var lineCount = 0;
// var linex = ""
// readline.createInterface({
//     input: gunzip,terminal : false,
// }).on('line', function(line) {
//     lineCount++;
//     linex += line;
// }).on('close', function() {
//     console.log(linex)
// });

// gzFileInput.on('data', function(data) {

//     gunzip.write(data);
// });
// gzFileInput.on('end', function() {
//     gunzip.end();
// });


const fs       = require('fs');
const zlib     = require('zlib');
const readline = require('readline');

var lineReader = readline.createInterface({
   terminal : false,
  input: fs.createReadStream('test.txt.gz').pipe(zlib.createGunzip())
});

var n = 0;
lineReader.on('line', function (line)  {
  n += 1
  console.log("line: " + n);
  console.log(line);
});
