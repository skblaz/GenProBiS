
var zlib = require('zlib'); 
var express = require("express");
const bodyParser = require('body-parser')
var app = express();
var router = express.Router();
// var path = __dirname + '/views/'; // Under views there is website data.c
var exec = require('child_process').exec;
var http = require('http');
var readline = require('readline');
var fs = require('fs');
var main_calls = 0;
var hostname = "tyr.cmm.ki.si";
var line_line = require('line-by-line');
// app.use(express.compress());

var enableCORS = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, *');

    if ('OPTIONS' == req.method) {
        res.send(200);
    } else {
        next();
    };
};

app.use(enableCORS);

main_calls=0;
jobs = {};

app.use('/', express.static(__dirname + '/public'));

//app.use(express.static(__dirname + '/public'));

//type of parser - basic is OK.
app.use(bodyParser.urlencoded({ extended: true }));


// handle requests, main!
app.post('/get_data', function (req, res) {
    main_calls++;
    console.log(jobs);
    if (main_calls < 6){
	var PDB_proc = req.param('pdbid');
	console.log("Currently processing PDB ID: "+PDB_proc);
	if (PDB_proc.length == 5){	   
	    var protein = PDB_proc.substring(0, 4).toLowerCase();
	    var chain = PDB_proc.substring(4, 5).toUpperCase();
	    var PDBname = protein+"_"+chain;

	    var outfileSNP1 = "data/PDB_SNP_MAPPING_ALL/JSON/"+protein+chain+".json.gz";

	    if (!jobs.hasOwnProperty(PDBname)){
	    	jobs[PDBname] = false;

	    }
	    
	    //first look into the database..
	    
	    try {
		
		require('./Process.js').readData(outfileSNP1,function(datax){
		    // separate validation for surfaces, this is fast.
		    
		    // standard way!

		    if (datax == "backup"){
			require('./Process.js').initfiles(protein,chain,function(datax){
			    console.log("this is backup loop");
			    res.send(datax);
			    res.end()			    
			});

		    }else{
			//console.log(datax)
			console.log("OK input..")
			res.send(datax);
			res.end();
			main_calls=0;
			delete jobs[PDBname];

		    }
		    //clean(PDBname);		   
		    
		});
				
	    } catch (e) {

		require('./Process.js').initfiles(protein,chain,function(datax){

		    res.send(JSON.stringify(datax));
		    res.end()
		    

		});
		
	    }	    

	}else{
	    res.send(null);
	    main_calls=0;
	    res.end();
	}
	
    }else{
	res.send("BUSY");
	main_calls=0;
	res.end('Server is currently busy, please come again later!');
    }
    // this way one can easily maintain a database!
});

app.on('uncaughtException', function (exception) {
    console.log("Reading files not complete, this PDB doesn't have sufficient SNP data.");
});

// search by accessions
app.post('/get_global', function (req, resG) {
    
    var gAcc = req.param('globalID');
    var basePATH = "data/SNP_DATA_PIPELINE/FinalResults/WholeBase.txt";
    try{

	if (gAcc.startsWith("rs") || gAcc.startsWith("COSM") || gAcc.startsWith("ENSVV") || gAcc.startsWith("vcZ")  ){

	    var results = [];
	    
    	    var obj = JSON.parse(fs.readFileSync('data/SNP_DATA_PIPELINE/FinalResults/MAP_INDEX/PDB_SNPMapping.json','utf8'));

	    obj[gAcc].forEach(function(entry){
		
		results.push([entry.split("_")[0],entry.split("_")[1],entry.split("_")[2]+"_"+entry.split("_")[3]]);
		
	    });

	    console.log("Index used - rs!");
    	    resG.send(results);
    	    resG.end();

	}else{

	    var results = [];
	    
    	    var obj = JSON.parse(fs.readFileSync('data/SNP_DATA_PIPELINE/FinalResults/MAP_INDEX/PDB_UniGenMapping.json','utf8'));

	    obj[gAcc].forEach(function(entry){
		
		results.push([entry.split("_")[0],entry.split("_")[1],entry.split("_")[2]+"_"+entry.split("_")[3]]);
		
	    });

	    console.log("Index used!");
    	    resG.send(results);
    	    resG.end();
	    
	}
	
    }catch(err){
	
	
	var searchfol = "data/SNP_DATA_PIPELINE/FinalResults/searchfiles";
	console.log("Preprocessing entry: "+ gAcc);
	var totfiles = 0;   
	var filenames = [];
	for (var k = 0;k<7;k++){
            var peh =  k<10 ? "0"+k : k
            filenames.push(searchfol+"/BASE"+peh);
	}
	//var filenames = ["data/SNP_DATA_PIPELINE/FinalResults/WholeBase.txt"];
	var allfiles = filenames.length;

	// gre cez celo bazo in generira gumbke glede na hite.
	var results = [];
	console.log(gAcc)
	readfiles (gAcc,filenames, function(clen, fnum){

	    totfiles += fnum
	    clen.forEach(function(cl){
		results.push(cl);
	    });

	    if (totfiles == allfiles){
		try{
		    results = unique(results);
		}catch(err){

		    results = [];
		}

		if (results != []){
		    
		    if (results.length > 100){
			
			resG.send(results.slice(0,99));
			resG.end();
			
		    }else{
			
			resG.send(results);
			resG.end();
		    }
		}else{
		    console.log("No reuslts found..")
		    resG.end();
		}

	    }
	});
	
    }
    //Odpre za branje wholebase fajl, če je vnos 2 enak iskanemu genu, potem vzame polji 7 in 8, ter ju združi z _ in doda v list, ki bo prek jsona poslan.
    
});
// INDEX
app.get('/get_index', function (req, resI) {

    try{
   console.log("Generating index..");
   var basePATH = "data/SNP_DATA_PIPELINE/FinalResults/indexdb.txt";
   // gre cez celo bazo in generira gumbke glede na hite.
   // var keyOBJ = new Object();
   var keys = new Object();
   var rd = readline.createInterface({
	input: fs.createReadStream(basePATH),
	output: process.stdout,
       terminal: false
   });

	var body = [];
	var rsnums = [];
	
	rd.on('line', function(line) {

       try{
	if (line.startsWith(' rs')==true || line.startsWith('rs')==true || line.startsWith('COSM')==true || line.startsWith(' COSM')==true || line.startsWith('vcZ')==true || line.startsWith(' vcZ')==true || line.startsWith('ENSVV')==true || line.startsWith(' ENSVV')==true){
	    rsnums.push(line);
	}else{
	    body.push(line);
	}
       }catch(e){
	   console.log("Couldn't process the line..");
       }
	
   });
    
   rd.on('close',function(){
	console.log("finished reading index file..");
	//body = unique(body);		      
	//console.log(body.length);
	keys.rs = rsnums;
	keys.other = body;
	if (body != []){
	    //keyOBJ.keys = body;
	    resI.send(JSON.stringify(keys));
	    resI.end();
	}else{
	    resI.end();
	}
	
   });

   rd.on('error',function(){
	console.log('error Reading file');
	resI.end();
   });

    }catch(err){
	console.log("Index file not present..");
    }
   
});
// INFO
app.get('/get_dbinfo', function (req, resI) {

    try{
   console.log("Generating index..");
   var basePATH = "data/SNP_DATA_PIPELINE/FinalResults/SummarySpecies.txt";
   // gre cez celo bazo in generira gumbke glede na hite.
   // var keyOBJ = new Object();
   var spec = new Object();
   var rd = readline.createInterface({
	input: fs.createReadStream(basePATH),
	output: process.stdout,
	terminal: false
   });
	
	var specs = {};
   rd.on('line', function(line) {	    

       var parts = line.split(/\s+/);

       if (parts[0] == ''){
	   specs[parts[2]] = parts[1]
       }else{
	   specs[parts[1]] = parts[0]
       }
	
   });
    
	rd.on('close',function(){
	    spec.species = specs
	    resI.send(JSON.stringify(spec));
	    resI.end();
	// console.log("finished reading index file..");
	// //body = unique(body);		      
	// //console.log(body.length);
	// keys.rs = rsnums;
	// keys.other = body;
	// if (body != []){
	//     //keyOBJ.keys = body;
	//     resI.send(JSON.stringify(keys));
	//     resI.end();
	// }
	resI.end();
	
   });

   rd.on('error',function(){
	console.log('error Reading file');
	resI.end();
   });

    }catch(err){
	console.log("Index file not present..");
    }
   
});

// samples
app.get('/get_examples', function (req, resI) {

    var basePATH = "data/SNP_DATA_PIPELINE/FinalResults/examples.txt";


    if (fs.existsSync(basePATH)) {
	// Do something

	    try{
		console.log("Generating examples..");

		// gre cez celo bazo in generira gumbke glede na hite.
		// var keyOBJ = new Object();
		var keys = new Object();
		var rd = readline.createInterface({
		    input: fs.createReadStream(basePATH),
		    output: process.stdout,
		    terminal: false
		});

		var body = [];
		var rsnums = [];
		rd.on('line', function(line) {	    
		    
		    body.push(line);

		});
		
		rd.on('close',function(){
		    console.log("finished reading examples..");
		    if (body != []){
			resI.send(body);
			resI.end();
		    }else{
			resI.end();
		    }
		    
		});

		rd.on('error',function(){
		    console.log('error Reading file');
		    resI.end();
		});

	    }catch(err){
		console.log("Examples file not present..");
	    }
    }else{
	resI.end();
    }
});

// BASE
app.get('/download_gp', function(req, res){
    try{
  var file = __dirname + '/data/PDB_SNP_MAPPING_ALL/WHOLE_BASE/wholebase.txt';
	res.download(file); // Set disposition and send it.
    }catch(er){
	console.log("Couldn't download the file..");
    }
});


router.use(function (req,res,next) {
    console.log("/" + req.method);
    next();
});
// router.get("/",function(req,res){
//     res.sendFile(path + "index.html");
// });
app.use("/",router);

app.listen(3000, function (err) {    
    if (err) {
	throw err
    }
    
    console.log('Server started on port 3000');
})
// API
app.get('/api', function(req, resA) {

    var pdbid = req.param('pdb');
    
    var pdbchain = req.param('chain');

    var type = req.param('type');


    if (type == "json"){
	var path_snp = "data/PDB_SNP_MAPPING_ALL/JSON/"+pdbid+(pdbchain).toUpperCase()+".json.gz";
	var lineReader = readline.createInterface({
	    input: fs.createReadStream(path_snp).on('error',function(err){

		console.log("No file..")
		resA.end("no file")
	    }).pipe(zlib.createGunzip()).on('error', function(err){
		console.log("This doesn't work!")
		resA.end("no file")
	    })
	});

	var wholejson = "";
	
	lineReader.on('line', (line) => {
	    
	    wholejson += line;
	    
	});

	lineReader.on('close', () => {

	    resA.send(wholejson)
	    
	});
	
	lineReader.on('error', (line) => {

	    console.log("Couldn't read the file")
	    resA.end("File not available..")
	    
	});

    }else if (type == "csv"){

	var path_snp = "data/PDB_SNP_MAPPING_ALL/READABLE/"+pdbid+(pdbchain).toUpperCase()+".txt.gz";

	var lineReader = readline.createInterface({
	    input: fs.createReadStream(path_snp).on('error',function(err){

		console.log("No file..")
		resA.end("no file")
	    }).pipe(zlib.createGunzip()).on('error', function(err){
		console.log("This doesn't work!")
		resA.end("no file")
	    })
	});

	var wholejson = "";
	
	lineReader.on('line', (line) => {
	    
	    wholejson += line+"\n";
	    
	});

	lineReader.on('close', () => {

	    resA.send(wholejson)
	    
	});
	
	lineReader.on('error', (line) => {

	    console.log("Couldn't read the file")
	    resA.end("File not available..")
	    
	});

    }

});
// pdb code
app.get('/get_pdb', function(req, resP) {

    var name = req.param('pdbNAME');
    
    var chain = req.param('pdbCHAIN');

    var toread = "data/pdb/"+name.slice(1,3)+"/pdb"+name+".ent.gz";
    
    var lineReader = readline.createInterface({
	terminal: false,
	input: fs.createReadStream(toread).pipe(zlib.createGunzip())
    });
    
    var wholePDB = "";
    
    lineReader.on('line', function(line) { 
	
	wholePDB += line+"\n";
	
    });

    lineReader.on('close', function() {
	resP.send(String(wholePDB));
	resP.end();
    });

    lineReader.on('error', function() {
	console.log('Could process the PDB..');
	resP.end();
	
	//	read_gGRID(protein,chain,pdbcoordinates)
	
    });

    
    // try{
    // 	var path_pdb = "data/PDBs/"+name+"_"+chain+".pdb";
    // 	console.log(path_pdb);
    // 	console.log("File Exists, proceeding pdb call..");
    // 	resP.send(require('./Process.js').readPDB(path_pdb));
    // 	resP.end();

    // }catch(e){
    // 	console.log("File doesn't exist..");
    // 	resP.send("Nothing found for this entry.");
    // 	resP.end();
    // }

    

//     try{
// 	var path_pdbg = "data/PDBs/"+name+"_"+chain+".pdb.gz";
// 	var path_pdb = "data/PDBs/"+name+"_"+chain+".pdb";

// //	console.log("Ligand file Exists, proceeding pdb call..");

// 	var dat = '';
// 	fs.createReadStream(path_pdbg).on('error',function(e){
// 	    console.log("Ouch :(");
// 	    console.log("Trying uncompressed PDB files..");
// 	    try{
// 		var read_file = require('./Process.js').readPDB(path_pdb);
// 		resP.send(read_file);
// 		resP.end();
// 		console.log("request ended..");
// 	    }catch(er){
// 		console.log("None of the files read..");
// 		resP.end();

// 	    }
	    
// 	}).pipe(zlib.createGunzip())
// 	    .on('data', function (data){
// 		dat += data.toString()})
// 	    .on('end', function (){

// 		console.log("read PDB file..");

// 		resP.send(dat);
// 		resP.end();
// 	    }).on('error',function(e){

// 		console.log("error");
// 		resP.end();
// 	    });


//     }catch(e){
// 	console.log("File doesn't exist..");
// 	resP.send("Nothing found for this entry (Ligands).");
// 	resP.end();
//     }
       
});

// ligand code
app.get('/get_ligands', function(req, resP) {

    var name = req.param('ligNAME');
    var chain = req.param('ligCHAIN');

    
    var path_ligz = "data/lig_files/lig_"+name+"_"+chain+".json.gz";

    var lineReader = readline.createInterface({
	terminal: false,
	input: fs.createReadStream(path_ligz).on('error',function(e){

	    // generate it!
	    console.log("Generating the lig file..");
	    var command = 'cd data/algorithms;nodejs generateDB.js '+name+' '+chain+' 3 1 t;'+'cd data/algorithms; nodejs generateDB.js '+name+' '+chain+' 3 1';
	    var child = require('child_process').exec(command);

	    child.on('exit', function() {

		console.log("file generated..");
		var lineReader = readline.createInterface({
		    terminal: false,
		    input: fs.createReadStream(path_ligz).on('error',function(err){
			console.log("File not in the API yet!");
			resP.send("[]");
			resP.end(); 
		    }).pipe(zlib.createGunzip())
		});
		
		var wholeLIG = "";
		
		lineReader.on('line', function(line) { 
		    
		    wholeLIG += line;
		    
		});

		lineReader.on('close', function() {

		    resP.send(wholeLIG);
		    resP.end();
		});
		
	    });
	    
	}).pipe(zlib.createGunzip())
    });
    
    var wholeLIG = "";
    
    lineReader.on('line', function(line) { 
	
	wholeLIG += line;
	
    });

    lineReader.on('close', function() {
	console.log("Sending ligands..");
	resP.send(wholeLIG);
	resP.end();
    });

    lineReader.on('error', function() {

	// enter another cycle of computation..
	console.log("File not found");
	resP.send("[]");
	resP.end();
	
	// console.log("Sending error..");
	// var child = require('child_process').exec('cd data/algorithms;nodejs generateDB.js '+name+' '+chain+' 3 1 t;'+'cd data/algorithms; nodejs generateDB.js '+name+' '+chain+' 3 1');

	// child.on('exit', function() {

	    
	//     var lineReader = readline.createInterface({
	// 	terminal: false,
	// 	input: fs.createReadStream(path_ligz).pipe(zlib.createGunzip())
	//     });
	    
	//     var wholeLIG = "";
	    
	//     lineReader.on('line', function(line) { 
		
	// 	wholeLIG += line;
		
	//     });

	//     lineReader.on('close', function() {

	// 	resP.send(wholeLIG);
	// 	resP.end();
	//     });
 
	// });
	
    });

});

//plot the sequence
app.get('/get_seqplot', function(req, resP) {

    var pdb_id = req.param('pdbid_seqplot');
    var toread = "data/pdb/"+pdb_id.slice(1,3)+"/pdb"+pdb_id.slice(0,4)+".ent.gz";
    var lineReader = require('readline').createInterface({terminal: false,
	input: require('fs').createReadStream(toread).pipe(zlib.createGunzip())
    });

    var ldic = {};
    var locs = [];
    var aas = [];
    // 1bm6A, 1zmdA - tale dela probleme!
    var caa = "";
    var cl = "";
    var cmax = 0;
    var srcon = 0;
    var missing = [];
    var uniTrigger = false;
    var delList = [];
    var delAAlist = [];
    
    lineReader.on('line', function (line) {

	var parts = line.split(/\s+/);
    
	if (parts[1] == "465" && parts.length == 6 && parts[3] == pdb_id.slice(4,5).toUpperCase()){
	    //console.log(parts[4],parts.length);
	    missing.push(parts[4]);
	    
	}

	// check atom
	if (parts[0]=="ATOM" && parts[4] == pdb_id.slice(4,5).toUpperCase()){

	    // iterate only over molecules
	    if (parts[3] != caa || parts[5] != cl){

		// remove multi chain problems
		if (cmax <= parseInt(parts[5]) || parts[5].match( /(A|B|C|D|E|F|G|H|I|J|K)/ )){
				if (locs.indexOf(parts[5]) == -1) {
					locs.push(parts[5]);
		    cmax = parseInt(parts[5]);
				}
		}
	    }

	}
	// check atom
	else if (parts[0]=="ATOM" && (parts[4]).indexOf(pdb_id.slice(4,5).toUpperCase()) != -1){

	    // iterate only over molecules
	    if (parts[3] != caa || parts[5] != cl){

		// remove multi chain problems
		if (cmax <= parseInt(parts[4].substring(1)) || ((parts[4]).substring(1)).match( /(A|B|C|D|E|F|G|H|I|J|K)/ )){
				if (locs.indexOf((parts[4]).substring(1)) == -1) {
					locs.push((parts[4]).substring(1));
		    cmax = parseInt((parts[4]).substring(1));
				}
		}
	    }

	}

	// update molecule parameters
	cl = parts[5];
	caa = parts[3];

	
	if (parts[0] == "SEQRES" && parts[2] == pdb_id.slice(4,5).toUpperCase()){
	    srcon++;
	    //console.log(parts);
	    var tmpc = 0;
	    parts.forEach(function(part){
		tmpc ++;
		if (tmpc>4 && part != ''){
		    aas.push(part);
//		    console.log(part);
		}
	    });
	}

	// only uniprot is valid!
	if (parts[0] == "DBREF" && parts[5] == "UNP"){
	    uniTrigger = true;
	}


	if (parts[0] == "SEQADV" && uniTrigger == true){

	    delList.push(parts[6]);
	    delAAlist.push(parts[5]);
	}
	
    });


    lineReader.on('close', function () {

	var counts = {};
	//console.log(aas, srcon);
	
	ldic["lo"] = locs;
	ldic["aa"] = aas;
	ldic["missing"] = missing;
	ldic['deletion'] = delList;
	ldic['deletionAA'] = delAAlist;

	resP.send(ldic);
	resP.end();
	
	// try{
	// (function send_Unidata(query,chain){

	//     function get_uniPDBMapping(query1){

	// 	var obj = JSON.parse(fs.readFileSync('data/SNP_DATA_PIPELINE/FinalResults/PDB_UniMapping.json', 'utf8'));

	// 	return obj[query1]
		
	//     }
	//     var options = {
	// 	host: 'www.uniprot.org',
	// 	path: '/uniprot/'+get_uniPDBMapping(query)+'.txt'
	//     }

	//     var request = http.request(options, function (res) {
	// 	var data = '';
	// 	var PDBseq = "";
	// 	res.on('data', function (chunk) {
	// 	    data += chunk;
	// 	});
	// 	res.on('end', function () {

	// 	    var sequence = "";
	// 	    var range = [];
	// 	    var rcount = false;
	// 	    data.split("\n").forEach(function(entry){

	// 		var splits = entry.match(/\S+/g);
	// 		if (splits != null){
	// 		    if (splits[0] == "DR"){
	// 			if (splits[2].replace(";","").toLowerCase() == query){
	// 			    splits.forEach(function(check){
	// 				if (check.match(/=/g)){
	// 				    if (check.match(chain)){
	// 					//console.log(check)
	// 					range = check.match(/\d+/g)
	// 					range = range.slice(range.length-2,range.length);
						
	// 				    }
	// 				}
	// 			    });
	// 			}
	// 		    }else if (rcount == true){
	// 			sequence += entry;
	// 		    }else if (splits[0] == "SQ" && rcount == false){
	// 			rcount = true;
	// 		    }else{}
	// 		}
	// 	    });
	// 	    try{
	// 		sequence = sequence.replace("//","").match(/\S+/g).join("");
	// 	    }catch(err){
	// 		resP.end();
	// 	    }
	// 	    var aacount = 0;
	// 	    var PDBseq = "";
	// 	    sequence.split("").forEach(function(aa){
	// 		aacount ++;
	// 		if (aacount >= parseInt(range[0]) && aacount <= parseInt(range[1])){
	// 		    PDBseq += aa;
	// 		}
	// 	    });	    	    	    

	// 	    ldic['UniSeq'] = PDBseq;
	// 	    resP.send(ldic);
	// 	    resP.end();
		    
	// 	});
	//     });
	//     request.on('error', function (e) {
	// 	console.log(e.message);
	// 	resP.end();
	//     });
	//     request.end();

	    
	// })(pdb_id.slice(0,4), pdb_id.slice(4,5));
	// }catch(err){	
	// // resP.send(ldic);
	//     resP.end();
	// }
    });

    lineReader.on('error', function (line) {
	
	console.log("Couldn't read the file..");
    });
    


});
//buffer code
app.post('/get_buffer', function(req, resB) {

    var name = req.param('bname');
    
    var chain = req.param('bchain');
    
    try{
	// 	var path_buffer = "data/buffers/buffer_"+name+"_"+chain+".json";
     	var path_bufferg = "data/buffers/buffer_"+name+"_"+chain+".json.gz";
	
    	//fs.accessSync(path_buffer, fs.F_OK);
    	console.log("Proceeding buffer call..");
	
    	var dat = '';
	
    	fs.createReadStream(path_bufferg).on('error',function(e){
    		resB.send({});
    		resB.end();
    	    
    	}).pipe(zlib.createGunzip())
	
    	    .on('data', function (data){
		
    		dat += data.toString()})
	
    	    .on('end', function (){
		
    		//		console.log(d);o3
		
    		console.log("read buffer file..");
		
    		resB.send(dat);
    		resB.end();
		
    	    }).on('error',function(e){
		resB.end();
		console.log("No buffer yet found!");
		
    	    });
	

	
	// 	//resB.send(require('./Process.js').readBuffer(path_buffer));
	
	// 	//resB.end();

    }catch(e){
    	resB.send([]);
     	console.log("File doesn't exist..");
     	//resB.end();
    }
    

});

//keep main result files and all entries connected with CURRENT entry.
// function clean(pdbname){exec("cd data/PDB_SNP_surface; ls | mawk '!/^snpV/ && !/"+pdbname+".pdb/ && !/"+pdbname+".snp/' | xargs rm");}

// function unique(arr) {
//     var u = {}, a = [];
//     for(var i = 0, l = arr.length; i < l; ++i){
//         if(!u.hasOwnProperty(arr[i])) {
//             a.push(arr[i]);
//             u[arr[i]] = 1;
//         }
//     }
//     return a;
// }


if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position){
      position = position || 0;
      return this.substr(position, searchString.length) === searchString;
  };
}


function readfiles (gAcc,filenames,cb){
    //console.log("Function called");

    filenames.forEach(function (fname){
	line_reader = new line_line(fname); // trenutna example baza
	//console.log("reading>>>>"+fname);
	var body = [];
	// var rd = readline.createInterface({
	//     input: fs.createReadStream(fname),
	//     output: process.stdout,
	//     terminal: false
	// });
	
	// rd.on('line', function(line) {
	    
	//     var entries = line.split(/\s+/);

	//     for (var j =0; j < entries.length;j++){
	// 	if (entries[j] == gAcc || entries[j] == gAcc.toUpperCase()){
	// 	    body.push([entries[4],entries[5],entries[7]]);
	// 	}
	//     }

	// });

	line_reader.on('line', function (line) {
	    	    var entries = line.split(/\s+/);

	    for (var j =0; j < entries.length;j++){

		if (j == 7){
		    if (entries[j].split("_")[0] == gAcc){
		    body.push([entries[4],entries[5],entries[7]]);
		    }
		}
		
		if (entries[j] == gAcc || entries[j] == gAcc.toUpperCase()){
		    body.push([entries[4],entries[5],entries[7]]);
		}
	    }
	});
	
	// rd.on('close',function(){	    
	//     console.log("finished reading file.."+fname);
	//     cb(body,1)
	
	// });

	line_reader.on('end', function () {
	    //console.log("Finished reding the: "+fname)
	    cb(body, 1)
	});


	line_reader.on('error', function (err) {
	    console.log("Failed to read the file..");
	    cb([], 1);
	});	
	// rd.on('error',function(){
	//     console.log('error Reading file');
	//     cb([],1)
	// });

    });   
}


function get_uniPDBMapping(query){

    var obj = JSON.parse(fs.readFileSync('data/SNP_DATA_PIPELINE/FinalResults/PDB_UniMapping.json', 'utf8'));

    return obj[query]

    

}
