blazs#!/bin/bash


if [ "$1" == "--GNF"  ]
   
then

    ## this generates ligands, buffers and grid files needed for computation
    rm -rvf ../buffers/* # first delete the buffers.
    rm -rvf ../lig_files/* # first delete the buffers.
    cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk '{print "nodejs generateDB.js",$5,$6,3,1,"t"}' | sort -u | parallel

    cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk '{print "nodejs predloga.js tmp_"$5"_"$6".pdb"}' | sort -u | parallel

elif [ "$1" == "--maplocal" ]
     
then

    ## this creates the files to be mapped..
    cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk '{print "nodejs generateDB.js",$5,$6,3,1,"t"}' | sort -u | parallel

    ## map SNPs - this generates SNPv files..
    cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk '{print "nodejs generateDB.js",$5,$6,3,1}' | sort -u | parallel
    
    
elif [ "$1" == "--R" ]

then

    cat ../TMPfiles/nosurf.txt | mawk '{print "nodejs generateDB.js",$1,$2,3,1}' | sort -u | parallel
    
elif [ "$1" == "--identify" ]

then
    ## without the t flag, computation is executed.
    cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk '{print "nodejs generateDB.js",$5,$6,3,1}' | sort -u | parallel

    
    cd ../buffers

    if which pigz >/dev/null; then
        pigz *
    else
        gzip *
    fi


    cd ../lig_files

    if which pigz >/dev/null; then
        pigz *
    else
        gzip *
    fi


elif [ "$1" == "--WP"  ]
     
then

    ## this generates ligands, buffers and grid files needed for computation
    rm -rvf ../buffers/* # first delete the buffers.
    rm -rvf ../lig_files/* # first delete the buffers.

    ### make files for computation..
    cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk '{print "cd /home/blazs/GenProBiS/data/algorithms;nodejs generateDB.js",$5,$6,3,1,"t"}' | sort -u | parallel --sshloginfile slaves --workdir /home/blazs/GenProBiS/data/algorithms

    ### compute buffers
    cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk '{print "cd /home/blazs/GenProBiS/data/algorithms;nodejs predloga.js tmp_"$5"_"$6".pdb"}' | sort -u | parallel --sshloginfile slaves --workdir /home/blazs/GenProBiS/data/algorithms    

    ### MAP SNPs
    cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk '{print "cd /home/blazs/GenProBiS/data/algorithms;nodejs generateDB.js",$5,$6,3,1}' | sort -u | parallel --sshloginfile slaves --workdir /home/blazs/GenProBiS/data/algorithms

    ### 2nd cycle
    cat ../TMPfiles/nosurf.txt | mawk '{print "cd /home/blazs/GenProBiS/data/algorithms;nodejs generateDB.js",$1,$2,3,1}' | sort -u | parallel --sshloginfile slaves --workdir /home/blazs/GenProBiS/data/algorithms

    ### compress stuff..
    cd ../buffers

    if which pigz >/dev/null; then
        pigz *
    else
        gzip *
    fi

    cd ../lig_files

    if which pigz >/dev/null; then
        pigz *
    else
        gzip *
    fi


elif [ "$1" == "--testrun"  ]
     ## this is to test
then

    mawk '{print "ls"}' | parallel --sshloginfile slaves
    
elif [ "$1" == "--nobuffer"  ]
     ## this is to test

then
    echo "nobuffer.."
    cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk ' { print "cd /home/blazs/GenProBiS/data/algorithms;nodejs generateDB.js",$5,$6,3,1,"t" } ' | head -n 13000 | sort -u | parallel -j1 --sshloginfile slaves
  
elif [ "$1" == "--onlybuffer"  ]
     ## this is to test
then

    cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk '{print "cd /home/blazs/GenProBiS/data/algorithms;nodejs predloga.js tmp_"$5"_"$6".pdb"}' | sort -u | parallel  --sshloginfile slaves --workdir /home/blazs/GenProBiS/data/algorithms

      
elif [ "$1" == "--partialbase"  ]
     ## this is to test
then

    cat ../SNP_DATA_PIPELINE/FinalResults/searchfiles/"BASE$2" | mawk ' { print "cd /home/blazs/GenProBiS/data/algorithms;nodejs generateDB.js",$5,$6,3,1,"t" } ' | sort -u | parallel 


    cat ../SNP_DATA_PIPELINE/FinalResults/searchfiles/"BASE$2" | mawk ' { print "cd /home/blazs/GenProBiS/data/algorithms;nodejs generateDB.js",$5,$6,3,1 } ' | sort -u | parallel 

elif [ "$1" == "--partialbuffer"  ]
     ## this is to test
then

    cat ../SNP_DATA_PIPELINE/FinalResults/searchfiles/"BASE$2" | mawk ' { print "cd /home/blazs/GenProBiS/data/algorithms;nodejs predloga.js","tmp_"$5"_"$6".pdb.gz" } ' | sort -u #| parallel 


elif [ "$1" == "--reduce"  ]

     
then

    ## Do a parallel reduce step. All files will be in the /home/blazs/GenProBiS/data folder, copy where necessary (e.g. var/www..)
    if [ "$2" != ":" ]; then  #omit localhost
	## gather Ligand files..
	cat slaves | mawk '{print "rsync -avr",$2":/home/blazs/GenProBiS/data/lig_files/*", "/home/blazs/GenProBiS/data/lig_files"}' | parallel

	# ## gather tmp grid files
	 cat slaves | mawk '{print "rsync -avr",$2":/home/blazs/GenProBiS/data/grid_files/*", "/home/blazs/GenProBiS/data/grid_files"}' | parallel
	
	## .snpV files
	cat slaves | mawk '{print "rsync -avr",$2":/home/blazs/GenProBiS/data/PDB_SNP_surface/*", "/home/blazs/GenProBiS/data/PDB_SNP_surface"}' | parallel

	# ## PPISNPs
	cat slaves | mawk '{print "rsync -avr",$2":/home/blazs/GenProBiS/data/PPISNP/*", "/home/blazs/GenProBiS/data/PPISNP/"}' | parallel

	cat slaves | mawk '{print "rsync -avr",$2":/home/blazs/GenProBiS/data/buffers/*", "/home/blazs/GenProBiS/data/buffers"}' | parallel


    fi
    
elif [ "$1" == "--map"  ]
     
then

    ### send folders to all hosts..
    cat slaves | mawk '{print "rsync -avr","/home/blazs/GenProBiS", $2":~"}' |  parallel
    echo "Done mapping the files to slaves..";
    
elif [ "$1" == "--MapReduceNB"  ]
     
then
    
    ### send folders to all hosts..
    cat slaves | mawk '{print "rsync -avr","/home/blazs/GenProBiS", $2":~"}' |  parallel
    echo "Done mapping the files to slaves..";

    ## calculate
    echo "Preparing input.. Calculation commencing.."
    cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk '{print "cd /home/blazs/GenProBiS/data/algorithms;nodejs generateDB.js",$5,$6,3,1,"t",";nodejs generateDB.js",$5,$6,3,1 }' | sort -u | parallel --sshloginfile slaves 

    ## get 
    cat slaves | mawk '{print "rsync -avr",$2":/home/blazs/GenProBiS/data/lig_files/*", "/home/blazs/GenProBiS/data/lig_files"}' | parallel

    ## gather tmp grid files
    cat slaves | mawk '{print "rsync -avr",$2":/home/blazs/GenProBiS/data/grid_files/*", "/home/blazs/GenProBiS/data/grid_files"}' | parallel
    
    ## .snpV files
    cat slaves | mawk '{print "rsync -avr",$2":/home/blazs/GenProBiS/data/PDB_SNP_surface/*", "/home/blazs/GenProBiS/data/PDB_SNP_surface"}' | parallel

    ## PPISNPs
    cat slaves | mawk '{print "rsync -avr",$2":/home/blazs/GenProBiS/data/PPISNP/*", "/home/blazs/GenProBiS/data/PPISNP/"}' | parallel

    ## PDB files
    cat slaves | mawk '{print "rsync -avr",$2":/home/blazs/GenProBiS/data/PDBs/*", "/home/blazs/GenProBiS/data/PDBs"}' | parallel

elif [ "$1" == "--MapReduceB"  ]
     
then

    ## calculate buffers
    echo "Preparing input.. Calculation commencing.."
    #cat ../SNP_DATA_PIPELINE/FinalResults/WholeBase.txt | mawk '{print "cd /home/blazs/GenProBiS/data/algorithms;nodejs predloga.js tmp_"$5"_"$6".pdb.gz" }' | sort -u | parallel --sshloginfile slaves 


    ## PDB files
    cat slaves | mawk '{print "rsync -avr",$2":/home/blazs/GenProBiS/data/buffers/*", "/home/blazs/GenProBiS/data/buffers"}' | parallel

## copy to current Master node
elif [ "$1" == "--cpMaster" ]

then
     rsync -avr /home/blazs/GenProBiS/data/lig_files/* /var/www/html/GenProBiS/data/lig_files
     
     rsync -avr /home/blazs/GenProBiS/data/PDB_SNP_surface/* /var/www/html/GenProBiS/data/PDB_SNP_surface

     rsync -avr /home/blazs/GenProBiS/data/grid_files/* /var/www/html/GenProBiS/data/grid_files
     
     rsync -avr /home/blazs/GenProBiS/data/buffers/* /var/www/html/GenProBiS/data/buffers
     
     

    ## clean before new update!
elif [ "$1" == "--cleanSlave" ]

then
    
     rm -rvf /home/blazs/GenProBiS/data/lig_files/*
     rm -rvf /home/blazs/GenProBiS/data/PDB_SNP_surface/*
     rm -rvf /home/blazs/GenProBiS/data/buffers/*
     rm -rvf /home/blazs/GenProBiS/data/grid_files/*
     
elif [ "$1" == "--generateSNPdb" ]

then 

#    rsync blazs@probis:/home/probis/insilab.org/files/GenProBiS/probisdb/bresi ../BS_DATA

    #rsync blazs@probis:/home/probis/insilab.org/files/GenProBiS/probisdb/bresi/* ../BS_DATA/bresi/
    #rsync -a --port=33444 ftp.wwpdb.org::ftp_data/structures/divided/pdb/ ./pdb

    #    rm -rvf ../PDB_SNP_MAPPING_ALL/WHOLE_BASE/wholebase.txt

    #rm -rvf ../PDB_SNP_MAPPING_ALL/JSON/*
    #rm -rvf ../PDB_SNP_MAPPING_ALL/READABLE/*
    #rm -rvf ../PDB_SNP_MAPPING_ALL/WHOLE_BASE/*
   
    cat ../SNP_DATA_PIPELINE/FinalResults/SNP/* | mawk '{print "node map_snp_pdb_all.js",$5$6}' | sort -u | parallel
    
elif [ "$1" == "--generateOLD" ]

then 

#    rsync blazs@probis:/home/probis/insilab.org/files/GenProBiS/probisdb/bresi ../BS_DATA

    #rsync blazs@probis:/home/probis/insilab.org/files/GenProBiS/probisdb/bresi/* ../BS_DATA/bresi/
    #rsync -a --port=33444 ftp.wwpdb.org::ftp_data/structures/divided/pdb/ ./pdb

    rm -rvf ../PDB_SNP_MAPPING_ALL/JSON/*
    rm -rvf ../PDB_SNP_MAPPING_ALL/READABLE/*
    rm -rvf ../PDB_SNP_MAPPING_ALL/WHOLE_BASE/*
    
    cat ../SNP_DATA_PIPELINE/FinalResults/SNP/* | mawk '{print "node map_snp_pdb_all.js",$5$6}' | sort -u | parallel
    

  
else
    echo "Nothing selected.."
    
fi

