

var SurfaceData1=[];
var PDBcoordinates1 = [[50,-1,1,1],[60,1,2,1],[652,2,2,2]];
var GRIDcoordinates1 = [[1,1,1],[1,3,1],[2,2,2.1]];

var errorBound1 = 0.2;
PDBcoordinates1.forEach(function(tri) {

    GRIDcoordinates1.forEach(function(triG){
	var px = tri[1] > 0 ? 1 : tri[1] == 0 ? 0 : -1;
	var py = tri[2] > 0 ? 1 : tri[2] == 0 ? 0 : -1;
	var pz = tri[3] > 0 ? 1 : tri[3] == 0 ? 0 : -1;

	var gx = triG[0] > 0 ? 1 : triG[0] == 0 ? 0 : -1;
	var gy = triG[1] > 0 ? 1 : triG[1] == 0 ? 0 : -1;
	var gz = triG[2] > 0 ? 1 : triG[2] == 0 ? 0 : -1;

	//console.log(px+" "+ gx+ " ,," + " "+ py  + " "+ gy + " ,,"+ pz + " "+ gz )

	var temptrigger = 0;
	if ( px == gx && py == gy && pz == gz){

	    temptrigger = 1;
	}
	
	if (

	    //x
	    temptrigger == 1 &&
	    Math.abs(triG[0]) < Math.abs(tri[1]*
					 (1+errorBound1)) &&
		Math.abs(triG[0]) > Math.abs(tri[1]*
					     (1-errorBound1)) &&
		//y
		Math.abs(triG[1]) < Math.abs(tri[2]*
					     (1+errorBound1)) &&
		Math.abs(triG[1]) > Math.abs((tri[2]*
					      (1-errorBound1))) &&
		//z
		Math.abs(triG[2]) < Math.abs(tri[3]*
					     (1+errorBound1)) &&
		Math.abs(triG[2]) > Math.abs((tri[3]*
					      (1-errorBound1)))
	){
	    //console.log("helol");
	    SurfaceData1.push(tri[0]);

	}

    });
});

console.log(SurfaceData1)
