## This command computes the species summaries.
cd ../SNP_DATA_PIPELINE/FinalResults
cat WholeBase.txt | mawk '{print $8}' | mawk '{split($1,a,"_");print a[1]"_"a[2]}' | sort |uniq -c > 
