// to je predloga za branje fajlov. Tukej not butn svojo kodo, mene je cas povozu atm, tko da naum iskal kje tocno dat notr
//inpute!

var zlib = require('zlib')
var fs = require('fs');
var stream = require('stream');
var fex = require('file-exists');
var readline = require('readline');
var vec3 = require('gl-vec3')

// tole iniciiranje je temp, to bo na eni tocki argv[0] in argv[1], zagnan pa bo ubistvu kot nodejs buffers.js 3kej A recimo.

var pdb = process.argv[2];
var keychain = pdb.split("_");// to je pdb id
var ime_PDB_grid = '../grid_files/'+pdb;
var outfile = '../buffers/buffer_'+keychain[1]+'_'+keychain[2].replace(".pdb.gz","")+'.json';



try{
    fex(ime_PDB_grid);
    
    var basePATH = ime_PDB_grid;
    var rd = readline.createInterface({
	input: fs.createReadStream(basePATH).pipe(zlib.createGunzip()),
	output: process.stdout,
	terminal: false
    });

    var body = "";
    var models = [];
    var modelcount = 0;
    var pixi = "";
    var currentregion = [];
    var tempmodel = "";
    var temptype = "";
    
    rd.on('line', function(line) {  
	
	var TMPGRID1 = line.split(/\s+/);

	if (TMPGRID1[2] == "proteid")
		TMPGRID1[2] == "protein";
	
	if (TMPGRID1.length == 3){

	    //tempmodel = "Binding$Site"+"$#_"+TMPGRID1[1] +"_"+TMPGRID1[2];

	    // this is for first entry..
	    if (modelcount > 0 ){
	    	console.log("Currently processed: "+tempmodel);
		//	console.log(TMPGRID1[2]);
	    	models.push([tempmodel, generate_buffer(body,tempmodel.split("_")[2])]);			    
	    	tempmodel = "Binding$Site"+"$#_"+TMPGRID1[1] +"_"+TMPGRID1[2];
		body = "";
	    	//this is for all other entries..
	    }else{
	    	tempmodel = "Binding$Site"+"$#_"+TMPGRID1[1] +"_"+TMPGRID1[2];
	    	modelcount ++;
		console.log(tempmodel+" "+modelcount);
	    }
	    
	}else{

	    body += line +'\n';
	}

    });

    rd.on('close',function(){
	console.log("Currently processed: "+tempmodel.split("_")[2]);
	models.push([tempmodel, generate_buffer(body,tempmodel.split("_")[2])]); // do the last one..
	
	// fs.writeFile(outfile,JSON.stringify(models), function(err) {
    	//     if(err) {
    	// 	return console.log(err);
    	//     }
    	//     console.log("The file "+ outfile+" was saved.."+modelcount+ " models found..");
    	// });

	zlib.gzip(JSON.stringify(models),function(err,res){
	    
	    if (err) throw err;
	    
	    fs.writeFile(outfile+".gz",res);
	     console.log("The file "+ outfile+".gz was saved.."+modelcount+ " models found..");
	});
	
    });

    rd.on('error',function(){

    });
}catch(e){
    console.log("File non-existing.."+ime_PDB_grid);
}



    function generate_buffer(grid_string,type){
    //tukej vzemi ta grid string in potem to pozeni cez tvoj del, pa nej shrani pol kot json tko:
    var finalJSON = new Object();
    var struc = load_surf_pdb(grid_string, 'krneki');
    //console.log(grid_string);
    //buffer_string = generateSurfaces_pdb(struc.chains[0]);
    finalJSON = generateSurfaces_pdb(struc.chains[0], type);
    return finalJSON
    
}



AATypes = {"ALA": 1, "CYS": 1, "ASP": 1, "GLU": 1, "PHE": 1, "GLY": 1, "HIS": 1, "ILE": 1, "LYS": 1, "LEU": 1, "MET": 1, "ASN": 1, "PRO": 1, "GLN": 1, "ARG": 1, 
"SER": 1, "THR": 1, "VAL": 1, "TRP": 1, "TYR": 1, "ACE": 1, "NME": 1, "HIP": 1, "HIE": 1, "HID": 1, "CYX": 1,
	   "A": 1, "T": 1, "G": 1, "C": 1, "DA": 1, "DT": 1, "DG": 1, "DC": 1, "U": 1, "DU": 1, "MSE": 1, "SEQ": 1, "CSW": 1};


var edgeTable = new Int32Array([
		0x0, 0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c,
		0x80c, 0x905, 0xa0f, 0xb06, 0xc0a, 0xd03, 0xe09, 0xf00,
		0x190, 0x99, 0x393, 0x29a, 0x596, 0x49f, 0x795, 0x69c,
		0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90,
		0x230, 0x339, 0x33, 0x13a, 0x636, 0x73f, 0x435, 0x53c,
		0xa3c, 0xb35, 0x83f, 0x936, 0xe3a, 0xf33, 0xc39, 0xd30,
		0x3a0, 0x2a9, 0x1a3, 0xaa, 0x7a6, 0x6af, 0x5a5, 0x4ac,
		0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0,
		0x460, 0x569, 0x663, 0x76a, 0x66, 0x16f, 0x265, 0x36c,
		0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a, 0x963, 0xa69, 0xb60,
		0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0xff, 0x3f5, 0x2fc,
		0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0,
		0x650, 0x759, 0x453, 0x55a, 0x256, 0x35f, 0x55, 0x15c,
		0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53, 0x859, 0x950,
		0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0xcc,
		0xfcc, 0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0,
		0x8c0, 0x9c9, 0xac3, 0xbca, 0xcc6, 0xdcf, 0xec5, 0xfcc,
		0xcc, 0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9, 0x7c0,
		0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c,
		0x15c, 0x55, 0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650,
		0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6, 0xfff, 0xcf5, 0xdfc,
		0x2fc, 0x3f5, 0xff, 0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0,
		0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c,
		0x36c, 0x265, 0x16f, 0x66, 0x76a, 0x663, 0x569, 0x460,
		0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af, 0xaa5, 0xbac,
		0x4ac, 0x5a5, 0x6af, 0x7a6, 0xaa, 0x1a3, 0x2a9, 0x3a0,
		0xd30, 0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c,
		0x53c, 0x435, 0x73f, 0x636, 0x13a, 0x33, 0x339, 0x230,
		0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895, 0x99c,
		0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x99, 0x190,
		0xf00, 0xe09, 0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c,
		0x70c, 0x605, 0x50f, 0x406, 0x30a, 0x203, 0x109, 0x0 ]);

var triTable = new Int32Array([
                           		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1,
                           		3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1,
                           		3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1,
                           		3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1,
                           		9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1,
                           		9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1,
                           		2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1,
                           		8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1,
                           		9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1,
                           		4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1,
                           		3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1,
                           		1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1,
                           		4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1,
                           		4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1,
                           		9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1,
                           		1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1,
                           		5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1,
                           		2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1,
                           		9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1,
                           		0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1,
                           		2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1,
                           		10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1,
                           		4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1,
                           		5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1,
                           		5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1,
                           		9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1,
                           		0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1,
                           		1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1,
                           		10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1,
                           		8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1,
                           		2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1,
                           		7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1,
                           		9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1,
                           		2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1,
                           		11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1,
                           		9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1,
                           		5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1,
                           		11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1,
                           		11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1,
                           		1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1,
                           		9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1,
                           		5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1,
                           		2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1,
                           		0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1,
                           		5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1,
                           		6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1,
                           		0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1,
                           		3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1,
                           		6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1,
                           		5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1,
                           		1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1,
                           		10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1,
                           		6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1,
                           		1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1,
                           		8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1,
                           		7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1,
                           		3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1,
                           		5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1,
                           		0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1,
                           		9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1,
                           		8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1,
                           		5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1,
                           		0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1,
                           		6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1,
                           		10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1,
                           		10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1,
                           		8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1,
                           		1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1,
                           		3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1,
                           		0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1,
                           		10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1,
                           		0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1,
                           		3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1,
                           		6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1,
                           		9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1,
                           		8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1,
                           		3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1,
                           		6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1,
                           		0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1,
                           		10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1,
                           		10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1,
                           		1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1,
                           		2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1,
                           		7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1,
                           		7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1,
                           		2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1,
                           		1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1,
                           		11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1,
                           		8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1,
                           		0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1,
                           		7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1,
                           		10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1,
                           		2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1,
                           		6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1,
                           		7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1,
                           		2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1,
                           		1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1,
                           		10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1,
                           		10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1,
                           		0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1,
                           		7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1,
                           		6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1,
                           		8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1,
                           		9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1,
                           		6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1,
                           		4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1,
                           		10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1,
                           		8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1,
                           		0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1,
                           		1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1,
                           		8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1,
                           		10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1,
                           		4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1,
                           		10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1,
                           		5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1,
                           		11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1,
                           		9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1,
                           		6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1,
                           		7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1,
                           		3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1,
                           		7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1,
                           		9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1,
                           		3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1,
                           		6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1,
                           		9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1,
                           		1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1,
                           		4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1,
                           		7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1,
                           		6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1,
                           		3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1,
                           		0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1,
                           		6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1,
                           		0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1,
                           		11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1,
                           		6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1,
                           		5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1,
                           		9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1,
                           		1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1,
                           		1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1,
                           		10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1,
                           		0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1,
                           		5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1,
                           		10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1,
                           		11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1,
                           		9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1,
                           		7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1,
                           		2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1,
                           		8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1,
                           		9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1,
                           		9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1,
                           		1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1,
                           		9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1,
                           		9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1,
                           		5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1,
                           		0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1,
                           		10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1,
                           		2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1,
                           		0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1,
                           		0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1,
                           		9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1,
                           		5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1,
                           		3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1,
                           		5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1,
                           		8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1,
                           		9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1,
                           		0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1,
                           		1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1,
                           		3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1,
                           		4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1,
                           		9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1,
                           		11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1,
                           		11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1,
                           		2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1,
                           		9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1,
                           		3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1,
                           		1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1,
                           		4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1,
                           		4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1,
                           		0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1,
                           		3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1,
                           		3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1,
                           		0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1,
                           		9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1,
                           		1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
                           		-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ]);

load_surf_pdb = function(data, filename) {
  var currentChain = null; var ccid = null; var currentMol = null; var cmid = null; var atom, i;
  var data = data.split("\n");
  
  var chainName, molID, atomName, molName, x, y, z, offset, element, mat, temp, begin_cid, begin_mid, end_cid, end_mid, c;
  
  var helixData = [];
  var sheetData = [];
  var newModels = [];
  this.chains = [];
  this.atomRef = {};
  
  var struc = null, Xpos, cmnum;
  
  for (i=0; i<data.length; i++) {
  	if ((data[i].substring(0,5) == "MODEL" && currentChain) || ! struc) {
      if (struc) break;
      struc = new add_entryObject({id: filename});
      cmnum = data[i].substr(5).trim();
      ccid = cmid = null;
    }
    if (data[i].substring(0, 4) == "ATOM" || data[i].substring(0, 6) == "HETATM") {
      chainName = data[i].substring(21, 22).trim();
      molID = data[i].substring(22, 28).trim();
      atomName = data[i].substring(11, 16).trim();
      molName = data[i].substring(17, 21).trim();
      
      x = parseFloat(data[i].substring(30, 38).trim());
      y = parseFloat(data[i].substring(38, 46).trim());
      z = parseFloat(data[i].substring(46, 54).trim());
      
      for (offset=0; offset<atomName.length; offset++) if (! isNumberFun(atomName[offset])) break;
      if (atomName.length > 1 && ! isNumberFun(atomName[1]) && atomName[1] == atomName[1].toLowerCase()) element = atomName.substring(offset, offset+2);
      else element = atomName.substring(offset, offset+1);

      if (chainName != ccid) {
        this.chains.push(currentChain = new add_chainObject(chainName, struc)); struc.chains.push(currentChain);
        currentChain.CID = this.CID++;
        ccid = chainName; cmid = null;
      }
      if (molID != cmid) {
        currentChain.molecules.push(currentMol = new add_molObject(molName, molID, currentChain));
        currentMol.MID = this.MID++;
        cmid = molID;
      }
 
      Xpos = currentChain.modelsXYZ[0].length;
      currentChain.modelsXYZ[0].push(x, y, z);
      currentMol.atoms.push(atom=new add_atomObject(Xpos, atomName, element, currentMol, currentChain));

      if (data[i].substr(0, 4) != "ATOM" || ! AATypes.hasOwnProperty(currentMol.name.substr(0, 3))) {
        if (currentMol.name == "HOH" || currentMol.name == "DOD" || currentMol.name == "WAT" || currentMol.name == "SOL") {currentMol.water = true; currentMol.ligand = false; atom.display = false;}
        //do special stuff for dna/rna
        else if (atom.atomName == "P") {currentMol.P = atom; currentMol.N = atom; currentMol.xna = true; currentMol.ligand = false; if (! currentMol.CA) {currentChain.isHet = false; currentMol.CA = atom;}}
        else if (atom.atomName == "C1'") {currentChain.isHet = false; currentMol.CA = atom; currentMol.xna = true; currentMol.ligand = false;}
        else if (atom.atomName == "O3'") {currentMol.C = atom; currentMol.xna = true; currentMol.ligand = false;}
      }
      else {
        if (atom.atomName == "N") {currentMol.N = atom; currentMol.ligand = false;}
        else if (atom.atomName == "CA") {currentMol.CA = atom; currentMol.ligand = false;}
        else if (atom.atomName == "C") {currentChain.isHet = false; currentMol.C = atom; currentMol.ligand = false;}
        else if (atom.atomName == "O") {currentMol.O = atom; currentMol.ligand = false;}
      }
      if (atom.element == "H" || atom.element == "D") atom.display = false;
      currentChain.atoms.push(atom);
      atom.AID = this.AID++;
      this.atomRef[atom.AID] = atom;
    }
  }
  
  var cid = 0, xyzs;
  
  for (; i<data.length; i++) {
    if (data[i].substring(0,5) == "MODEL") {
      ccid = cid = -1;
    }
    if (data[i].substring(0, 4) == "ATOM" || data[i].substring(0, 6) == "HETATM") {
      chainName = data[i].substring(21, 22).trim();
      
      x = parseFloat(data[i].substring(30, 38).trim());
      y = parseFloat(data[i].substring(38, 46).trim());
      z = parseFloat(data[i].substring(46, 54).trim());

      if (chainName != ccid) {
        ccid = chainName; cid += 1;
        struc.chains[cid].modelsXYZ.push(xyzs=[]);
      }
      
      xyzs.push(x, y, z);
    }
  }
  
  struc.number_of_frames = struc.chains.length ? struc.chains[0].modelsXYZ.length : 0;
  
  // add code to load multiple models...
 
  
  var newChains = [], chainRef, rC, m1;
  for (c=0; c<struc.chains.length; c++) {
    currentChain = struc.chains[c];
    chainRef = currentChain, rC = currentChain.molecules.length;
    for (m1=0; m1<rC; m1++) {
      currentChain.molecules[m1].chain_alt = currentChain.molecules[m1].chain;
      currentChain.molecules[m1].chain = chainRef;
      if ( ((! currentChain.molecules[m1].next && m1 < rC-1) || (! currentChain.molecules[m1].previous && m1 != 0)) && 
           (! (currentChain.molecules[m1].water || currentChain.molecules[m1].ligand) || (m1 && currentChain.molecules[m1-1].name != currentChain.molecules[m1].name) )
         ) {
            chainRef = new add_chainObject((chainRef.name).replace(/^\s+|\s+$/g, ""), chainRef.entry); chainRef.isHet = false;
            newChains.push(chainRef);
            if (! currentChain.molecules[m1].previous) currentChain.molecules[m1].chain = chainRef;
         }
    }
  }
  
  if (newChains.length) {
    Array.prototype.push.apply(this.chains, newChains);
    Array.prototype.push.apply(struc.chains, newChains);
    var tmp = [];
    for (c=0; c<struc.chains.length; c++) {
      Array.prototype.push.apply(tmp, struc.chains[c].molecules);
      struc.chains[c].molecules = [];
      struc.chains[c].modelsXYZ_old = struc.chains[c].modelsXYZ; struc.chains[c].modelsXYZ = [];
      for (m1=0; m1<struc.chains[c].modelsXYZ_old.length; m1++) struc.chains[c].modelsXYZ.push([]);
      struc.chains[c].atoms = [];
    }
    for (m1=0; m1<tmp.length; m1++) {
      tmp[m1].chain.molecules.push(tmp[m1]);
      for (i=0; i<tmp[m1].atoms.length; i++) {
        atom = tmp[m1].atoms[i].xyz;
        for (c=0; c<tmp[m1].chain_alt.modelsXYZ_old.length; c++) {
          if (c >= tmp[m1].chain.modelsXYZ.length) tmp[m1].chain.modelsXYZ.push([]); // find a more efficient spot to put this --> maybe this chain isn't part of struc.chains???
          tmp[m1].chain.modelsXYZ[c].push(
            tmp[m1].chain_alt.modelsXYZ_old[c][atom], 
            tmp[m1].chain_alt.modelsXYZ_old[c][atom+1], 
            tmp[m1].chain_alt.modelsXYZ_old[c][atom+2]
          );
        }
        tmp[m1].atoms[i].xyz = tmp[m1].chain.modelsXYZ[0].length-3;
        tmp[m1].atoms[i].chain = tmp[m1].chain; 
      }
      Array.prototype.push.apply(tmp[m1].chain.atoms, tmp[m1].atoms);
      delete tmp[m1].chain_alt;
    }
    
    for (c=0; c<struc.chains.length; c++) delete struc.chains[c].modelsXYZ_old;
    
    for (c=0; c<struc.chains.length; c++) {
      if (struc.chains[c].molecules.length == 0) {struc.chains.splice(c, 1); c--; continue;}
      if (struc.chains[c].molecules[0].water || struc.chains[c].molecules[0].ligand) {
        struc.chains[c].name = (struc.chains[c].name ? struc.chains[c].name+" - " : "") + struc.chains[c].molecules[0].name;
        struc.chains[c].isHet = true;
      }
    }
  }
  
  return struc;
};

add_atomObject = function (Xpos, AN, element, molObj, chainObj) {
  this.xyz = Xpos; // this should become an idx
  this.element = element.charAt(0).toUpperCase() + element.slice(1).toLowerCase();
  this.atomName = AN;
  this.molecule = molObj;
  this.chain = chainObj;
  this.radius = 0.0;
  this.AID = 0;
}

add_molObject = function (name, id, chain) {
  this.atoms = [];
  this.name = name;
  this.id = id;
  this.RSID = id;
  this.chain = chain;
  this.ligand = true;
  this.water = false;
  this.next = null, this.previous = null;
  this.sndStruc = 1;
  this.xna = false;
}

add_chainObject = function (name, entry) {
  this.name = name;
  this.authName = name;
  this.molecules = [];
  this.entry = entry;
  this.modelsXYZ = [[]];
  this.atoms = [];
  this.bonds = [];
  this.bondsOK = false;
  this.isHet = true;
}

add_entryObject = function (meta) { // this should become a structure object instead --> models should only be virtual; i.e. only the coordinates should be saved, the structure (chain->residue->atom) is determined by the initial model
  this.chains = [];
  this.meta = meta || {};
};

isNumberFun=function(n) {return ! isNaN(parseFloat(n)) && isFinite(n);}

generateSurfaces_pdb = function(chain,type) {
	var c, surf, surfaces = [], surfaces2 = [], verts = 0, idcs = 0, settings, alpha = false;
  var surface_colors = {"protein" : [255,255,0,70], "compound" : [0,255,0,70], "nucleic" : [64,224,208,70], "ion" : [255,128,0,70]};
  
  surfaces_all = coarseSurface_pdb(chain, 1, 1.4, null, type);

  for (var key in surfaces_all) {
  	var surf = surfaces_all[key];
  	surf.rgba = surface_colors[key];
  	surfaces.push(surf);
    verts += surf.vertices.length;
    idcs += surf.faces.length*3;
    alpha = alpha || surf.rgba[3] != 255;
  }

  var vertices = new Float32Array(verts*7); // x, y, z, nx, ny, nz, rgba
  var vertices8 = new Uint8Array(vertices.buffer);
  var indices = new Uint32Array(idcs);
  var m=0, m8=0, s, rgba, offset = 0, i=0;
		
  for (s=0; s<surfaces.length; s++) {
    surf = surfaces[s]; rgba = surf.rgba;
    for (c=0; c<surf.vertices.length; c++, m8 += 28) {
      vertices[m++] = surf.vertices[c][0];
      vertices[m++] = surf.vertices[c][1];
      vertices[m++] = surf.vertices[c][2];
            
      vertices[m++] = surf.normals[c][0];
      vertices[m++] = surf.normals[c][1];
      vertices[m++] = surf.normals[c][2];
            
            
      vertices8[m8+24] = rgba[0];
      vertices8[m8+25] = rgba[1];
      vertices8[m8+26] = rgba[2];
      vertices8[m8+27] = rgba[3];
      m++; // color
    }
          
    for (c=0; c<surf.faces.length; c++) {
      indices[i++] = surf.faces[c][0]+offset; indices[i++] = surf.faces[c][1]+offset; indices[i++] = surf.faces[c][2]+offset;
    }
    offset += surf.vertices.length;
  }
  
  
  
  for (s=0; s<surfaces2.length; s++) {
    surf = surfaces2[s];
    
    vertices.set(surf.vBuffer, m);
    m += surf.vBuffer.length;
    m8 += surf.vBuffer.length*4;
    
    for (c=0; c<surf.iBuffer.length; c++) indices[i++] = surf.iBuffer[c]+offset;
    
    offset += surf.vBuffer.length/7;
  }
  
  buffer_check_json = {vP: 0, iP: 0}; // surfaces
  buffer_check_json.alphaMode = true;
  buffer_check_json.vertexBuffer = Array.prototype.slice.call(vertices);
  buffer_check_json.vertexBuffer8 = Array.prototype.slice.call(vertices8);
  buffer_check_json.indexBuffer = Array.prototype.slice.call(indices);
 
  return buffer_check_json
}

coarseSurface_pdb = function(chain, res, probeR, settings,type) {
  settings = settings || {};
  
  var atoms_all = {};
  
  atoms_all[type] = chain.molecules[0].atoms
  
  var inv_res = 1 / res;
  // make this also work with res not being a number, but an array of integers, so Nx, Ny, Nz, where this represents the number of voxels...
  
  var modelsXYZ = chain.modelsXYZ[0];
  
  surfaces_all = {};
  
  for (var at in atoms_all) {
  	var atoms = atoms_all[at];
  	if (atoms.length > 0) {
  		var geomRanges = [1e99, -1e99, 1e99, -1e99, 1e99, -1e99], a, tmp1, elements = {}, rX, rY, rZ, dX, dY, dZ, gs, grid_r = {}, grid_a = {}, block_ranges = [0, 0, 0, 0, 0, 0],
  	  gr, ga, r, aX, aY, aZ, xi, yi, zi, mp, xb, yb, zb, dx, dy, dz, grid_x, grid_y, vdwR = {
  	    DUMMY: 1.7, 
  	    H: 1.09, 
  	    D: 1.09, 
  	    C: 1.7, 
  	    N: 1.55, 
  	    O: 1.52, 
  	    S: 1.8,
  	    Cl: 1.75,
  	    B: 1.8,
  	    P: 1.8,
  	    Fe: 1.8,
  	    Ba: 1.8,
  	    So: 1.8,
  	    Mg: 1.8,
  	    Zn: 1.8,
  	    Cu: 1.4,
  	    Ni: 1.8,
  	    Br: 1.95,
  	    Ca: 1.8,
  	    Mn: 1.8,
  	    Al: 1.8,
  	    Ti: 1.8,
  	    Cr: 1.8,
  	    Ag: 1.8,
  	    F: 1.47,
  	    Si: 1.8,
  	    Au: 1.8,
  	    I: 2.15,
  	    Li: 1.8,
  	    He: 1.8,
  	    Se: 1.9
  	  };
  	      
  	  for (a=0; a<atoms.length; a++) {
  	    tmp1 = modelsXYZ[atoms[a].xyz];
  	    if (tmp1 < geomRanges[0]) geomRanges[0] = tmp1;
  	    if (tmp1 > geomRanges[1]) geomRanges[1] = tmp1;
  	        
  	    tmp1 = modelsXYZ[atoms[a].xyz+1];
  	    if (tmp1 < geomRanges[2]) geomRanges[2] = tmp1;
  	    if (tmp1 > geomRanges[3]) geomRanges[3] = tmp1;
  	        
  	    tmp1 = modelsXYZ[atoms[a].xyz+2];
  	    if (tmp1 < geomRanges[4]) geomRanges[4] = tmp1;
  	    if (tmp1 > geomRanges[5]) geomRanges[5] = tmp1;
  	        
  	    elements[atoms[a].element] = 1;
  	  }
  	      
  	      
  	  rX = Math.ceil((geomRanges[1]-geomRanges[0] + (2*probeR) + (2*1.8))/res)+2;
  	  rY = Math.ceil((geomRanges[3]-geomRanges[2] + (2*probeR) + (2*1.8))/res)+2;
  	  rZ = Math.ceil((geomRanges[5]-geomRanges[4] + (2*probeR) + (2*1.8))/res)+2;

  	  dX = -geomRanges[0] + probeR + 1.8 + res;
  	  dY = -geomRanges[2] + probeR + 1.8 + res;
  	  dZ = -geomRanges[4] + probeR + 1.8 + res;
  	      
  	  gs = rX*rY*rZ;

  	  for (a in elements) {
  	    grid_r[a] = new Float32Array(gs);
  	    grid_a[a] = [];
  	    for (xi=0; xi<gs; xi++) {
  	      grid_r[a][xi] = 1e99;
  	      grid_a[a].push(null);
  	    }
  	    elements[a] = a in vdwR ? vdwR[a] : vdwR.DUMMY;
  	  }

  	  for (a=0; a<atoms.length; a++) {
  	    gr = grid_r[atoms[a].element];
  	    ga = grid_a[atoms[a].element];
  	    r = elements[atoms[a].element];
  	        
  	    aX = modelsXYZ[atoms[a].xyz]   + dX;
  	    aY = modelsXYZ[atoms[a].xyz+1] + dY;
  	    aZ = modelsXYZ[atoms[a].xyz+2] + dZ;

  	    block_ranges[0] = 0; block_ranges[1] = rX;
  	    block_ranges[2] = 0; block_ranges[3] = rY;
  	    block_ranges[4] = 0; block_ranges[5] = rZ;
  	        
  	    for (xi=block_ranges[0]; xi<block_ranges[1]; xi++) {
  	      xb = xi*res;
  	      for (yi=block_ranges[2]; yi<block_ranges[3]; yi++) {
  	        yb = yi*res;
  	        for (zi=block_ranges[4]; zi<block_ranges[5]; zi++) {
  	          zb = zi*res;
  	             
  	          mp = xi + rX * (yi + rY * zi);
  	              
  	          dx = aX-xb; dy = aY-yb; dz = aZ-zb;
  	          tmp1 = dx*dx + dy*dy + dz*dz;
  	          if (tmp1 < gr[mp]) {
  	            gr[mp] = tmp1; ga[mp] = r;
  	          }
  	        }
  	      }
  	        
  	    }        
  	  }
  	      
  	  // now that for each grid point & element the nearest atom is calculated --> map it back to the surface...
  	      
  	  grid_x = new Float32Array(gs);
  	      
  	  for (xi=0; xi<gs; xi++) {
  	    mp = 1e99;
  	    for (a in elements) {
  	      r = Math.sqrt(grid_r[a][xi]) - (probeR + grid_a[a][xi]); // distance from point - grid
  	      if (r < mp) mp = r;
  	    }
  	    grid_x[xi] = mp * inv_res;
  	  }
  	  
  	  if (! settings.deproj && ! settings.sas) {
  	    var probeR_alt = probeR/res;
  	    for (xi=0; xi<gs; xi++) grid_x[xi] += probeR_alt;
  	  }
  	  
  	  var surf = polygonize_pdb([rX, rY, rZ], grid_x, 0.0);
  	  
  	  taubinSmoothing_pdb(surf.vertices, surf.faces, .5, -.53, 10);

  	  var i, faces, f, normals = [], normal;
  	  for (i=0; i<surf.vertices.length; i++) normals.push([0, 0, 0]);
  	  
  	  for (i in surf.vertexIndex) {
  	    normal = normals[surf.vertexIndex[i][0]];
  	    faces = surf.vertexIndex[i][1];
  	    for (f=0; f<faces.length; f++) vec3.add(normal, normal, surf.face_normals[faces[f]]);
  	    vec3.normalize(normal, normal);
  	  }
  	  surf.normals = normals;
  	  
  	  taubinSmoothing_pdb(surf.normals, surf.faces, .5, -.53, 10);
  	  
  	  if (settings.deproj) {
  	    
  	    var sf = 1./res;
  	  
  	    for (i=0; i<normals.length; i++) {
  	      surf.vertices[i][0] = ((surf.vertices[i][0] - normals[i][0] * probeR * sf) * res) - dX;
  	      surf.vertices[i][1] = ((surf.vertices[i][1] - normals[i][1] * probeR * sf) * res) - dY;
  	      surf.vertices[i][2] = ((surf.vertices[i][2] - normals[i][2] * probeR * sf) * res) - dZ;
  	    }
  	    
  	  }
  	  else {
  	    
  	    for (i=0; i<normals.length; i++) {
  	      surf.vertices[i][0] = ((surf.vertices[i][0]) * res) - dX;
  	      surf.vertices[i][1] = ((surf.vertices[i][1]) * res) - dY;
  	      surf.vertices[i][2] = ((surf.vertices[i][2]) * res) - dZ;
  	    }
  	    
  	  }
  	  surfaces_all[at] = surf;
  	} 
  }
  
  if ("NoKey" in surfaces_all) return surfaces_all["NoKey"];
  else return surfaces_all;
}

function polygonize_pdb(size, gridVals, isolevel, selectFunc) {
	var cubeIndex = 0, vertlist = [], i, x, y, z, v0 = [0, 0, 0], v1 = [0, 0, 0], iv = 0, faces = [], vertexIndex = {}, vertices = [], face_normals = [], idx, vert1, vert2, vert3;
	var grid0, grid1, grid2, grid3, grid4, grid5, grid6, grid7, idx1, idx2, idx3;
	
  for (i=0; i<12; i++) vertlist.push([0.0, 0.0, 0.0]);
  
  for (z=0; z<size[2]-1; z++) {
    for (y=0; y<size[1]-1; y++) {
      for (x=0; x<size[0]-1; x++) {

        if (selectFunc) {
          grid0 = selectFunc(x,   y,   z);
          grid1 = selectFunc(x+1, y,   z);
          grid2 = selectFunc(x+1, y,   z+1);
          grid3 = selectFunc(x,   y,   z+1);
          grid4 = selectFunc(x,   y+1, z);
          grid5 = selectFunc(x+1, y+1, z);
          grid6 = selectFunc(x+1, y+1, z+1);
          grid7 = selectFunc(x,   y+1, z+1);
        }
        else {
          grid0 = x + size[0] * (y + size[1] * z);
          grid1 = (x+1) + size[0] * (y + size[1] * z);
          grid2 = (x+1) + size[0] * (y + size[1] * (z+1));
          grid3 = x + size[0] * (y + size[1] * (z+1));
          grid4 = x + size[0] * ((y+1) + size[1] * z);
          grid5 = (x+1) + size[0] * ((y+1) + size[1] * z);
          grid6 = (x+1) + size[0] * ((y+1) + size[1] * (z+1));
          grid7 = x + size[0] * ((y+1) + size[1] * (z+1));
        }
        
        cubeIndex = 0;
        if (gridVals[grid0] < isolevel) cubeIndex |= 1;
      	if (gridVals[grid1] < isolevel) cubeIndex |= 2;
	      if (gridVals[grid2] < isolevel) cubeIndex |= 4;
	      if (gridVals[grid3] < isolevel) cubeIndex |= 8;
	      if (gridVals[grid4] < isolevel) cubeIndex |= 16;
	      if (gridVals[grid5] < isolevel) cubeIndex |= 32;
	      if (gridVals[grid6] < isolevel) cubeIndex |= 64;
	      if (gridVals[grid7] < isolevel) cubeIndex |= 128;

        
	      if (edgeTable[cubeIndex] === 0) continue;
  
	      if (edgeTable[cubeIndex] & 1) {
          v0[0] = x; v0[1] = y; v0[2] = z; // grid0
          v1[0] = x+1; v1[1] = y; v1[2] = z; // grid1
          iv = (isolevel-gridVals[grid0]) / (gridVals[grid1]-gridVals[grid0]);
          vec3.lerp(vertlist[0], v0, v1, iv);
        }
	      if (edgeTable[cubeIndex] & 2) {
          v0[0] = x+1; v0[1] = y; v0[2] = z; // grid1
          v1[0] = x+1; v1[1] = y; v1[2] = z+1; // grid2
          iv = (isolevel-gridVals[grid1]) / (gridVals[grid2]-gridVals[grid1]);
          vec3.lerp(vertlist[1], v0, v1, iv);
        }
	      if (edgeTable[cubeIndex] & 4) {
          v0[0] = x+1; v0[1] = y; v0[2] = z+1; // grid2
          v1[0] = x; v1[1] = y; v1[2] = z+1; // grid3
          iv = (isolevel-gridVals[grid2]) / (gridVals[grid3]-gridVals[grid2]);
          vec3.lerp(vertlist[2], v0, v1, iv);
        }
	      if (edgeTable[cubeIndex] & 8) {
          v0[0] = x; v0[1] = y; v0[2] = z+1; // grid3
          v1[0] = x; v1[1] = y; v1[2] = z; // grid0
          iv = (isolevel-gridVals[grid3]) / (gridVals[grid0]-gridVals[grid3]);
          vec3.lerp(vertlist[3], v0, v1, iv);
        }
	      if (edgeTable[cubeIndex] & 16) {
          v0[0] = x; v0[1] = y+1; v0[2] = z; // grid4
          v1[0] = x+1; v1[1] = y+1; v1[2] = z; // grid5
          iv = (isolevel-gridVals[grid4]) / (gridVals[grid5]-gridVals[grid4]);
          vec3.lerp(vertlist[4], v0, v1, iv);
        }
	      if (edgeTable[cubeIndex] & 32) {
          v0[0] = x+1; v0[1] = y+1; v0[2] = z; // grid5
          v1[0] = x+1; v1[1] = y+1; v1[2] = z+1; // grid6
          iv = (isolevel-gridVals[grid5]) / (gridVals[grid6]-gridVals[grid5]);
          vec3.lerp(vertlist[5], v0, v1, iv);
        }
	      if (edgeTable[cubeIndex] & 64) {
          v0[0] = x+1; v0[1] = y+1; v0[2] = z+1; // grid6
          v1[0] = x; v1[1] = y+1; v1[2] = z+1; // grid7
          iv = (isolevel-gridVals[grid6]) / (gridVals[grid7]-gridVals[grid6]);
          vec3.lerp(vertlist[6], v0, v1, iv);
        }
	      if (edgeTable[cubeIndex] & 128) {
          v0[0] = x; v0[1] = y+1; v0[2] = z+1; // grid7
          v1[0] = x; v1[1] = y+1; v1[2] = z; // grid4
          iv = (isolevel-gridVals[grid7]) / (gridVals[grid4]-gridVals[grid7]);
          vec3.lerp(vertlist[7], v0, v1, iv);
        }
	      if (edgeTable[cubeIndex] & 256) {
          v0[0] = x; v0[1] = y; v0[2] = z; // grid0
          v1[0] = x; v1[1] = y+1; v1[2] = z; // grid4
          iv = (isolevel-gridVals[grid0]) / (gridVals[grid4]-gridVals[grid0]);
          vec3.lerp(vertlist[8], v0, v1, iv);
        }
	      if (edgeTable[cubeIndex] & 512) {
          v0[0] = x+1; v0[1] = y; v0[2] = z; // grid1
          v1[0] = x+1; v1[1] = y+1; v1[2] = z; // grid5
          iv = (isolevel-gridVals[grid1]) / (gridVals[grid5]-gridVals[grid1]);
          vec3.lerp(vertlist[9], v0, v1, iv);
        }
	      if (edgeTable[cubeIndex] & 1024) {
          v0[0] = x+1; v0[1] = y; v0[2] = z+1; // grid2
          v1[0] = x+1; v1[1] = y+1; v1[2] = z+1; // grid6
          iv = (isolevel-gridVals[grid2]) / (gridVals[grid6]-gridVals[grid2]);
          vec3.lerp(vertlist[10], v0, v1, iv);
        }
	      if (edgeTable[cubeIndex] & 2048) {
          v0[0] = x; v0[1] = y; v0[2] = z+1; // grid3
          v1[0] = x; v1[1] = y+1; v1[2] = z+1; // grid7
          iv = (isolevel-gridVals[grid3]) / (gridVals[grid7]-gridVals[grid3]);
          vec3.lerp(vertlist[11], v0, v1, iv);
        }

	      for (i=0; triTable[16 * cubeIndex + i] != -1; i+=3) {
          vert1 = vertlist[triTable[16 * cubeIndex + i + 1]];
          vert2 = vertlist[triTable[16 * cubeIndex + i + 2]];
          vert3 = vertlist[triTable[16 * cubeIndex + i]];

          // represent the position as an index with respect to the grid size
          idx1 = (vert1[0] | 0) + size[0] * ((vert1[1] | 0) + size[1] * (vert1[2] | 0));
          idx2 = (vert2[0] | 0) + size[0] * ((vert2[1] | 0) + size[1] * (vert2[2] | 0));
          idx3 = (vert3[0] | 0) + size[0] * ((vert3[1] | 0) + size[1] * (vert3[2] | 0));

          var triangle = [-1, -1, -1];
          
          // vertices that map to the same index (i.e. have the same or have virtually the same position), are not added again, but are instead referenced (i.e. via indexed buffers)
          
          if (! vertexIndex[idx1]) {
            vertexIndex[idx1] = [vertices.length, []];
            vertices.push([vert1[0], vert1[1], vert1[2]]);
          }
          vertexIndex[idx1][1].push(faces.length);
          
          if (! vertexIndex[idx2]) {
            vertexIndex[idx2] = [vertices.length, []];
            vertices.push([vert2[0], vert2[1], vert2[2]]);
          }
          vertexIndex[idx2][1].push(faces.length);
          
          if (! vertexIndex[idx3]) {
            vertexIndex[idx3] = [vertices.length, []];
            vertices.push([vert3[0], vert3[1], vert3[2]]);
          }
          vertexIndex[idx3][1].push(faces.length);

          
          if (selectFunc) {
            triangle[0] = vertexIndex[idx3][0];
            triangle[1] = vertexIndex[idx2][0];
            triangle[2] = vertexIndex[idx1][0];
            vec3.sub(v0, vert2, vert1); vec3.sub(v1, vert3, vert1); var normal = [0.0, 0.0, 0.0]; vec3.cross(normal, v1, v0);
          }
          else {
            triangle[0] = vertexIndex[idx1][0];
            triangle[1] = vertexIndex[idx2][0];
            triangle[2] = vertexIndex[idx3][0];
            vec3.subtract(v0, vert2, vert1); vec3.subtract(v1, vert3, vert1); var normal = [0.0, 0.0, 0.0]; vec3.cross(normal, v0, v1);
          }
          
          faces.push(triangle);
          face_normals.push(normal);
        }
      }
    }
  }
  
  return {vertices: vertices, faces: faces, vertexIndex: vertexIndex, face_normals: face_normals};
};



// ** performs taubin smoothing for geometry (such as coarse/ccp4 surface) **
taubinSmoothing_pdb = function(vertices, faces, lambda, mu, iter) {
  var i, f, v, avgCoord = [0.0, 0.0, 0.0, 0], ref, tmp;
  var vertexInfo = [], vertexTemp = [];
      
  for (v=0; v<vertices.length; v++) {
    vertexInfo.push({});
    vertexTemp.push([0.0, 0.0, 0.0]);
  }
      
  for (f=0; f<faces.length; f++) {
    vertexInfo[faces[f][0]][faces[f][1]] = true; vertexInfo[faces[f][0]][faces[f][2]] = true;
    vertexInfo[faces[f][1]][faces[f][0]] = true; vertexInfo[faces[f][1]][faces[f][2]] = true;
    vertexInfo[faces[f][2]][faces[f][0]] = true; vertexInfo[faces[f][2]][faces[f][1]] = true;
  }
  for (v=0; v<vertices.length; v++) vertexInfo[v] = Object.keys(vertexInfo[v]);
      
  var smoothFunc = function(factor) {
    for (v=0; v<vertices.length; v++) {
      avgCoord[0] = 0.0; avgCoord[1] = 0.0; avgCoord[2] = 0.0; avgCoord[3] = 0;
      for (i=0; i<vertexInfo[v].length; i++) {
        ref = vertices[vertexInfo[v][i]];
        avgCoord[0] += ref[0]; avgCoord[1] += ref[1]; avgCoord[2] += ref[2]; avgCoord[3] += 1;
      }
      if (avgCoord[3] == 0) continue;
      tmp = 1./avgCoord[3];
      avgCoord[0] *= tmp; avgCoord[1] *= tmp; avgCoord[2] *= tmp;
          
      vertexTemp[v][0] = vertices[v][0] + factor * (avgCoord[0] - vertices[v][0]);
      vertexTemp[v][1] = vertices[v][1] + factor * (avgCoord[1] - vertices[v][1]);
      vertexTemp[v][2] = vertices[v][2] + factor * (avgCoord[2] - vertices[v][2]);
      
    }
  };
      
   for (var it=0; it<iter; it++) {
     smoothFunc(lambda);
     for (v=0; v<vertices.length; v++) {vertices[v][0] = vertexTemp[v][0]; vertices[v][1] = vertexTemp[v][1]; vertices[v][2] = vertexTemp[v][2];}
     smoothFunc(mu);
    for (v=0; v<vertices.length; v++) {vertices[v][0] = vertexTemp[v][0]; vertices[v][1] = vertexTemp[v][1]; vertices[v][2] = vertexTemp[v][2];}
  }
}
