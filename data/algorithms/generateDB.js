


//this is the main database generation algorithm. It is run in combination with generateDBI.pl file, which generates input for this. It can be run in parallel.

var zlib = require('zlib');
var exec = require('child_process').exec;
var readline = require('readline');
var fs = require('fs');
var http = require('http');
var protein = process.argv[2];//"3kej";
var chain = process.argv[3];//"A";
var stream = require('stream');
var PDBnameClean = protein+"_"+chain;	
var fex = require('file-exists');

console.log("Currently processing: "+protein+" "+chain);

// call this to decide upon computation..
initDatafiles(protein,chain);


function initDatafiles(protein, chain){


    if (process.argv[6] != "t"){

	// go compute in the callbacks
	read_gPDB(protein,chain);

    }else{
	
	
	// grid url
	var urlG = "/api/probisdb?pdbid="+protein+"&chain="+chain+"&key=grid";

	// ligand url
	var urlL = "/api/probisdb?pdbid="+protein+"&chain="+chain+"&key=ligands";

	//var url = "http://insilab.org/api/predb?pdbid="+protein+"&chain="+chain;
	var gridout = '../grid_files'+'/tmp_'+protein+'_'+chain+'.pdb';

	var ligfile = '../lig_files'+'/lig_'+protein+'_'+chain+'.json';
	
	var PDBname = "../PDBs/"+protein+"_"+chain+".pdb";

	var http = require('http');
	
	var gunzip = require('zlib').createGunzip();
	
	var options = {
	    host: 'insilab.org',
	    path: urlL
	}
	var request = http.request(options, function (res) {
	    var body = '';

	    res.pipe(gunzip);

	    gunzip.on('data', function (data) {
		body += data;
	    });

	    gunzip.on('end', function() {


		// write ligands to file..
		
		try{


		    // do the ligands
		    
		    zlib.gzip(split_ligands(JSON.parse(body)),function(err,res){
			
			if (err) throw err;
			
			fs.writeFile(ligfile+".gz",res);
			
		    });


		    // do the grids
		    //fs.writeFile(gridout, reformat_grid(urlG));
		    
		    reformat_grid(urlG, function (result) {
			zlib.gzip(result,function(err,res){			    
			    if (err) throw err;			    
			    fs.writeFile(gridout+".gz",res);			    
			});
		    });

		    
		}catch(err){
		    console.log ("No file in the API currently..");
		}
		
	    });
	});
	gunzip.on('error', function (e) {
	    console.log(e.message);
	});
	request.end();
	
	//}

    }
    
}

function reformat_grid(urlgrid, cb){

    var http = require('http');
    var gunzip = require('zlib').createGunzip();
    
    var options = {
	host: 'insilab.org',
	path: urlgrid
    }
    var request = http.request(options, function (res) {
	var body = '';

	res.pipe(gunzip);

	gunzip.on('data', function (data) {
	    body += data;
	});

	gunzip.on('end', function() {

	    
	    var grid = JSON.parse(body);
	    
	    var currentMDL = "";
	    
	    var outfile = "";
	    
	    var counter  = 0;
	    
	    grid.forEach(function(ent){
		
		var joint = ent['bs_id']+" "+ent['ltype'];
		
		if ( joint != currentMDL ){

		    if (counter > 0){

			outfile += "REMARK "+joint+"\n";
			currentMDL = ent['bs_id']+" "+ent['ltype'];
			//console.log("Chainging to>>>"+ent['bs_id']+" "+ent['ltype'])

		    }else{

			//console.log("Chainging to>>>"+ent['bs_id']+" "+ent['ltype'])
			outfile += "REMARK "+joint+"\n";			
			currentMDL = ent['bs_id']+" "+ent['ltype'];
			
		    }
		    
		    //update it on each iteration..

		    
		}else{
		    
		    
		    var xcor = ent['x'].toFixed(3);
		    var ycor = ent['y'].toFixed(3);
		    var zcor = ent['z'].toFixed(3);

		    var spacing = Array(28).join(" ");
		    var precor = "ATOM"+Array(7).join(" ")+"1"+Array(4).join(" ")+"U"+Array(3).join(" ")+"DIK"+Array(6).join(" ")+"1"+Array(6).join(" ")+xcor+"  "+ycor+"  "+zcor+"\n"

		    outfile += precor; 
		    
		    //ATOM      1   U  DIK     1     -12.246  29.885 -26.752
		    //6, 3, 2, 5, 5
		}

		counter ++;
	    });	    

	    console.log(counter+" points in this  grid..");
	    
	    // to bo zazipano!..
	    // fs.writeFile(writeto, outfile);

	    // zlib.gzip(split_ligands(JSON.parse(outfile)),function(err,res){
	    
	    // 	if (err) throw err;
	    // 	fs.writeFile(writeto,res);
	    
	    // });


	    cb(outfile);


	    
	});
    });
    gunzip.on('error', function (e) {
	console.log(e.message);
    });
    request.end();
    
}

function split_ligands(ligs){
    //"Binding$Site$#_4","PTW:2003:G:1:0:3mnn.10.PTW.2003.G.G:4kgc.15.HRU.201.G.G",
    var finalObj = []; 
    counter = 0
    ligs.forEach(function(el){

	//console.log("Binding$Site$#_"+el['bs_id']+"_"+el['ltype']);
	//console.log(el['lig_id']);
	var bsID = "Binding$Site$#_"+el['bs_id']+"_"+el['ltype'];

	var ligID = el['lig_id'].toString();
	var pdb = el['pdb_file'].toString();

	
	finalObj.push([bsID,ligID,pdb]);
	//	console.log(finalObj);
	counter ++;
    });

    //console.log(finalObj);
    console.log("ligands generated.."+counter+" found!");

    return (JSON.stringify(finalObj));


}

function is_numeric(str){
    return /^\d+$/.test(str);
}

function getSNPjson(urlSNP, tmpSNP){

    http.get(urlSNP, function(res){
	
    	var body = '';
    	res.on('data', function(chunk){
            body += chunk;
    	});

    	res.on('end', function(){
            var gridDATA = JSON.parse(body);
    	    //sleepFor(400);
    	    fs.writeFile(tmpSNP, gridDATA.snps);
	    
    	});
    }).on('error', function(e){
    	console.log("Got an error: ", e);
	
    });

}

function fileExists(filePath)
{
    try
    {
        return fs.statSync(filePath).isFile();
    }
    catch (err)
    {
        return false;
    }
}

function puts(error, stdout, stderr) { console.log(stdout) }


function intersect(a, b) {
    var t;
    if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
    return a.filter(function (e) {
        if (b.indexOf(e) !== -1) return true;
    });
}

function sleepFor( sleepDuration ){
    var now = new Date().getTime();
    while(new Date().getTime() < now + sleepDuration){ /* do nothing */ } 
}


function clean(pdbname){
    exec("cd PDB_SNP_surface; ls | mawk '!/^snpV/ && !/"+pdbname+".pdb/ && !/"+pdbname+".snp/' | xargs rm"); 
}

var combine = function(a, size) {
    var fn = function(n, src, got, all) {
        if (n == 0) {
            if (got.length > 0) {
                all[all.length] = got;
            }
            return;
        }
        for (var j = 0; j < src.length; j++) {
            fn(n - 1, src.slice(j + 1), got.concat([src[j]]), all);
        }
        return;
    }
    var all = [];

    fn(size, a, [], all);
    
    all.push(a);
    return all;
}



function calculate_SNP(protein,chain, pdbcoordinates, gridcoordinates){

    // use pdb and gridcoodrinates to calculate PPIsnps, PIs and other stuff
    // also, here you get surface points! surface = [];
    // then read the SNP file and map SNPs to surface. Then write the snpV.

    var finalJSON1 = new Object(); //json container of snp info within PI.    
    var surface = [];
    var errorBound = process.argv[4];
    var countfile = "../PPISNP/PPISNPlist.txt";
    var nosurfaces = "../TMPfiles/nosurf.txt"; // for validation..
    var outfileSNP1 = "../PDB_SNP_surface/"+"snpV_"+protein+"_"+chain+".snpV";
    pdbcoordinates.forEach(function(tri) {

	gridcoordinates.forEach(function(triG){
	    
	    var dist = Math.sqrt(Math.pow((tri[1]-triG[1]),2) +
				 Math.pow((tri[2]-triG[2]),2) +
				 Math.pow((tri[3]-triG[3]),2))
	    //console.log(dist);
	    if (dist < errorBound){
		//add point mean to the Surface3[]
		surface.push(triG[0]+'_'+tri[0]); // + triG[0]
		//console.log(triG[0]+" "+tri[0]);
	    }
	});
	
    });
    // map snps..
    var tmpSNP1 = '../SNP_DATA_PIPELINE/FinalResults/SNP'+'/'+protein+'.txt';
    var instreamSNP1 = fs.createReadStream(tmpSNP1);
    instreamSNP1.on('error', function(){
	console.log("SNPs don't exist for this PDB..");
    });
    var outstreamSNP1 = new stream;
    var rlSNP1 = readline.createInterface(instreamSNP1, outstreamSNP1);
    var tmpSNPlist1 = [];
    var tmpSNPnames2 = [];
    
    rlSNP1.on('line', function(line3) {
	
	var snpEntries3 = line3.split(/\s+/);
	
	if (snpEntries3[0] == null){
	    console.log("SNPs not found for this entry.");

	}else if (snpEntries3[5] == chain){

	    tmpSNPlist1.push(snpEntries3[6]);
	    var tmp = snpEntries3[4]+"_"+snpEntries3[3]+"_"+snpEntries3[1];
	    var tmp2 = snpEntries3[0]+"_"+snpEntries3[1].replace("_","|")+"_"+snpEntries3[2]+"_"+snpEntries3[3]+"_"+snpEntries3[4]+"_"+snpEntries3[5]+"_"+snpEntries3[6]+"_"+snpEntries3[7];
	    //console.log(tmp2);
	    tmpSNPnames2.push(tmp2);

	}else{}

    });

    rlSNP1.on('close', function(){

	var uniqSurf = surface.reduce(function(a,b){
	    if (a.indexOf(b) < 0 ) a.push(b);
	    return a;
	},[]);


	var uniqSNP = tmpSNPlist1.reduce(function(a,b){
	    if (a.indexOf(b) < 0 ) a.push(b);
	    return a;
	},[]);

	finalJSON1.snploci = uniqSNP; //loci for intersection
	finalJSON1.surface = uniqSurf; //calculated surface points
	//console.log(finalJSON1.surface);
	finalJSON1.snpdata = tmpSNPnames2; //all data
	var PPISNP = [];
	uniqSurf.forEach(function(surfel){
	    if(uniqSNP.indexOf(surfel.split("_")[3]) != -1){
		
		PPISNP.push(surfel.split("_")[3]);
		
	    }
	});
	
	// var PPISNP = intersect(finalJSON1.snploci,function(x){return x.split("_")[2]}.apply(undefined, uniqSurf));

	finalJSON1.piSNP = PPISNP;
	
	if (finalJSON1.piSNP.length > 0){
	    
	    console.log("PPISNP found! "+finalJSON1.piSNP);
	}
	//tale finalJSON bo mel zraven se ligands data. finalJSON1.piSNP = object.ligands

	if (finalJSON1.piSNP != []){
	    finalJSON1.snpdata.forEach(function(entry){
		//console.log(entry);
		if (finalJSON1.piSNP.indexOf(entry.split("_")[6]) != -1 ){
		    
		    fs.appendFile(countfile, entry+'\n', function (err) {
			
		    });
		}			
	    });

	}

	if (finalJSON1.surface.length < 1){

	    fs.appendFile(nosurfaces, protein+" "+chain+'\n', function (err) {
		
	    });				
	}
	
	
	// write only if
	//write as table to a different file maybe?

	// zlib.gzip(JSON.stringify(finalJSON1), (err,res) => {
	    
	//     if (err) throw err;
	    
	//     fs.writeFile(outfileSNP1+".gz",res);
	    
	// });
	
	fs.writeFile(outfileSNP1, JSON.stringify(finalJSON1), function(err) {
	    if(err) {
		return console.log(err);
	    }

	    console.log("The file "+ outfileSNP1+" was saved..");
	}); 

    });

    // tukej pride konstrukcija snpV fajla, najprej pa pogleda preseke!
}

function read_gGRID(protein,chain,pdbc){

    var gridcoordinates = [];
    // reads gzipped grid file..
    // process line here
    var gridname = "../grid_files/tmp_"+protein+"_"+chain+".pdb.gz";
    //../pdb_whole/+slice(protein,0,2)+"/"+entfajl al kaj je ze..
    var lineReader = readline.createInterface({
	terminal: false,
	input: fs.createReadStream(gridname).pipe(zlib.createGunzip())
    });

    lineReader.on('line', function(line) {
	//take each 15th line..
	//if (linecounter%speed == 0){
	var TMPGRID1 = line.split(/\s+/);
	if (TMPGRID1.length == 3){
	    tempmodel = "";temptype = "";		    
	    tempmodel = "Binding$Site"+"$#_"+TMPGRID1[1] +"_"+ TMPGRID1[2];
	    console.log(tempmodel);
	}

	if (TMPGRID1[0] == "ATOM"){		
	    var PDBxg = TMPGRID1[5];
	    var PDByg = TMPGRID1[6];
	    var PDBzg = TMPGRID1[7];
	    gridcoordinates.push([tempmodel,PDBxg,PDByg,PDBzg]); // tempmodel
	}
	//}

    });
    
    lineReader.on('close', function()  {
	calculate_SNP(protein,chain,pdbc,gridcoordinates)
	//console.log(gridcoordinates)
    });

    lineReader.on('error', function()  {
	console.log('Could process the PDB..');

    });
    
    //calculate_SNP(protein,chain,pdbc,gridcoordinates)
}

function read_gPDB(protein,chain){

    
    var toread = "../pdb/"+protein.slice(1,3)+"/pdb"+protein+".ent.gz";
    console.log(toread);
    var PDBcoordinates = [];
    
    var lineReader = readline.createInterface({
	terminal: false,
	input: fs.createReadStream(toread).pipe(zlib.createGunzip())
    });
    
    lineReader.on('line', function(line) { 
	
	
    	var TMPpdb1 = line.split(/\s+/);
    	//console.log(TMPpdb1);
    	if (TMPpdb1[0] == "ATOM"){

    	    var PDBx = TMPpdb1[6];
    	    var PDBy = TMPpdb1[7];
    	    var PDBz = TMPpdb1[8];
    	    if (PDBx != null && PDBy != null && PDBz != null && TMPpdb1[4] == chain){
    		var tmp1 = [TMPpdb1[5],PDBx,PDBy,PDBz]; //5 is amino acid..
		PDBcoordinates.push(tmp1); // add for further editing.. 
    	    }
    	}
	
    });

    
    lineReader.on('close', function() {
	//console.log(PDBcoordinates);
	read_gGRID(protein,chain,PDBcoordinates)
    });

    lineReader.on('error', function() {
	console.log('Could process the PDB..');
	//	read_gGRID(protein,chain,pdbcoordinates)
    });
}

