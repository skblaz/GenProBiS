// this algorithm maps all the SNPs to their correcs pdb locations.

// by Blaz Skrlj and Nika Erzen of KI (2017)

const fs = require('fs')
const readline = require('readline')
const zlib = require('zlib')
const bresipath = "../BS_DATA/bresi/bresi_"+process.argv[2]+".json.gz";

//first process the pdb with the code already obtained!

var three2one = {"ALA" : "A", "ARG" : "R","ASN" : "N","ASP" : "D","ASX" : "B", "CYS" : "C","GLU" : "E","GLN" : "Q","GLX" : "Z", "GLY" : "G","HIS" : "H","ILE" : "I","LEU" : "L","LYS" : "K","MET" : "M","PHE" : "F", "PRO" : "P","SER" : "S","THR" : "T","TRP" : "W","TYR" : "Y","VAL" : "V"}

var one2three = {};
for (var key in three2one)
	one2three[three2one[key]] = key;
//process pdb

get_processed_pdb(process.argv[2], function(pdb_name,outJSON){

    // process mapping
    
    get_unirsid(pdb_name,outJSON,function(outJSON){

	//if (process.argv[3] == "--xml"){
	//console.log(outJSON)
	
	    zlib.gzip(JSON.stringify(outJSON),function(err,res){
		
		if (err) throw err;
		
		fs.writeFile("../PDB_SNP_MAPPING_ALL/JSON/"+pdb_name+".json.gz", res)
	    	
	    });	    

	    //reform the JSON
	    
	    var mapBS = {}

	    outJSON.forEach(function(meh){

		if (meh['snp'] != undefined){
		    mapBS[meh['pdb']] = meh['snp']
		}
	    })
	    
	    // do the BS mapping

	    let lineReader = readline.createInterface({
		input: fs.createReadStream(bresipath).on('error',function(){
		}).pipe(zlib.createGunzip())
	    });

	    var bsjson = ""
	    lineReader.on('line', (line) => {
		bsjson += line

	    });

	    lineReader.on('close', () => {

		var outrows = "";
		JSON.parse(bsjson).forEach(function(bsaa){


		    // this part actually maps resi hash to bs hash, under asumption pdb resi are unique keys!
		    
		    if (bsaa['resi'] in mapBS){
			outrows += mapBS[bsaa['resi']]['uniprotid']+","+mapBS[bsaa['resi']]['gene']+","+mapBS[bsaa['resi']]['change']+","+mapBS[bsaa['resi']]['id']+","+mapBS[bsaa['resi']]['species']+","+pdb_name+","+bsaa['resi']+","+bsaa['bs_id']+","+bsaa['ltype']+"\n"

		    }
		    		    
		})

		//write to separate + to wholebase

		zlib.gzip(outrows,function(err,res){
		    
		    if (err) throw err;
		    
		    fs.writeFile("../PDB_SNP_MAPPING_ALL/READABLE/"+pdb_name+".txt.gz", res)

		});
		
		fs.appendFile("../PDB_SNP_MAPPING_ALL/WHOLE_BASE/wholebase.txt", outrows, function (err) {

		});	
	    })

	    // alternative implementation.

	// }else{
	    
	//     //map and output
	//     //console.log("Uniprot numbers obtained.")
	    
	//     map_to_pdb(pdb_name,outJSON,function(result,absolute, baselocs){
		
	// 	var uni_to_pdb = {};
	// 	var tofrontend = new Object();
	// 	var seqplot_info = {};
		
	// 	var index = 0
	// 	var unique_uni = outJSON.uniloc.sort().filter(uniq)

	// 	seqplot_info.lo = outJSON["lo"]
	// 	seqplot_info.aa = outJSON["aa"]
	// 	seqplot_info.missing = outJSON["missing"]
	// 	seqplot_info.deletion = outJSON["deletion"]
	// 	seqplot_info.deletionAA = outJSON["deletionAA"]
		
	// 	//to be re-done
	// 	var finalfrontend = []
		
	// 	for (var k=0; k< result.length;k++){
		    
	// 	    var frontend = {}
		    
	// 	    var missingTrigger = false
	// 	    var deletionTrigger = false
		    
	// 	    for (var l=0; l<outJSON['missing'].length;l++){
	// 		if (result[k] == outJSON['missing']){
	// 		    missingTrigger = true
	// 		}
	// 	    }

	// 	    for (var l=0; l<outJSON['deletion'].length;l++){
	// 		if (result[k] == outJSON['deletion']){
	// 		    deletionTrigger = true
	// 		}		    
	// 	    }

	// 	    // [{"uni":345, "pdb":"60A","resn":"VAL","missing":false,"deleted":false,"snp":{"uniprotid":"P00734","gene":"F2","change":"His605Gln","id":"rs368442575","species":"homo sapiens","variation":"variation60"}},...]
		    
	// 	    outJSON.snpdata.forEach(function(x){
	// 		parts = x.split("_")
	// 		if (parts[2].match(/\d+/)[0] == unique_uni[k]){
			    
	// 		    snpdict = {}
	// 		    snpdict['uniprotid'] = parts[0]
	// 		    snpdict['gene'] = parts[1]
	// 		    snpdict['change'] = parts[2]
	// 		    snpdict['id'] = parts[3]
	// 		    snpdict['species'] = parts[7]+" "+parts[8]
	// 		    frontend['uni'] = unique_uni[k]
	// 		    frontend['pdb'] = result[k]
	// 		    frontend['resn'] = outJSON['aa'][result[k]]
	// 		    frontend['missing'] = missingTrigger
	// 		    frontend['deletion'] = deletionTrigger
	// 		    frontend['snp'] = snpdict
			    
	// 		}
			
	// 	    })
		    
	// 	    //		console.log(baselocs)
		    
	// 	    finalfrontend.push(frontend)
		    
	// 	    // tukej kr nardi json al neki..
		    
	// 	    uni_to_pdb[unique_uni[k]] = result[k]
	// 	}

	// 	//console.log(absolute)
	// 	//console.log(result)
	// 	//take also
		
	// //	console.log(outJSON['unirange'])	    		
		
	// 	for (var k=0; k < baselocs.length; k++){

	// 	    var frontend = {}		
	// 	    var missingTrigger = false
	// 	    var deletionTrigger = false
		    
	// 	    for (var l=0; l<outJSON['missing'].length;l++){
	// 		if (result[k] == outJSON['missing']){
	// 		    missingTrigger = true
	// 		}
	// 	    }

	// 	    for (var l=0; l<outJSON['deletion'].length;l++){
	// 		if (result[k] == outJSON['deletion']){
	// 		    deletionTrigger = true
	// 		}		    
	// 	    }
		    
	// 	    //uniprot range, in od tu vzae.
		    
	// 	    frontend['uni'] = outJSON['unirange'][0]+parseInt(baselocs[k])		
	// 	    frontend['pdb'] = baselocs[k]
	// 	    frontend['resn'] = outJSON['aa'][baselocs[k]]
	// 	    frontend['missing'] = missingTrigger
	// 	    frontend['deletion'] = deletionTrigger

	// 	    finalfrontend.push(frontend)
	// 	}

	// 	seqplot_info.validated_locations = result
	// 	seqplot_info.absolute_locations = absolute

		
	// 	//  console.log(result, outJSON['lo'])

	// 	//tofrontend.seq_info = seqplot_info
	// 	//tofrontend.snpdata = outJSON.snpdata

		
	// 	zlib.gzip(JSON.stringify(finalfrontend),function(err,res){
		    
	// 	    if (err) throw err;
		    
	// 	    fs.writeFile("../PDB_SNP_MAPPING_ALL/JSON/"+pdb_name+".json.gz", res)
		    
		    
	// 	});
		

	// 	let lineReader = readline.createInterface({
	// 	    input: fs.createReadStream(bresipath).on('error',function(){
	// 		//console.log("couldnt read the file..", bresipath)
	// 	    }).pipe(zlib.createGunzip())
	// 	});


	// 	var bsjson = ""
	// 	lineReader.on('line', (line) => {
	// 	    bsjson += line
	// 	    //console.log(line)
	// 	});

	// 	lineReader.on('close', () => {

	// 	    bsjson = JSON.parse(bsjson);

	// 	    // console.log(bsjson)
	// 	    var cool_json = {}
		    
	// 	    bsjson.forEach(function(y){

	// 		cool_json[y['resi']] = [y['bs_id'],y['ltype']];
			
	// 	    });

	// 	    //PDB,Chain,UniProt,Gene,Mutation_accession,Species,UniLoc,RSID,BS,BSID,BS_type
	// 	    var outrows = "";
	// 	    var tabuLoc = [];
		    
	// 	    outJSON.snpdata.forEach(function(snpentry){

	// 		var parts = snpentry.split("_");
	// 		var location = parts[2].match(/\d+/)[0];
	// 		if (location in uni_to_pdb){

	// 		    if (uni_to_pdb[location] in cool_json){
				
	// 			outrows += parts[4]+","+parts[5]+","+parts[0]+","+parts[1]+","+parts[3]+","+parts[7]+"|"+parts[8]+","+location+","+uni_to_pdb[location]+","+"TRUE"+","+cool_json[uni_to_pdb[location]][0]+","+cool_json[uni_to_pdb[location]][1] + "\n"
								
	// 		    }else{
				
	// 			outrows += parts[4]+","+parts[5]+","+parts[0]+","+parts[1]+","+parts[3]+","+parts[7]+"|"+parts[8]+","+location+","+uni_to_pdb[location]+","+"FALSE"+","+"NA"+","+"NA"+"\n"
	// 		    }			    
	// 		}
			
	// 	    });

	// 	    //write this also 
	// 	    zlib.gzip(outrows,function(err,res){
			
	// 		if (err) throw err;
			
	// 		fs.writeFile("../PDB_SNP_MAPPING_ALL/READABLE/"+pdb_name+".txt.gz", res)

	// 	    });
		    
	//     	    fs.appendFile("../PDB_SNP_MAPPING_ALL/WHOLE_BASE/wholebase.txt", outrows, function (err) {

	//     	    });		    
	//     	});		
	//     });

	// }
    });

    
});
//}


function get_processed_pdb(pdb_argument, cb1){


    //e.g. 3kejA
    var pdb_id = pdb_argument;
    var toread = "../pdb/"+pdb_id.slice(1,3)+"/pdb"+pdb_id.slice(0,4)+".ent.gz";
    var lineReader = require('readline').createInterface({terminal: false,
							  input: require('fs').createReadStream(toread).pipe(zlib.createGunzip())
							 });

    var ldic = {};
    var locs = [];
    var aas = [];
    var caa = "";
    var cl = "";
    var cmax = 0;
    var srcon = 0;
    var missing = [];
    var uniTrigger = false;
    var delList = [];
    var delAAlist = [];
    
    lineReader.on('line', function (line) {

	var parts = line.split(/\s+/);
	
	if (parts[1] == "465" && parts.length == 6 && parts[3] == pdb_id.slice(4,5).toUpperCase()){
	    //console.log(parts[4],parts.length);
	    missing.push(parts[4]);
	    
	}

	// check atom
	if (parts[0]=="ATOM" && parts[4] == pdb_id.slice(4,5).toUpperCase()){

	    // iterate only over molecules
	    if (parts[3] != caa || parts[5] != cl){

		// remove multi chain problems
		if (cmax <= parseInt(parts[5]) || parts[5].match( /(A|B|C|D|E|F|G|H|I|J|K)/ )){
		    if (locs.indexOf(parts[5]) == -1) {
			locs.push(parts[5]);
			cmax = parseInt(parts[5]);
		    }
		}
	    }

	}
	// check atom
	else if (parts[0]=="ATOM" && (parts[4]).indexOf(pdb_id.slice(4,5).toUpperCase()) != -1){

	    // iterate only over molecules
	    if (parts[3] != caa || parts[5] != cl){

		// remove multi chain problems
		if (cmax <= parseInt(parts[4].substring(1)) || ((parts[4]).substring(1)).match( /(A|B|C|D|E|F|G|H|I|J|K)/ )){
		    if (locs.indexOf((parts[4]).substring(1)) == -1) {
			locs.push((parts[4]).substring(1));
			cmax = parseInt((parts[4]).substring(1));
		    }
		}
	    }

	}

	// update molecule parameters
	cl = parts[5];
	caa = parts[3];

	
	if (parts[0] == "SEQRES" && parts[2] == pdb_id.slice(4,5).toUpperCase()){
	    srcon++;
	    //console.log(parts);
	    var tmpc = 0;
	    parts.forEach(function(part){
		tmpc ++;
		if (tmpc>4 && part != ''){
		    aas.push(part);
		    //		    console.log(part);
		}
	    });
	}

	// only uniprot is valid!
	if (parts[0] == "DBREF" && parts[5] == "UNP"){
	    uniTrigger = true;
	}


	if (parts[0] == "SEQADV" && uniTrigger == true){

	    delList.push(parts[6]);
	    delAAlist.push(parts[5]);
	}
	
    });


    lineReader.on('close', function () {

	var counts = {};
	
	ldic["lo"] = locs;
	
	ldic["aa"] = aas;
	
	ldic["missing"] = missing;
	
	ldic['deletion'] = delList;
	
	ldic['deletionAA'] = delAAlist;

	cb1(pdb_argument,ldic); // this triggers the map part!
    });

    lineReader.on('error', function (line) {
	
	console.log("Couldn't read the file..");
    });

}


function map_to_pdb(pdb_id,data,cb){

    
    //get_surf(bresipath,(surf)=>{


    var unirsid = (function(wholedata){
	var uniloc = {}
        wholedata.forEach(function(ent){
	    uniloc[ent.split("_")[2].slice(5,ent.split("_")[2].length-3)] = ent.split("_")[6]
        });
	
        return uniloc
    })(data.snpdata);
    //console.log(unirsid)
    
    data = (function(data){
	var i=0;
	while (i<data["deletion"].length) {
	    if (["0","1","2","3","4","5","6","7","8","9"].indexOf(data["deletion"][i][0]) == -1) {
		data["deletion"].splice(i, 1);
		data["deletionAA"].splice(i, 1);
	    }
	    else
		i++;
	}
	return data

    })(data);

    var absLoc = (function(UniPdbMap, pdbZero){
	var locations = []
	for (var x1 in unirsid){

	    locations.push(parseInt(x1)-( parseInt(x1)-parseInt(unirsid[x1])+parseInt(pdbZero)))
	}

	return locations

    })(unirsid, data["lo"][0]);

    //console.log(absLoc)	

    RSIDlocs = [];
    BASElocs = []
    
    var mval =  (function(missing_vals){
	var missing_beg = [];

	for (var i=0; i<missing_vals.length; i++){
	    x = missing_vals[i];
	    if (parseInt(x.replace(/[^\d.-]/g, '')) <= parseInt((data['lo'][0]).replace(/[^\d.-]/g, '')) && ((x.replace(/[0-9]/g, '').replace("-","")) <= (data['lo'][0]).replace(/[0-9]/g, '').replace("-",""))){
		missing_beg.push(x);				
	    }			    
	}

	return missing_beg.length;

    })(data['missing']);

    absLoc = absLoc.map(function(ert){

	return ert + mval
	
    })

    //console.log(absLoc)

    function addSequence(location,value,missing,deletion,realind,ccount,beginnerMissing) {
	if (beginnerMissing) {
	    for (var i=0; i<absLoc.length; i++)
		absLoc[i] = absLoc[i];
	}
	else if (missing) {
	    for (var i=0; i<absLoc.length; i++)
		absLoc[i] = absLoc[i]-1;
	}
	var rgba = "";
	if (beginnerMissing) {
	    if (missing)
		rgba = "background-color: rgba(211,211,211,0.60);";
	    if (deletion)
		rgba = "background-color: rgba(211,211,211,0.60);";
	    if (absLoc.indexOf(realind) != -1)
		RSIDlocs.push(location);
	}
	else if (missing) {
	    rgba = "background-color: rgba(211,211,211,0.60);";
	    if (absLoc.indexOf(realind-1) != -1)
		RSIDlocs.push(location);
	}
	else if (deletion) {
	    rgba = "background-color: rgba(0,0,0,0.60);";
	    if (absLoc.indexOf(realind) != -1)
		RSIDlocs.push(location);
	}
	else if (absLoc.indexOf(realind) != -1 ) {
	    //rgba = "background-color: rgba(255, 0, 0, 0.20);";
	    RSIDlocs.push(location);
	}else{
	    BASElocs.push(location)
	}
	// else if (absLoc.indexOf(realind) == -1 && surf.indexOf(location) != -1)
	//     rgba = "background-color: rgba(0, 255, 0, 0.20);";
	// else if (absLoc.indexOf(realind) != -1 && surf.indexOf(location) == -1) {
	//     rgba = "background-color: rgba(0, 0, 255, 0.20);";
	//     RSIDlocs.push(location);
	// }
	//seqview += "<a  style='font-family:monospace;color:black;"+rgba+"' href='#' data-toggle='tooltip' title='"+location+"'>"+value+"</a>";
	//break lines
	if (ccount % 50 == 0 && ccount > 0){
	    if (location != "undefined" && location != undefined){
		//seqview += "<span style='color:gray;font-family:bold'>   "+location+"</span>"+"<br><br>";
	    }else{

		//seqview += "<span style='color:gray;font-family:bold'>   "+parseInt(location)+"</span>"+"<br><br>";
	    }
	}
	//if (ccount % 10 == 0){seqview += "   ";}
    }

    var loCounter = 0; var missingCounter = 0; var deletionCounter = 0; var realIndex = 0; var ccount = 1; var modified = 0; var begginerMissing = true;
    var aaLo = 0; var aaDel = 0;
    for (var k=0; k<(data["aa"].length+data["deletionAA"].length); k++) {
	var location = 1;
	var missingBool = false;
	var deletionBool = false;
	
	var intLo = parseInt(((data["lo"][loCounter-modified]) || "100000000").replace(/[^\d.-]/g, ''));
	var intMissing = parseInt(((data["missing"][missingCounter]) || "100000000").replace(/[^\d.-]/g, ''));
	var intDeletion = parseInt(((data["deletion"][deletionCounter]) || "100000000").replace(/[^\d.-]/g, ''));
	var letterLo = ((data["lo"][loCounter-modified]) ||"0").replace(/[0-9]/g, '').replace("-","");
	var letterMissing = ((data["missing"][missingCounter]) ||"0").replace(/[0-9]/g, '').replace("-","");
	var letterDeletion = ((data["deletion"][deletionCounter]) ||"0").replace(/[0-9]/g, '').replace("-","");
	if (intLo<intMissing && intLo<intDeletion) {
	    location = data["lo"][loCounter-modified];
	    begginerMissing = false;
	    //console.log(data["lo"][loCounter]);
	    loCounter++;
	}
	else if (intMissing<intLo && intMissing<intDeletion) {
	    missingBool = true;
	    location = data["missing"][missingCounter];
	    //console.log(data["missing"][missingCounter]);
	    missingCounter++;
	}
	else if (intDeletion<intLo && intDeletion<intMissing) {
	    deletionBool = true;
	    location = data["deletion"][deletionCounter];
	    deletionCounter++;
	}
	else if (intLo==intDeletion && intLo==intMissing) {
	    if (letterLo < letterMissing && letterLo < letterDeletion) {
		location = data["lo"][loCounter-modified];
		begginerMissing = false;
		//console.log(data["lo"][loCounter]);
		loCounter++;
	    }
	    else if (letterMissing < letterDeletion && letterMissing < letterLo) {
		missingBool = true;
		location = data["missing"][missingCounter];
		//console.log(data["missing"][missingCounter]);
		missingCounter++;
	    }
	    else if (letterDeletion < letterMissing && letterDeletion < letterLo) {
		deletionBool = true;
		location = data["deletion"][deletionCounter];
		//console.log(data["missing"][missingCounter]);
		deletionCounter++;
	    }
	}
	else if (intLo==intMissing) {
	    if (letterLo < letterMissing) {
		location = data["lo"][loCounter-modified];
		begginerMissing = false;
		//console.log(data["lo"][loCounter]);
		loCounter++;
	    }
	    else {
		missingBool = true;
		location = data["missing"][missingCounter];
		//console.log(data["missing"][missingCounter]);
		missingCounter++;
	    }
	}
	else if (intLo==intDeletion) {
	    if (letterLo < letterDeletion) {
		location = data["lo"][loCounter-modified];
		begginerMissing = false;
		//console.log(data["lo"][loCounter]);
		loCounter++;
	    }
	    else {
		deletionBool = true;
		location = data["deletion"][deletionCounter];
		//console.log(data["missing"][missingCounter]);
		deletionCounter++;
	    }
	}
	else if (intDeletion==intMissing) {
	    if (letterLo < letterMissing) {
		deletionBool = true;
		location = data["deletion"][deletionCounter];
		//console.log(data["missing"][missingCounter]);
		deletionCounter++;
	    }
	    else {
		missingBool = true;
		location = data["missing"][missingCounter];
		//console.log(data["missing"][missingCounter]);
		missingCounter++;
	    }
	}
	if (deletionBool) {
	    var aa = data["deletionAA"][aaDel];
	    aaDel++;
	}
	else {
	    var aa = data["aa"][aaLo];
	    aaLo++;
	}
	
	var value = three2one[aa]||"X";
	
	if (value=="X") {
	    location = location-1;
	    modified++;
	}

	
	addSequence(location,value,missingBool,deletionBool,realIndex,ccount,begginerMissing)

	

	ccount++;
	if (!missingBool || begginerMissing)
	    realIndex++;
    }

    //console.log(surf)
    //console.log(RSIDlocs)

    cb(RSIDlocs,absLoc,BASElocs);  //return this to the seqplot object..
    // });
}

function get_unirsid(pdb_name,SNPinfo,cb){

   // if (process.argv[3] == "--xml"){
	//read SNP file, extract locations!
	var pdbname = pdb_name.slice(0,4)
	var chain = pdb_name.slice(4,5)
	var SNPpath = "../SNP_DATA_PIPELINE/FinalResults/SNP/"+pdbname+".txt";
	var lineReader = readline.createInterface({
	    terminal: false,
	    input: fs.createReadStream(SNPpath)
	});

	var tmpobj = new Object()
    
    lineReader.on('line', function(line) { 

	var parts = line.split(" ");

	if (parts[5] == chain){
	    
	    if (!tmpobj.hasOwnProperty(parts[0])){
		tmpobj[parts[0]] = {}
	    }
	    
	    if (!tmpobj[parts[0]].hasOwnProperty(parts[6])){
		tmpobj[parts[0]][parts[6]] = []
	    }

	    tmpobj[parts[0]][parts[6]].push({'uniprotid' : parts[0], 'gene' : parts[1], 'change' : parts[2], 'id' : parts[3], 'species' : parts[7].split("_")[0]+" "+parts[7].split("_")[1]})
	    
	}
	
    });

    lineReader.on('close', function() {

	var d = '';
	fs.createReadStream("../SNP_DATA_PIPELINE/SIFTXML/PDB_UNI/"+pdbname+".json.gz")
	    .pipe(zlib.createGunzip())
	    .on('data', function (data){
		d += data.toString()
	    })
	    .on('end', function (){
		//		    console.log(d)
		var allrs = JSON.parse(d);

		var output = []
		//console.log(tmpobj);
		//		    console.log(tmpobj['P45452']['236'])
		for (key in tmpobj){
		    
		    allrs.forEach(function(obz){

			if (obz['chain'] == chain){
//			    console.log(obz['aaname'])
			    var skey = obz['pdbres']
			    var outdict = {}
			    
			    if (tmpobj[key].hasOwnProperty(skey)){

				//		    {"uni":345, "pdb":"60A","resn":"VAL","missing":false,"deleted":false,"snp":{"uniprotid":"P00734","gene":"F2","change":"His605Gln","id":"rs368442575","species":"homo sapiens"
				
				//				console.log("................")
				var tabulist = []

				tmpobj[key][skey].forEach(function(snpdata){
				    
//				    if (skey == '231'){console.log(snpdata)}
				    
				    if (tabulist.indexOf(snpdata['change']+snpdata['id']) == -1){
					tabulist.push(snpdata['change']+snpdata['id'])
					
					//					console.log(snpdata)
			    		outdict['uni'] = obz['unires'];
			    		outdict['pdb'] = skey
			    		if (obz['uniaaname'] != undefined)
			    			outdict['resn'] = one2three[obz['uniaaname']]||"X"
							outdict['counter'] = obz['counter']

					outdict['respdb'] = obz['pdbaaname']

			    		if (SNPinfo['missing'].indexOf(skey) != -1){

			    		    outdict['missing'] = true
					    
			    		}else{
					    
			    		    outdict['missing'] = false
					    
			    		}

			    		if (SNPinfo['deletion'].indexOf(skey) != -1){

			    		    outdict['deleted'] = true
					    
			    		}else{
					    
			    		    outdict['deleted'] = false
					    
			    		}

			    		outdict['snp'] = snpdata
			    		output.push(outdict)
				    }
				    outdict = {}
			    	    //console.log(tmpobj[key][skey])
			    	    //console.log("test")
				    //				})

				})


			    }else{
			    	
			    	outdict['uni'] = obz['unires']
			    	outdict['pdb'] = obz['pdbres']
		    		if (obz['uniaaname'] != undefined)
		    			outdict['resn'] = one2three[obz['uniaaname']]||"X"
						outdict['counter'] = obz['counter']

				outdict['respdb'] = obz['pdbaaname']
				
			    	if (SNPinfo['missing'].indexOf(skey) != -1){

			    	    outdict['missing'] = true
				    
			    	}else{

			    	    outdict['missing'] = false
				    
			    	}

			    	if (SNPinfo['deletion'].indexOf(skey) != -1){

			    	    outdict['deleted'] = true

			    	}else{

			    	    outdict['deleted'] = false
			    	}

				output.push(outdict)	
				outdict = {}
			    }			    
			}
		    })

		}


		cb(output)
		//FOR rs in allrs, if pdbres is a key in tmpobj
		    
		})
	    
	    //cb(SNPinfo);
	});

	lineReader.on('error', function() {
	    console.log('Could process the PDB..');
	    
	});
	
    // }else{

    // 	// basic mapping - mistakes are here.


    // 	//read SNP file, extract locations!
    // 	var pdbname = pdb_name.slice(0,4)
    // 	var chain = pdb_name.slice(4,5)
    // 	var SNPpath = "../SNP_DATA_PIPELINE/FinalResults/SNP/"+pdbname+".txt";
    // 	var lineReader = readline.createInterface({
    // 	    terminal: false,
    // 	    input: fs.createReadStream(SNPpath)
    // 	});
    // 	//var SNPinfo = new Object();
    // 	var uniID = [];
    // 	var geneID = [];
    // 	var aa1 = [];
    // 	var aa2 = [];
    // 	var unirsid = [];
    // 	var rsID = [];
    // 	var pdbname = [];
    // 	var pdbchain = [];
    // 	var locations = [];
    // 	var species = [];
    // 	var wholeUni = [];
    // 	var snpdata = [];
    // 	var unirange;
	
    // 	lineReader.on('line', function(line) { 
    // 	    var parts = line.split("\t");
	    
    // 	    unirange = [parseInt(parts[8]),parseInt(parts[9])]
	    
    // 	    if (parts[5] == chain){
    // 		snpdata.push(parts.join("_"));
    // 		wholeUni.push(parts[2])
    // 		uniID.push(parts[0]);
    // 		geneID.push(parts[1]);
    // 		aa1.push(parts[2].substring(2,5));
    // 		aa2.push(parts[2].substring(parts[2].length-3,parts[2].length));
    // 		unirsid.push(parts[2].match(/\d+/)[0]);
    // 		rsID.push(parts[3]);
    // 		pdbname.push(parts[4]);
    // 		pdbchain.push(parts[5]);
    // 		locations.push(parts[6]);
    // 		species.push(parts[7].split("_")[0]+"_"+parts[7].split("_")[1]);
    // 	    }	
    // 	});
    // 	lineReader.on('close', function() {
    // 	    SNPinfo.unirange = unirange
    // 	    SNPinfo.uniprot = uniID;
    // 	    SNPinfo.snpdata = snpdata;
    // 	    SNPinfo.geneID = geneID;
    // 	    SNPinfo.from = aa1;
    // 	    SNPinfo.to = aa2;
    // 	    SNPinfo.uniloc = unirsid;
    // 	    //	SNPinfo.uniloc_unique_sorted = unirsid.sort().filter( uniq );
    // 	    SNPinfo.rs = rsID;
    // 	    SNPinfo.pdbname = pdbname;
    // 	    SNPinfo.pdbchain = pdbchain;
    // 	    SNPinfo.unedited_locations = locations;
    // 	    SNPinfo.species = species;
    // 	    cb(SNPinfo);
    // 	});
    // 	lineReader.on('error', function() {
    // 	    console.log('Could process the PDB..');
	    
    // 	});
	
    // }
}

function uniq(value, index, self) { 
    return self.indexOf(value) === index;
}


function get_surf(path,cb){
    
    var bsjson = ""
    let lineReader = readline.createInterface({
	input: fs.createReadStream(bresipath).pipe(zlib.createGunzip())
    });


    var bsjson = ""
    lineReader.on('line', (line) => {
	bsjson += line
	//console.log(line)
    });

    lineReader.on('close', (line) => {
	bsjson = JSON.parse(bsjson);

	// console.log(bsjson)
	var cool_json = {}
	var surfre = [];
	
	bsjson.forEach(function(y){

	    surfre.push(y['resi']);
	    //cool_json[y['resi']] = [y['bs_id'],y['ltype']];
	    
	    
	});


	cb(surfre)
	
    });

}
