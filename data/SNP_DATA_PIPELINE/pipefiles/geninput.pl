#!/usr/bin/perl

use strict;
use warnings;

my $dir = '../ProcessedDataTables';
my $dir2 = '../SIFTXML/DUMP';


opendir(DIR, $dir) or die $!;
opendir(DIR2, $dir2) or die $!;

my @files = ();
my @files2 = ();
while (my $file = readdir(DIR)) {  
     
	next if ($file =~ m/^\./);

	push @files,$file;
}

while (my $file2 = readdir(DIR2)) {  
     
	next if ($file2 =~ m/^\./);

	push @files2,$file2;

}

foreach my $fel (@files){
    foreach my $rel (@files2){
	print "node map_xmlpdb.js "."../ProcessedDataTables/".$fel." "." ../SIFTXML/DUMP/".$rel,"\n";
    }	
}

closedir(DIR);
closedir(DIR2);
exit 0;


