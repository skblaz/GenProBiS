
const rl = require('readline')
const zlib = require('zlib')
const fs = require('fs');

var snphash = {}
var maphash = {}

//console.log("mapping initiated: ", process.argv[2], process.argv[3])

fs.readFile(process.argv[3], 'utf8', function (err,data) {

    if (err) {
	return console.log(err);
    }

    var splits = data.split("\n")


    splits.forEach(function(splitek){

	var splits = splitek.split(" ")

	if (!maphash.hasOwnProperty(splits[0])){

	    maphash[String(splits[0])] = {}	    

	}

	if (!maphash[String(splits[0])].hasOwnProperty(String(splits[1]))){

	    maphash[String(splits[0])][String(splits[1])] = []
	    
	}
	
	maphash[String(splits[0])][String(splits[1])].push({'pdbid' : splits[2],'chain' : splits[3], 'pdbres' : splits[4]})
	
			
    })	

    //console.log(maphash)
    
    var lineReader = require('readline').createInterface({
    	input: require('fs').createReadStream(process.argv[2])
    });

    lineReader.on('line', function (line) {
	
    	var splits = line.split("\t")

    	if (String(splits[1]) in maphash){

    	    try{

    		if(maphash[String(splits[1])].hasOwnProperty(String(splits[2]).match(/\d+/))){

    		    var pdblist = maphash[String(splits[1])][String(splits[2]).match(/\d+/)]
		    
    		    pdblist.forEach(function(tmphash){
			
			var outrow = String(splits[1])+" "+splits[0]+" "+splits[2]+" "+splits[3]+" "+tmphash['pdbid']+" "+tmphash['chain']+" "+tmphash['pdbres']+" "+process.argv[2].split("/")[2]+"\n";			
				    
			var path = "../FinalResults/SNP/"+tmphash['pdbid']+".txt"
			
    			fs.appendFile(path,outrow, function (err) {

    			});
			
    		    })

    		}
		
    	    }catch(err){
		
    	//	console.log("Invalid entry..")
		
    	    }
    	}
	
    });


    lineReader.on('close', function(){

    	//console.log("mapped them all..")
    })

    
});




