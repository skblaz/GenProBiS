## This R algorithm joins sifts and andividual Species SNP data.
## read sifts!
print ("Finding SIFTS table")
setwd("../SIFTS")
print ("Table found!   .......   reading table")
mainSIFTS <- read.table("pdb_chain_uniprot.tsv",skip=1,sep="\t",head=T)
print ("Table successfully read! Moving on..")
setwd("../ProcessedDataTables")
namevector <- c("Gene Name","SP_PRIMARY","AA change","Source_DB","Consequence_type", "Clinical_significance", "Phenotype_disease","Phenotype_disease_source", "Cytogenetic_band", "Chr_coordinates","Ensembl_gene_ID","Ensembl_transcript_ID","Ensembl_translation_ID","Variant_evidence")

## read line by line  ---------------  Approach 1
print ("Starting file processing..")

fileNames <- list.files()

print (fileNames)
## PDB finder function

for (file in fileNames){
    print (paste0("Currently processing file> ", file))
    ## process file with sifts table
    ## save file as necessary!
    Cutoff <- length(readLines(file))
    currentDF<- read.table(file, header=F, nrow=Cutoff-30)
    print (head(currentDF))   
 }

print (counter)

    ## basic algorithm
    ## for each species do:
    ##     extract SNP and uniprot location data
    ## for each SNP search through sifts if match, reduce from PDB
    ## write to ..
