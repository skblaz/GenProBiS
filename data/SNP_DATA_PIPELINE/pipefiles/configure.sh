

## This version maps ALL polymorphisms found in the UniProt database..
############################################################################################

if [ "$1" == "--allP" ];
   
then
    cd ..

    mkdir PreviousInputFiles

    ## if not exists it will skip it, this is for backup
    mkdir InputFilesVariation

    ## backup
    rm -rvf InputFilesVariation && mkdir InputFilesVariation

    cd InputFilesVariation


    ## get SNP datafiles from uniprot
    
    echo "Obtaining files.."

    if [ "$2" == "--UseBackup" ];
       
    then
	
	echo "Using backup files.."
	
    else
	
	wget -m -nH --cut-dirs=6  ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/
	
    fi
    
    echo "Finding files.."

    cd ..

    rm -rvf ExtractedFiles

    mkdir ExtractedFiles

    ##extract files according to user input
    
    if [ "$2" == "--UseBackup"  ];
       
    then

	echo "Using backup files.."
	cd PreviousInputFiles/InputFilesVariation
	for f in *.gz; do
	    STEM=$(basename "${f}" .gz)
	    gunzip -cv "${f}" > ../../ExtractedFiles/"${STEM}"
	done
	cd ../..
    else

	echo "Using downloaded files.."
	cp -a InputFilesVariation PreviousInputFiles  #for backup
	cd InputFilesVariation
	for f in *.gz; do
	    STEM=$(basename "${f}" .gz)
	    gunzip -cv "${f}" > ../ExtractedFiles/"${STEM}"
	done
	cd ..
	
    fi
    
    
    #parallel --eta 'gunzip {} > ExtractedFiles/{.}' ::: InputFilesVariation/*.gz
    echo "File extraction completed!"
    #Obtain sift table
    ## sifts download and extract script for SIFT dataset
    echo "SIFT table downloaded"
    mkdir SIFT
    cd SIFT
    wget ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_uniprot.tsv.gz
    gunzip -cv pdb_chain_uniprot.tsv.gz >"sifts.tsv" 

    echo "Cleaning files.."
    ## for each extracted entry skip first 144 lines! - this is a description of a file.


    cd ..
    rm -rvf ProcessedDataTables
    mkdir ProcessedDataTables
    rm -rvf HUMSAVAR
    mkdir HUMSAVAR
    cd HUMSAVAR
    wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/humsavar.txt

    ## some dummy values
    
    cat humsavar.txt | tail -n +31 | mawk '{print $1"\t"$2"\t"$4"\t"$6"\t"$5}' > ../ProcessedDataTables/homo_sapiens_variationdisease

    ## tukej more v pravi formi dat to v extracted
    cd ..
    cd ExtractedFiles

    #Process each extracted file accordingly
    
    for fh in *.txt; do
	STEM=$(basename "${fh}" .txt)
	echo "Processing dataset... $STEM"
	cat "${fh}" | tail -n +144 > ../ProcessedDataTables/"$STEM"
    done
    
    #remove redundant files..
    rm -rvf ../ExtractedFiles
    rm -rvf ../InputFilesVariation

    ## call to R script for file joining!
    cd ../ProcessedDataTables


    ## split for faster processing
    echo "Splitting files"	
    find . -type f -size +150M -exec split -b 100M -d {} {} \;
    find . -type f -size +150M -exec rm -rvf {} \;


    cd ../pipefiles

    # Clean any previous versions!

    mkdir ../FinalResults

    # make fresh results each run, delete only generated results, leave PDB database for further validation!
    
    rm -rvf ../FinalResults/SNP
    rm -rvf ../FinalResults/PDB_SNP_VALIDATED
    mkdir ../FinalResults/SNP

    cd ../..   

    ## update the PDB mirror
    echo "Updating the PDB mirror"
    rsync -a --port=33444 ftp.wwpdb.org::ftp_data/structures/divided/pdb/ ./pdb
    
    cd SNP_DATA_PIPELINE/pipefiles
    
    #mkdir ../FinalResults/images   -- might be used in future relases
    #mkdir ../FinalResults/PDB_SNP_VALIDATED

    # keep converted and download PDB files
    #mkdir ../FinalResults/PDB
    #mkdir ../FinalResults/PDB_SEQ

    # #Run main algorithm which fills the SNP folder!

    ## Try to run in parallel... R splits files so they dont intersect
    ## maybe configure for different $3 inputs - parallel version

    if [ "$3" == "--4" ];
       
    then
	echo "Using 4 cores for computation"

	Rscript SNPPARALLEL.R 4 0 &
	Rscript SNPPARALLEL.R 4 1 &
	Rscript SNPPARALLEL.R 4 2 &
	Rscript SNPPARALLEL.R 4 3 &
	wait
	
    elif [ "$3" == "--6" ];

    then
	echo "using 6 cores for computation"
	
	Rscript SNPPARALLEL.R 6 0 &
	Rscript SNPPARALLEL.R 6 1 &
	Rscript SNPPARALLEL.R 6 2 &
	Rscript SNPPARALLEL.R 6 3 &
	Rscript SNPPARALLEL.R 6 4 &
	Rscript SNPPARALLEL.R 6 5 &
	wait

    elif [ "$3" == "--8" ]
    then
	
	echo "using 6 cores for computation"
	
	Rscript SNPPARALLEL.R 8 0 &
	Rscript SNPPARALLEL.R 8 1 &
	Rscript SNPPARALLEL.R 8 2 &
	Rscript SNPPARALLEL.R 8 3 &
	Rscript SNPPARALLEL.R 8 4 &
	Rscript SNPPARALLEL.R 8 5 &
	Rscript SNPPARALLEL.R 8 6 &
	Rscript SNPPARALLEL.R 8 7 &
	wait

    elif [ "$3" == "--16" ]
    then
	
	Rscript SNPPARALLEL.R 16 0 &
	Rscript SNPPARALLEL.R 16 1 &
	Rscript SNPPARALLEL.R 16 2 &
	Rscript SNPPARALLEL.R 16 3 &
	Rscript SNPPARALLEL.R 16 4 &
	Rscript SNPPARALLEL.R 16 5 &
	Rscript SNPPARALLEL.R 16 6 &
	Rscript SNPPARALLEL.R 16 7 &
	Rscript SNPPARALLEL.R 16 8 &
	Rscript SNPPARALLEL.R 16 9 &
	Rscript SNPPARALLEL.R 16 10 &
	Rscript SNPPARALLEL.R 16 11 &
	Rscript SNPPARALLEL.R 16 12 &
	Rscript SNPPARALLEL.R 16 13 &
	Rscript SNPPARALLEL.R 16 14 &
	Rscript SNPPARALLEL.R 16 15 &

    elif [ "$3" == "--xmlmap" ]
    then

	## restructure the folders for the use of xml schema
	
	cd ..
	
	mkdir SIFTXML
	cd SIFTXML
	
	rm -rvf DUMP
	rm -rvf PDB_UNI
	mkdir PDB_UNI
	mkdir DUMP
	
	cd DUMP
	touch wholebase.txt

	cd ..
	
	## sync the files from the SIFTS

	echo "Syncing the xml mapping files.."
	mkdir xml
	cd xml
	
	ls ../../../pdb | perl -ne 'print "ls ../../../pdb/".$_' | sh | perl -ne 'print "wget -nc ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/xml/".(substr $_, 3,4).".xml.gz"; print "\n"' | parallel -j8

	cd ../../pipefiles

	## from xml to json files (for final JSON later on) + wholebase for further calculation
	
	ls ../SIFTXML/xml/ |  mawk '{print "node pdb_uni.js ../SIFTXML/xml/"$1}' | parallel


	## compress the obtained files for further usage.
	
	cd ../SIFTXML/PDB_UNI

	pigz *

	cd ../DUMP	

	## split the files into chunks, that fit into ram (for parallel usage)
	
	find . -type f -size +100M -exec split -b 80M -d {} split \;
	
	find . -type f -size +100M -exec rm -rvf {} \;
	
	## generate input and compute the mappings. 
	
	cd ../../pipefiles
	
	## this generates the input for the mapping algorithm, executed in parallel. To use a single core, use the | sh instead of | parallel
	
	perl geninput.pl | wc -l | mawk '{print "Calculations:",$1}'
	perl geninput.pl | parallel

    else
	
	echo "No tool selected for the mapping process.."
	
    fi

    cd ../FinalResults/SNP
    #gzip *
    echo "Writing files to wholebase for index construction"
    cat * > ../WholeBase.txt

    echo "Writing index.."
    cd ..
    
    # index generation
    
    bash getindex.sh

    # statistics
    
    bash summary.sh
    
    #rsync -avr blazs@probis.cmm.ki.si:/home/probis/insilab.org/files/GenProBiS/probisdb/bresi/ ./BS_DATA/bresi

    
    echo "Updating the PDB mirror"

    if [ "$4" == "--dbgen" ]
       
    then
	echo "Initiating mapping and validation process for the whole database"

	cd ../../

	#rsync -a --port=33444 ftp.wwpdb.org::ftp_data/structures/divided/pdb/ ./pdb

	rm -rvf PDB_SNP_MAPPING_ALL
	mkdir PDB_SNP_MAPPING_ALL
	cd PDB_SNP_MAPPING_ALL
	
	mkdir JSON
	mkdir READABLE
	mkdir WHOLE_BASE	
	
	cd ..
	cd algorithms

	# This script generates whole, sequence level validated database.. Add hostfile for cluster usage.
	
	bash generateDBI.sh --generateSNPdb
	
    fi

    echo "Finished.."

    #this is used to actually construct the GenProBiS webserver data in json formats.
    if [ "$5" == "--singlenode" ]

    then
	
	cd ../../algorithms
	bash generateDBI.sh --GNF ## to generate files needed for computation
	bash generateDBI.sh --identify ## Computation, mapping
	bash generateDBI.sh --R ## Computation, 2nd cycle, this reduces the number of false positive results..
	
    elif [ "$5" == "--multinode" ]

    then
	bash generateDBI.sh --map ## map the jobs
	bash generateDBI.sh --WP ## Do this using the copied folder structure and slaves
	bash generateDBI.sh --reduce ## reduce the jobs
    else
	echo "Nothing selected.."
    fi
    cd ../SNP_DATA_PIPELINE/pipefiles
    echo "Done with SNP location detection.."    

    #Download all sequences for validation, check if PDBs were already downloaded..

    if [ "$6" == "--validate" ];

    then
	echo 'Beginning validation on sequence level..'

	cd ../FinalResults/PDB
	
	echo "Downloading PDB files"

	# only add PDB if not exists already, minimal request overhead is redundant in this case

	for en in ../SNP/*.txt; do

	    STEP=$(basename "${en}" .txt)

	    if [ -f "$STEP".pdb ];
	       
	    then
		
		echo ".."

	    else

		STEP=$(basename "${en}" .txt)
		wget http://www.rcsb.org/pdb/files/"$STEP".pdb

	    fi
	    
	done

	echo 'Finished downloading PDB files..'

	#Convert all PDBs to sequences..
	echo "Converting the PDB to sequences for validation"

	cd ../


	echo "Fasta conversion in process.."

	# Only convert the nonexisting ones..
	for et in PDB/*.pdb; do

	    STEG=$(basename "${et}" .pdb)

	    
	    if [ -f PDB_SEQ/"$STEG".fasta ]
	       
	    then
		
		echo "Passing on.. File exists!"

	    else
		
		../pipefiles/./pdb2fasta.sh "$et" > PDB_SEQ/"$STEG".fasta
		#echo "converting"
		
	    fi
	    
	done

	cd ../pipefiles

	##Validation algorithm in R
	#Rscript SNPvalidator.R


	if [ "$3" == "--4" ];
	   
	then
	    echo "Using 4 cores for computation"

	    Rscript SNPPARALLELVALIDATION.R 4 0 &
	    Rscript SNPPARALLELVALIDATION.R 4 1 &
	    Rscript SNPPARALLELVALIDATION.R 4 2 &
	    Rscript SNPPARALLELVALIDATION.R 4 3 &
	    wait
	    
	elif [ "$3" == "--6" ];

	then
	    echo "using 6 cores for computation"
	    
	    Rscript SNPPARALLELVALIDATION.R 6 0 &
	    Rscript SNPPARALLELVALIDATION.R 6 1 &
	    Rscript SNPPARALLELVALIDATION.R 6 2 &
	    Rscript SNPPARALLELVALIDATION.R 6 3 &
	    Rscript SNPPARALLELVALIDATION.R 6 4 &
	    Rscript SNPPARALLELVALIDATION.R 6 5 &
	    wait

	elif ["$3" == "--8"]
	then
	    
	    echo "using 6 cores for computation"
	    
	    Rscript SNPPARALLELVALIDATION.R 8 0 &
	    Rscript SNPPARALLELVALIDATION.R 8 1 &
	    Rscript SNPPARALLELVALIDATION.R 8 2 &
	    Rscript SNPPARALLELVALIDATION.R 8 3 &
	    Rscript SNPPARALLELVALIDATION.R 8 4 &
	    Rscript SNPPARALLELVALIDATION.R 8 5 &
	    Rscript SNPPARALLELVALIDATION.R 8 6 &
	    Rscript SNPPARALLELVALIDATION.R 8 7 &
	    wait

	elif ["$3" == "--16"]
	then
	    
	    Rscript SNPPARALLELVALIDATION.R 16 0 &
	    Rscript SNPPARALLELVALIDATION.R 16 1 &
	    Rscript SNPPARALLELVALIDATION.R 16 2 &
	    Rscript SNPPARALLELVALIDATION.R 16 3 &
	    Rscript SNPPARALLELVALIDATION.R 16 4 &
	    Rscript SNPPARALLELVALIDATION.R 16 5 &
	    Rscript SNPPARALLELVALIDATION.R 16 6 &
	    Rscript SNPPARALLELVALIDATION.R 16 7 &
	    Rscript SNPPARALLELVALIDATION.R 16 8 &
	    Rscript SNPPARALLELVALIDATION.R 16 9 &
	    Rscript SNPPARALLELVALIDATION.R 16 10 &
	    Rscript SNPPARALLELVALIDATION.R 16 11 &
	    Rscript SNPPARALLELVALIDATION.R 16 12 &
	    Rscript SNPPARALLELVALIDATION.R 16 13 &
	    Rscript SNPPARALLELVALIDATION.R 16 14 &
	    Rscript SNPPARALLELVALIDATION.R 16 15 &
	    
	else

	    echo "Doing the default computation"

	    Rscript SNPPARALLELVALIDATION.R 4 0 &
	    Rscript SNPPARALLELVALIDATION.R 4 1 &
	    Rscript SNPPARALLELVALIDATION.R 4 2 &
	    Rscript SNPPARALLELVALIDATION.R 4 3 &

	    wait
	    
	fi


	#ls ../FinalResults/SNP | xargs -n1 -P 3
	#Rscript SNPvalidatorP.R

	echo "Validation process finished.."
	
	rm -rvf ../SNPDATA
	mkdir ../SNPDATA
	
	cd ../FinalResults
	cp -rf PDB_SNP_VALIDATED ../SNPDATA

	## SNPDATA includes data to be served to the REST service
    else
	echo "Process finished."
    fi
    
fi


## This version maps ALL RS polymorphisms found in the Uniprot database..

if [ "$1" == "--allRS"  ];
then
    cd ..

    mkdir PreviousInputFiles

    ## if not exists it will skip it, this is for backup
    mkdir InputFilesVariation

    cp -a InputFilesVariation PreviousInputFiles
    rm -rvf InputFilesVariation && mkdir InputFilesVariation

    cd InputFilesVariation
    echo "Obtaining files.."
    
    if [ "$2" == "--UseBackup" ];
    then
	echo "Using backup files.."
    else
	wget -m -nH --cut-dirs=6  ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/    
    fi
    
    echo "Finding files.."

    cd ..

    rm -rvf ExtractedFiles

    mkdir ExtractedFiles
    if [ "$2" == "--UseBackup"  ];
    then
	echo "Using backup files.."
	cd PreviousInputFiles/InputFilesVariation
	for f in *.gz; do
	    STEM=$(basename "${f}" .gz)
	    gunzip -cv "${f}" > ../../ExtractedFiles/"${STEM}"
	done
	cd ../..
    else

	echo "Using downloaded files.."
	cd InputFilesVariation
	for f in *.gz; do
	    STEM=$(basename "${f}" .gz)
	    gunzip -cv "${f}" > ../ExtractedFiles/"${STEM}"
	done
	cd ..
	
    fi

    #parallel --eta 'gunzip {} > ExtractedFiles/{.}' ::: InputFilesVariation/*.gz
    echo "File extraction completed!"

    ## sifts download and extract script for SIFT dataset
    echo "SIFT table downloaded"
    
    mkdir SIFT
    cd SIFT
    wget ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_uniprot.tsv.gz
    gunzip -cv pdb_chain_uniprot.tsv.gz >"sifts.tsv" 

    echo "Cleaning files.."
    ## for each extracted entry skip first 144 lines! - this is a description of a file.


    cd ..
    rm -rvf ProcessedDataTables


    mkdir ProcessedDataTables

    cd ExtractedFiles

    for fh in *.txt; do
	STEM=$(basename "${fh}" .txt)
	echo "Processing dataset... $STEM"
	cat "${fh}" | tail -n +144 > ../ProcessedDataTables/"$STEM"
    done

    rm -rvf ../ExtractedFiles
    rm -rvf ../InputFilesVariation

    ## call to R script for file joining!
    cd ../ProcessedDataTables


    ## split for faster processing

    find . -type f -size +200M -exec split -b 100M -d {} {} \;
    find . -type f -size +200M -exec rm -rvf {} \;


    cd ../pipefiles

    # Clean any previous versions!

    mkdir ../FinalResults

    # make fresh results each run
    
    rm -rvf ../FinalResults/SNP2
    rm -rvf ../FinalResults/PDB_SNP_VALIDATED2
    mkdir ../FinalResults/SNP2
    #mkdir ../FinalResults/images
    mkdir ../FinalResults/PDB_SNP_VALIDATED2

    # keep converted and download PDB files
    mkdir ../FinalResults/PDB
    mkdir ../FinalResults/PDB_SEQ

    # #Run main algorithm which fills the SNP2 folder!

    Rscript SNPfinderAll2.R 

    wait

    echo "Done with SNP location detection.."

    #Download all sequences for validation, check if PDBs were already downloaded..

    echo 'Beginning validation..'

    cd ../FinalResults/PDB
    
    echo "Downloading PDB files"

    # only add PDB if not exists already

    for en in ../SNP2/*.txt; do

	STEP=$(basename "${en}" .txt)

	if [ -f "$STEP".pdb ];
	   
	then
	    
	    echo "File $en already exists, moving on.."

	else	  

	    STEP=$(basename "${en}" .txt)
	    wget http://www.rcsb.org/pdb/files/"$STEP".pdb

	fi
	
    done

    echo 'Finished downloading PDB files..'

    #Convert all PDBs to sequences..
    echo "Converting the PDB to sequences for validation"

    cd ../


    echo "Fasta conversion in process.."

    #Only convert the nonexisting ones..
    #Additional PDBs will be sufficiently downloaded..
    for et in PDB/*.pdb; do

	STEG=$(basename "${et}" .pdb)

	
	if [ -f PDB_SEQ/"$STEG".fasta ];
	   
	then
	    
	    echo "Passing on.. File exists!"

	else
	    
	    ../pipefiles/./pdb2fasta.sh "$et" > PDB_SEQ/"$STEG".fasta
	    
	    #echo "converting"
	    
	fi
	
    done
    rm -rvf ../SNPDATA
    mkdir ../SNPDATA
    cd ../pipefiles


    # #Validation algorithm in R
    Rscript SNPvalidator2.R

    echo "Validation process finished.."

    cd ../FinalResults
    cp -rf PDB_SNP_VALIDATED2 ../SNPDATA
    
fi

# Validate results, one must have obtained SNP PDB PDB_SEQ folders for this
if [ "$1" == "--validate" ];
then

    Rscript SNPvalidator.R

    echo "Validation process finished.."
    
fi

#find using ram - BEWARE! If files are not splitted previously, this might cause breakdowns!

if [ "$1" == "--find" ];
then

    Rscript SNPfinder2.R

    echo "Found mappings are in the SNP folder in results section.."
fi

# this finds all polymorphisms, not just RS type.
if [ "$1" == "--findAll" ];
then

    Rscript SNPfinderStrict.R

    echo "Found mappings are in the SNP folder in results section.."
fi

#run parallel version
if [ "$1" == "--findP" ];
then
    
    Rscript SNPfinderP1.R &
    Rscript SNPfinderP2.R &
    Rscript SNPfinderP3.R &
    Rscript SNPfinderP4.R &
    
    wait

    echo "Parallel mapping complete.."  
fi


#clean the files for fresh start
if [ "$1" == "--clean" ];
then

    echo "Are you sure you want to proceed? (y/n)"

    read uinput

    if [ "$uinput" == "y" ];then
	
	cd ..
	rm -rvf SIFT
	## Precious files!
	#rm -rvf PreviousinputFiles
	rm -rvf ProcessedDataTables
	rm -rvf FinalResults

    else

	echo "Exiting .."
    fi

fi

#Convert from PDB to sequences, needs folders PDB and PDB_SEQ

if [ "$1" == "--pdb2seq" ];
   
then

    cd ../FinalResults


    for et in PDB/*.pdb; do

	STEG=$(basename "${et}" .pdb)

	
	if [ -f PDB_SEQ/"$STEG".fasta ]
	   
	then
	    
	    echo "Passing on.. File exists!"

	else
	    
	    ../pipefiles/./pdb2fasta.sh "$et" > PDB_SEQ/"$STEG".fasta
	    #echo "converting"
	    
	fi
	
    done
    
fi

# this needs results in FinalResults/SNP folder

if [ "$1" == "--downloadPDB" ];
   
then
    cd ../FinalResults
    mkdir PDB
    cd PDB
    
    for en in ../SNP/*.txt; do

	STEP=$(basename "${en}" .txt)

	if [ -f "$STEP".pdb ]
	   
	then
	    
	    echo "File $en already exists, moving on.."

	else

	    STEP=$(basename "${en}" .txt)
	    wget http://www.rcsb.org/pdb/files/"$STEP".pdb

	fi
	
    done
    

fi

if [ "$1" == "--validateP" ];
   
then

    Rscript SNPvP1.R &
    Rscript SNPvP2.R &
    Rscript SNPvP3.R &
    Rscript SNPvP4.R &
    Rscript SNPvP5.R &
    Rscript SNPvP6.R &

    wait

    echo "validation complete!"
fi
## file splitting process, needs processed tables present

if [ "$1" == "--splitfiles"  ];

then

    mkdir ../ProcessedDataTables
    cd ../ExtractedFiles

    for fh in *.txt; do
	STEM=$(basename "${fh}" .txt)
	echo "Processing dataset... $STEM"
	cat "${fh}" | tail -n +144 > ../ProcessedDataTables/"$STEM"
    done


    ## call to R script for file joining!
    cd ../ProcessedDataTables

    ## split for faster processing

    find . -type f -size +200M -exec split -b 100M -d {} {} \;
    find . -type f -size +200M -exec rm -rvf {} \;

fi


