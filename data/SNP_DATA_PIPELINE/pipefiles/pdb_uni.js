
// this is the algorithm for conversion of gzipped xml files to json uni maps

const fs       = require('fs');
const zlib     = require('zlib');
const readline = require('readline');
var parseString = require('xml2js').parseString;

let lineReader = readline.createInterface({
    input: fs.createReadStream(process.argv[2]).pipe(zlib.createGunzip())
});

var xml = "";
lineReader.on('line', (line) => {
    xml += line;
    //console.log(xml)
});

lineReader.on('close',() =>{
    parseString(xml, function (err, result) {    

	var finalhash = []
	var counter = 0;
	    var readableformat = ""
	result['entry']['entity'].forEach(function(x){

	    //tatretja je za residue

	    //shrani tukej
	    
//	    var finalhash = []
	  //  var readableformat = ""
//	    x['segment']

	    x['segment'].forEach(function(seg){

	    seg['listResidue'][0]['residue'].forEach(function(xe){

		var PDBid;
		var PDBchain;
		var PDBresidue;
		var UNIid;
		var UNIresidue;
		var PDBaminoname;
		var UNIaminoname;
		
//console.log(xe['crossRefDb'])
		xe['crossRefDb'].forEach(function(xs){
		    
		    if (xs['$']['dbSource'] == 'PDB' ){

			PDBid = xs['$']['dbAccessionId']
			PDBchain = xs['$']['dbChainId']
			PDBresidue = xs['$']['dbResNum']
			PDBaminoname = xs['$']['dbResName']
			
		    }else if (xs['$']['dbSource'] == 'UniProt'){
			
			UNIid = xs['$']['dbAccessionId']
			UNIresidue = xs['$']['dbResNum']
			UNIaminoname = xs['$']['dbResName']
			
		    }else {}
		    
		    //console.log(xs['$']) 
		    
		})
		if (UNIresidue != undefined || PDBresidue != undefined){

		//console.log(PDBid, PDBchain,PDBresidue, UNIresidue)
		if (parseInt(UNIresidue) >= parseInt(PDBresidue) || (UNIresidue != undefined || PDBresidue == undefined) || UNIresidue == undefined || PDBresidue != undefined){

		    if (finalhash.indexOf({'counter': counter-1, 'uniid' : UNIid, 'unires' : UNIresidue, 'pdbid' : PDBid, 'chain' : PDBchain, 'pdbres' : PDBresidue, 'pdbaaname' : PDBaminoname, 'uniaaname': UNIaminoname}) == -1){

			finalhash.push({'counter': counter, 'uniid' : UNIid, 'unires' : UNIresidue, 'pdbid' : PDBid, 'chain' : PDBchain, 'pdbres' : PDBresidue, 'pdbaaname' : PDBaminoname, 'uniaaname': UNIaminoname})
			counter++;

			
			 readableformat += UNIid+" "+UNIresidue+" "+PDBid+" "+PDBchain+" "+PDBresidue+"\n"
		    }
		    		    
		   // var tmpreadableformat = UNIid+" "+UNIresidue+" "+PDBid+" "+PDBchain+" "+PDBresidue+"\n"
		
		
		//if (tmpreadableformat.split(" ") != ''){
		    //		    console.log(readableformat)
		//    readableformat += tmpreadableformat

		//}
		}
		}
	    });
	    });	    			 
	    
	})


	require('fs').appendFile('../SIFTXML/DUMP/wholebase.txt', readableformat, function (err) {			
	    //console.log(readableformat)
	});

	fs.writeFile("../SIFTXML/PDB_UNI/"+process.argv[2].split("/")[3].replace(".xml.gz",".json"), JSON.stringify(finalhash), function (err) {
	    if (err) return console.log(err);
	    
	});
    });    
})




