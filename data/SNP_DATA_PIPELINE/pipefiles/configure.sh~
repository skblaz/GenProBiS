# #!/bin/bash
# # #This .sh file enables download and extraction of main mutation files
# # ## to run it needs R-base installed, it can be installed using sudo apt-get install r-base

# #sudo apt-get update

# # obtain the data

# starts from pipefiles  folder, one additional subfolder is needed!

## this option is the default one - it maps all rs mutations

if [ "$1" == "--all" ];
   
then
    cd ..

    mkdir PreviousInputFiles

    ## if not exists it will skip it, this is for backup
    mkdir InputFilesVariation

    ## backup
    rm -rvf InputFilesVariation && mkdir InputFilesVariation

    cd InputFilesVariation


    ## get SNP datafiles from uniprot
    
    echo "Obtaining files.."

    if [ "$2" == "--UseBackup" ];
       
    then
	echo "Using backup files.."
    else
	wget -m -nH --cut-dirs=6  ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/    
    fi
    
    echo "Finding files.."

    cd ..

    rm -rvf ExtractedFiles

    mkdir ExtractedFiles

    ##extract files according to user input
    
    if [ "$2" == "--UseBackup"  ];
       
    then

	echo "Using backup files.."
	cd PreviousInputFiles/InputFilesVariation
	for f in *.gz; do
	    STEM=$(basename "${f}" .gz)
	    gunzip -cv "${f}" > ../../ExtractedFiles/"${STEM}"
	done
	cd ../..
    else

	echo "Using downloaded files.."
	cp -a InputFilesVariation PreviousInputFiles  #for backup
	cd InputFilesVariation
	for f in *.gz; do
	    STEM=$(basename "${f}" .gz)
	    gunzip -cv "${f}" > ../ExtractedFiles/"${STEM}"
	done
	cd ..
	
    fi
    
    
    #parallel --eta 'gunzip {} > ExtractedFiles/{.}' ::: InputFilesVariation/*.gz
    echo "File extraction completed!"
    #Obtain sift table
    ## sifts download and extract script for SIFT dataset
    echo "SIFT table downloaded"
    mkdir SIFT
    cd SIFT
    wget ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_uniprot.tsv.gz
    gunzip -cv pdb_chain_uniprot.tsv.gz >"sifts.tsv" 

    echo "Cleaning files.."
    ## for each extracted entry skip first 144 lines! - this is a description of a file.


    cd ..
    rm -rvf ProcessedDataTables
    mkdir ProcessedDataTables
    cd ExtractedFiles

    #Process each extracted file accordingly
    
    for fh in *.txt; do
	STEM=$(basename "${fh}" .txt)
	echo "Processing dataset... $STEM"
	cat "${fh}" | tail -n +144 > ../ProcessedDataTables/"$STEM"
    done
    
    #remove redundant files..
    rm -rvf ../ExtractedFiles
    rm -rvf ../InputFilesVariation

    ## call to R script for file joining!
    cd ../ProcessedDataTables


    ## split for faster processing
    echo "Splitting files"	
    find . -type f -size +50M -exec split -b 10M -d {} {} \;
    find . -type f -size +50M -exec rm -rvf {} \;


    cd ../pipefiles

    # Clean any previous versions!

    mkdir ../FinalResults

    # make fresh results each run, delete only generated results, leave PDB database for further validation!
    
    rm -rvf ../FinalResults/SNP
    rm -rvf ../FinalResults/PDB_SNP_VALIDATED
    mkdir ../FinalResults/SNP
    #mkdir ../FinalResults/images   -- might be used in future relases
    mkdir ../FinalResults/PDB_SNP_VALIDATED

    # keep converted and download PDB files
    mkdir ../FinalResults/PDB
    mkdir ../FinalResults/PDB_SEQ

    # #Run main algorithm which fills the SNP folder!

    ## Try to run in parallel... R splits files so they dont intersect
    ## maybe configure for different $3 inputs - parallel version
    Rscript SNPfinderP1.R &
    Rscript SNPfinderP2.R &
    Rscript SNPfinderP3.R &
    Rscript SNPfinderP4.R &
    Rscript SNPfinderP5.R &
    Rscript SNPfinderP6.R &

    wait

    echo "Done with SNP location detection.."

    #Download all sequences for validation, check if PDBs were already downloaded..

    echo 'Beginning validation..'

    cd ../FinalResults/PDB
    
    echo "Downloading PDB files"

    # only add PDB if not exists already, minimal request overhead is redundant in this case

    for en in ../SNP/*.txt; do

	STEP=$(basename "${en}" .txt)

	if [ -f "$STEP".pdb ];
	   
	then
	    
	    echo ".."

	else

	    STEP=$(basename "${en}" .txt)
	    wget http://www.rcsb.org/pdb/files/"$STEP".pdb

	fi
	
    done

    echo 'Finished downloading PDB files..'

    #Convert all PDBs to sequences..
    echo "Converting the PDB to sequences for validation"

    cd ../


    echo "Fasta conversion in process.."

    # Only convert the nonexisting ones..
    for et in PDB/*.pdb; do

	STEG=$(basename "${et}" .pdb)

	
	if [ -f PDB_SEQ/"$STEG".fasta ]
	   
	then
	    
	    echo "Passing on.. File exists!"

	else
	    
	    ../pipefiles/./pdb2fasta.sh "$et" > PDB_SEQ/"$STEG".fasta
	    #echo "converting"
	    
	fi
	
    done

    cd ../pipefiles

    ##Validation algorithm in R
    #Rscript SNPvalidator.R
    
    Rscript SNPvP1.R &
    Rscript SNPvP2.R &
    Rscript SNPvP3.R &
    Rscript SNPvP4.R &
    Rscript SNPvP5.R &
    Rscript SNPvP6.R &

    wait
    

    #ls ../FinalResults/SNP | xargs -n1 -P 3 Rscript SNPvalidatorP.R

    echo "Validation process finished.."
    
    rm -rvf ../SNPDATA
    mkdir ../SNPDATA
    
    cd ../FinalResults
    cp -rf PDB_SNP_VALIDATED ../SNPDATA

    ## SNPDATA includes data to be served to the REST service

fi


## This version maps ALL polymorphisms found in the Uniprot database..
############################################################################################

if [ "$1" == "--allP" ];
   
then
    cd ..

    mkdir PreviousInputFiles

    ## if not exists it will skip it, this is for backup
    mkdir InputFilesVariation

    ## backup
    rm -rvf InputFilesVariation && mkdir InputFilesVariation

    cd InputFilesVariation


    ## get SNP datafiles from uniprot
    
    echo "Obtaining files.."

    if [ "$2" == "--UseBackup" ];
       
    then
	echo "Using backup files.."
    else
	wget -m -nH --cut-dirs=6  ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/    
    fi
    
    echo "Finding files.."

    cd ..

    rm -rvf ExtractedFiles

    mkdir ExtractedFiles

    ##extract files according to user input
    
    if [ "$2" == "--UseBackup"  ];
       
    then

	echo "Using backup files.."
	cd PreviousInputFiles/InputFilesVariation
	for f in *.gz; do
	    STEM=$(basename "${f}" .gz)
	    gunzip -cv "${f}" > ../../ExtractedFiles/"${STEM}"
	done
	cd ../..
    else

	echo "Using downloaded files.."
	cp -a InputFilesVariation PreviousInputFiles  #for backup
	cd InputFilesVariation
	for f in *.gz; do
	    STEM=$(basename "${f}" .gz)
	    gunzip -cv "${f}" > ../ExtractedFiles/"${STEM}"
	done
	cd ..
	
    fi
    
    
    #parallel --eta 'gunzip {} > ExtractedFiles/{.}' ::: InputFilesVariation/*.gz
    echo "File extraction completed!"
    #Obtain sift table
    ## sifts download and extract script for SIFT dataset
    echo "SIFT table downloaded"
    mkdir SIFT
    cd SIFT
    wget ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_uniprot.tsv.gz
    gunzip -cv pdb_chain_uniprot.tsv.gz >"sifts.tsv" 

    echo "Cleaning files.."
    ## for each extracted entry skip first 144 lines! - this is a description of a file.


    cd ..
    rm -rvf ProcessedDataTables
    mkdir ProcessedDataTables
    cd ExtractedFiles

    #Process each extracted file accordingly
    
    for fh in *.txt; do
	STEM=$(basename "${fh}" .txt)
	echo "Processing dataset... $STEM"
	cat "${fh}" | tail -n +144 > ../ProcessedDataTables/"$STEM"
    done
    
    #remove redundant files..
    rm -rvf ../ExtractedFiles
    rm -rvf ../InputFilesVariation

    ## call to R script for file joining!
    cd ../ProcessedDataTables


    ## split for faster processing
    echo "Splitting files"	
    find . -type f -size +50M -exec split -b 10M -d {} {} \;
    find . -type f -size +50M -exec rm -rvf {} \;


    cd ../pipefiles

    # Clean any previous versions!

    mkdir ../FinalResults

    # make fresh results each run, delete only generated results, leave PDB database for further validation!
    
    rm -rvf ../FinalResults/SNP
    rm -rvf ../FinalResults/PDB_SNP_VALIDATED
    mkdir ../FinalResults/SNP
    #mkdir ../FinalResults/images   -- might be used in future relases
    mkdir ../FinalResults/PDB_SNP_VALIDATED

    # keep converted and download PDB files
    mkdir ../FinalResults/PDB
    mkdir ../FinalResults/PDB_SEQ

    # #Run main algorithm which fills the SNP folder!

    ## Try to run in parallel... R splits files so they dont intersect
    ## maybe configure for different $3 inputs - parallel version

    if [ "$3" == "--4" ];
       
    then
	echo "Using 4 cores for computation"

	Rscript SNPPARALLEL.R 4 0 &
	Rscript SNPPARALLEL.R 4 1 &
	Rscript SNPPARALLEL.R 4 2 &
	Rscript SNPPARALLEL.R 4 3 &
	wait
	
    elif [ "$3" == "--6" ];

    then
	echo "using 6 cores for computation"
	
	Rscript SNPPARALLEL.R 6 0 &
	Rscript SNPPARALLEL.R 6 1 &
	Rscript SNPPARALLEL.R 6 2 &
	Rscript SNPPARALLEL.R 6 3 &
	Rscript SNPPARALLEL.R 6 4 &
	Rscript SNPPARALLEL.R 6 5 &
	wait

    elif ["$3" == "--8"]
    then
	
	echo "using 6 cores for computation"
	
	Rscript SNPPARALLEL.R 8 0 &
	Rscript SNPPARALLEL.R 8 1 &
	Rscript SNPPARALLEL.R 8 2 &
	Rscript SNPPARALLEL.R 8 3 &
	Rscript SNPPARALLEL.R 8 4 &
	Rscript SNPPARALLEL.R 8 5 &
	Rscript SNPPARALLEL.R 8 6 &
	Rscript SNPPARALLEL.R 8 7 &
	wait

    elif ["$3" == "--16"]
    then
	
	Rscript SNPPARALLEL.R 16 0 &
	Rscript SNPPARALLEL.R 16 1 &
	Rscript SNPPARALLEL.R 16 2 &
	Rscript SNPPARALLEL.R 16 3 &
	Rscript SNPPARALLEL.R 16 4 &
	Rscript SNPPARALLEL.R 16 5 &
	Rscript SNPPARALLEL.R 16 6 &
	Rscript SNPPARALLEL.R 16 7 &
	Rscript SNPPARALLEL.R 16 8 &
	Rscript SNPPARALLEL.R 16 9 &
	Rscript SNPPARALLEL.R 16 10 &
	Rscript SNPPARALLEL.R 16 11 &
	Rscript SNPPARALLEL.R 16 12 &
	Rscript SNPPARALLEL.R 16 13 &
	Rscript SNPPARALLEL.R 16 14 &
	Rscript SNPPARALLEL.R 16 15 &
	
    else

	echo "Doing the default computation with 4 cores.."

	Rscript SNPPARALLEL.R 4 0 &
	Rscript SNPPARALLEL.R 4 1 &
	Rscript SNPPARALLEL.R 4 2 &
	Rscript SNPPARALLEL.R 4 3 &

	wait
	
    fi

    cd ../FinalResults/SNP
    gzip *
    cd ../../pipefiles
    echo "Done with SNP location detection.."

    #Download all sequences for validation, check if PDBs were already downloaded..

    if [ "$4" == "--validate" ];

    then
	echo 'Beginning validation on sequence level..'

	cd ../FinalResults/PDB
	
	echo "Downloading PDB files"

	# only add PDB if not exists already, minimal request overhead is redundant in this case

	for en in ../SNP/*.txt; do

	    STEP=$(basename "${en}" .txt)

	    if [ -f "$STEP".pdb ];
	       
	    then
		
		echo ".."

	    else

		STEP=$(basename "${en}" .txt)
		wget http://www.rcsb.org/pdb/files/"$STEP".pdb

	    fi
	    
	done

	echo 'Finished downloading PDB files..'

	#Convert all PDBs to sequences..
	echo "Converting the PDB to sequences for validation"

	cd ../


	echo "Fasta conversion in process.."

	# Only convert the nonexisting ones..
	for et in PDB/*.pdb; do

	    STEG=$(basename "${et}" .pdb)

	    
	    if [ -f PDB_SEQ/"$STEG".fasta ]
	       
	    then
		
		echo "Passing on.. File exists!"

	    else
		
		../pipefiles/./pdb2fasta.sh "$et" > PDB_SEQ/"$STEG".fasta
		#echo "converting"
		
	    fi
	    
	done

	cd ../pipefiles

	##Validation algorithm in R
	#Rscript SNPvalidator.R


	if [ "$3" == "--4" ];
	   
	then
	    echo "Using 4 cores for computation"

	    Rscript SNPPARALLELVALIDATION.R 4 0 &
	    Rscript SNPPARALLELVALIDATION.R 4 1 &
	    Rscript SNPPARALLELVALIDATION.R 4 2 &
	    Rscript SNPPARALLELVALIDATION.R 4 3 &
	    wait
	    
	elif [ "$3" == "--6" ];

	then
	    echo "using 6 cores for computation"
	    
	    Rscript SNPPARALLELVALIDATION.R 6 0 &
	    Rscript SNPPARALLELVALIDATION.R 6 1 &
	    Rscript SNPPARALLELVALIDATION.R 6 2 &
	    Rscript SNPPARALLELVALIDATION.R 6 3 &
	    Rscript SNPPARALLELVALIDATION.R 6 4 &
	    Rscript SNPPARALLELVALIDATION.R 6 5 &
	    wait

	elif ["$3" == "--8"]
	then
	    
	    echo "using 6 cores for computation"
	    
	    Rscript SNPPARALLELVALIDATION.R 8 0 &
	    Rscript SNPPARALLELVALIDATION.R 8 1 &
	    Rscript SNPPARALLELVALIDATION.R 8 2 &
	    Rscript SNPPARALLELVALIDATION.R 8 3 &
	    Rscript SNPPARALLELVALIDATION.R 8 4 &
	    Rscript SNPPARALLELVALIDATION.R 8 5 &
	    Rscript SNPPARALLELVALIDATION.R 8 6 &
	    Rscript SNPPARALLELVALIDATION.R 8 7 &
	    wait

	elif ["$3" == "--16"]
	then
	    
	    Rscript SNPPARALLELVALIDATION.R 16 0 &
	    Rscript SNPPARALLELVALIDATION.R 16 1 &
	    Rscript SNPPARALLELVALIDATION.R 16 2 &
	    Rscript SNPPARALLELVALIDATION.R 16 3 &
	    Rscript SNPPARALLELVALIDATION.R 16 4 &
	    Rscript SNPPARALLELVALIDATION.R 16 5 &
	    Rscript SNPPARALLELVALIDATION.R 16 6 &
	    Rscript SNPPARALLELVALIDATION.R 16 7 &
	    Rscript SNPPARALLELVALIDATION.R 16 8 &
	    Rscript SNPPARALLELVALIDATION.R 16 9 &
	    Rscript SNPPARALLELVALIDATION.R 16 10 &
	    Rscript SNPPARALLELVALIDATION.R 16 11 &
	    Rscript SNPPARALLELVALIDATION.R 16 12 &
	    Rscript SNPPARALLELVALIDATION.R 16 13 &
	    Rscript SNPPARALLELVALIDATION.R 16 14 &
	    Rscript SNPPARALLELVALIDATION.R 16 15 &
	    
	else

	    echo "Doing the default computation"

	    Rscript SNPPARALLELVALIDATION.R 4 0 &
	    Rscript SNPPARALLELVALIDATION.R 4 1 &
	    Rscript SNPPARALLELVALIDATION.R 4 2 &
	    Rscript SNPPARALLELVALIDATION.R 4 3 &

	    wait
	    
	fi


	#ls ../FinalResults/SNP | xargs -n1 -P 3
	#Rscript SNPvalidatorP.R

	echo "Validation process finished.."
	
	rm -rvf ../SNPDATA
	mkdir ../SNPDATA
	
	cd ../FinalResults
	cp -rf PDB_SNP_VALIDATED ../SNPDATA

	## SNPDATA includes data to be served to the REST service
    else
	echo "Process finished."
    fi
    
fi


## This version maps ALL RS polymorphisms found in the Uniprot database..

if [ "$1" == "--allRS"  ];
then
    cd ..

    mkdir PreviousInputFiles

    ## if not exists it will skip it, this is for backup
    mkdir InputFilesVariation

    cp -a InputFilesVariation PreviousInputFiles
    rm -rvf InputFilesVariation && mkdir InputFilesVariation

    cd InputFilesVariation
    echo "Obtaining files.."
    
    if [ "$2" == "--UseBackup" ];
    then
	echo "Using backup files.."
    else
	wget -m -nH --cut-dirs=6  ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/    
    fi
    
    echo "Finding files.."

    cd ..

    rm -rvf ExtractedFiles

    mkdir ExtractedFiles
    if [ "$2" == "--UseBackup"  ];
    then
	echo "Using backup files.."
	cd PreviousInputFiles/InputFilesVariation
	for f in *.gz; do
	    STEM=$(basename "${f}" .gz)
	    gunzip -cv "${f}" > ../../ExtractedFiles/"${STEM}"
	done
	cd ../..
    else

	echo "Using downloaded files.."
	cd InputFilesVariation
	for f in *.gz; do
	    STEM=$(basename "${f}" .gz)
	    gunzip -cv "${f}" > ../ExtractedFiles/"${STEM}"
	done
	cd ..
	
    fi

    #parallel --eta 'gunzip {} > ExtractedFiles/{.}' ::: InputFilesVariation/*.gz
    echo "File extraction completed!"

    ## sifts download and extract script for SIFT dataset
    echo "SIFT table downloaded"
    
    mkdir SIFT
    cd SIFT
    wget ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_uniprot.tsv.gz
    gunzip -cv pdb_chain_uniprot.tsv.gz >"sifts.tsv" 

    echo "Cleaning files.."
    ## for each extracted entry skip first 144 lines! - this is a description of a file.


    cd ..
    rm -rvf ProcessedDataTables


    mkdir ProcessedDataTables

    cd ExtractedFiles

    for fh in *.txt; do
	STEM=$(basename "${fh}" .txt)
	echo "Processing dataset... $STEM"
	cat "${fh}" | tail -n +144 > ../ProcessedDataTables/"$STEM"
    done

    rm -rvf ../ExtractedFiles
    rm -rvf ../InputFilesVariation

    ## call to R script for file joining!
    cd ../ProcessedDataTables


    ## split for faster processing

    find . -type f -size +200M -exec split -b 100M -d {} {} \;
    find . -type f -size +200M -exec rm -rvf {} \;


    cd ../pipefiles

    # Clean any previous versions!

    mkdir ../FinalResults

    # make fresh results each run
    
    rm -rvf ../FinalResults/SNP2
    rm -rvf ../FinalResults/PDB_SNP_VALIDATED2
    mkdir ../FinalResults/SNP2
    #mkdir ../FinalResults/images
    mkdir ../FinalResults/PDB_SNP_VALIDATED2

    # keep converted and download PDB files
    mkdir ../FinalResults/PDB
    mkdir ../FinalResults/PDB_SEQ

    # #Run main algorithm which fills the SNP2 folder!

    Rscript SNPfinderAll2.R 

    wait

    echo "Done with SNP location detection.."

    #Download all sequences for validation, check if PDBs were already downloaded..

    echo 'Beginning validation..'

    cd ../FinalResults/PDB
    
    echo "Downloading PDB files"

    # only add PDB if not exists already

    for en in ../SNP2/*.txt; do

	STEP=$(basename "${en}" .txt)

	if [ -f "$STEP".pdb ];
	   
	then
	    
	    echo "File $en already exists, moving on.."

	else	  

	    STEP=$(basename "${en}" .txt)
	    wget http://www.rcsb.org/pdb/files/"$STEP".pdb

	fi
	
    done

    echo 'Finished downloading PDB files..'

    #Convert all PDBs to sequences..
    echo "Converting the PDB to sequences for validation"

    cd ../


    echo "Fasta conversion in process.."

    #Only convert the nonexisting ones..
    #Additional PDBs will be sufficiently downloaded..
    for et in PDB/*.pdb; do

	STEG=$(basename "${et}" .pdb)

	
	if [ -f PDB_SEQ/"$STEG".fasta ];
	   
	then
	    
	    echo "Passing on.. File exists!"

	else
	    
	    ../pipefiles/./pdb2fasta.sh "$et" > PDB_SEQ/"$STEG".fasta
	    
	    #echo "converting"
	    
	fi
	
    done
    rm -rvf ../SNPDATA
    mkdir ../SNPDATA
    cd ../pipefiles


    # #Validation algorithm in R
    Rscript SNPvalidator2.R

    echo "Validation process finished.."

    cd ../FinalResults
    cp -rf PDB_SNP_VALIDATED2 ../SNPDATA
    
fi

# Validate results, one must have obtained SNP PDB PDB_SEQ folders for this
if [ "$1" == "--validate" ];
then

    Rscript SNPvalidator.R

    echo "Validation process finished.."
    
fi

#find using ram - BEWARE! If files are not splitted previously, this might cause breakdowns!

if [ "$1" == "--find" ];
then

    Rscript SNPfinder2.R

    echo "Found mappings are in the SNP folder in results section.."
fi

# this finds all polymorphisms, not just RS type.
if [ "$1" == "--findAll" ];
then

    Rscript SNPfinderStrict.R

    echo "Found mappings are in the SNP folder in results section.."
fi

#run parallel version
if [ "$1" == "--findP" ];
then
    
    Rscript SNPfinderP1.R &
    Rscript SNPfinderP2.R &
    Rscript SNPfinderP3.R &
    Rscript SNPfinderP4.R &
    
    wait

    echo "Parallel mapping complete.."  
fi


#clean the files for fresh start
if [ "$1" == "--clean" ];
then

    echo "Are you sure you want to proceed? (y/n)"

    read uinput

    if [ "$uinput" == "y" ];then
	
	cd ..
	rm -rvf SIFT
	## Precious files!
	#rm -rvf PreviousinputFiles
	rm -rvf ProcessedDataTables
	rm -rvf FinalResults

    else

	echo "Exiting .."
    fi

fi

#Convert from PDB to sequences, needs folders PDB and PDB_SEQ

if [ "$1" == "--pdb2seq" ];
   
then

    cd ../FinalResults


    for et in PDB/*.pdb; do

	STEG=$(basename "${et}" .pdb)

	
	if [ -f PDB_SEQ/"$STEG".fasta ]
	   
	then
	    
	    echo "Passing on.. File exists!"

	else
	    
	    ../pipefiles/./pdb2fasta.sh "$et" > PDB_SEQ/"$STEG".fasta
	    #echo "converting"
	    
	fi
	
    done
    
fi

# this needs results in FinalResults/SNP folder

if [ "$1" == "--downloadPDB" ];
   
then
    cd ../FinalResults
    mkdir PDB
    cd PDB
    
    for en in ../SNP/*.txt; do

	STEP=$(basename "${en}" .txt)

	if [ -f "$STEP".pdb ]
	   
	then
	    
	    echo "File $en already exists, moving on.."

	else

	    STEP=$(basename "${en}" .txt)
	    wget http://www.rcsb.org/pdb/files/"$STEP".pdb

	fi
	
    done
    

fi

if [ "$1" == "--validateP" ];
   
then

    Rscript SNPvP1.R &
    Rscript SNPvP2.R &
    Rscript SNPvP3.R &
    Rscript SNPvP4.R &
    Rscript SNPvP5.R &
    Rscript SNPvP6.R &

    wait

    echo "validation complete!"
fi
## file splitting process, needs processed tables present

if [ "$1" == "--splitfiles"  ];

then

    mkdir ../ProcessedDataTables
    cd ../ExtractedFiles

    for fh in *.txt; do
	STEM=$(basename "${fh}" .txt)
	echo "Processing dataset... $STEM"
	cat "${fh}" | tail -n +144 > ../ProcessedDataTables/"$STEM"
    done


    ## call to R script for file joining!
    cd ../ProcessedDataTables

    ## split for faster processing

    find . -type f -size +200M -exec split -b 100M -d {} {} \;
    find . -type f -size +200M -exec rm -rvf {} \;

fi


