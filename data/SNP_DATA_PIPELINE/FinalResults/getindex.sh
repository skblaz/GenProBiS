#ls ../..//  | mawk -F "_" '{gsub(".json.gz","",$3);print $2$3}' > examples.txt
cat Wholebase.txt | head -n 80000 | mawk '{if (length($6) == 1) print $1"\n"$2"\n"$5$6}' | sort -u  > examples.txt

cat examples.txt | wc -l

cat Wholebase.txt | tail -n 4000 |mawk '{if (length($6) == 1) print $4}'| sort -u  >> examples.txt

cat examples.txt | wc -l

cat Wholebase.txt | mawk '{if (length($6) == 1) print $4"\n"$1"\n"$2"\n"$5$6}' | sort -u  > indexdb.txt

cat Wholebase.txt | mawk '{split($8,a,"_");print a[1]"_"a[2],$4}' | sort -u | mawk '{print $1}' |sort | uniq -c > SummarySpecies.txt

cat Wholebase.txt | mawk '{print $8,$5$6}' | sort -u | mawk '{split($1,a,"_");print a[1]"_"a[2]}' | sort |uniq -c > pdbSpecies.txt

## this constructs the indexes for the fast search

nodejs map_whole_db.js uni &
nodejs map_whole_db.js snp &

wait

echo "DB index done!"
