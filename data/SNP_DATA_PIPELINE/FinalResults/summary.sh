
## species summary

cat WholeBase.txt | mawk '{split($8,a,"_");print a[1]"_"a[2],$4}' | sort -u | mawk '{print $1}' |sort | uniq -c > SummarySpecies.txt

cat WholeBase.txt | mawk '{print $8,$5$6}' | sort -u | mawk '{split($1,a,"_");print a[1]"_"a[2]}' | sort |uniq -c > pdbSpecies.txt

cat WholeBase.txt | mawk '{ split($8,a,"_");print $4,a[1]"_"a[2] }' | sort -u |mawk '$1 ~ /rs/{print $2}' | sort | uniq -c > rsSpecies.txt

cat WholeBase.txt | mawk '{ split($8,a,"_");print $4,a[1]"_"a[2] }' | sort -u |mawk '$1 ~ /COSM/{print $2}' | sort | uniq -c > COSMSpecies.txt

cat WholeBase.txt | mawk '{ split($8,a,"_");print $4,a[1]"_"a[2] }' | sort -u |mawk '$1 !~ /COSM/ && $1 !~ /rs/{print $2}' | sort | uniq -c > otherSpecies.txt
