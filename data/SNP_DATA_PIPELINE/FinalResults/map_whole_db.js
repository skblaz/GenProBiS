// this searches through the index and finds the correct entries!


var readline = require('readline');
var fs = require('fs');


function generate_indexSNP(){

    var basePATH = "Wholebase.txt";
    var keys = new Object();
    var rd = readline.createInterface({
	input: fs.createReadStream(basePATH),
	output: process.stdout,
	terminal: false
    });

    var totalEntries = 0;
    var ltable = new Object();
    rd.on('line', function(line) {	    
	try{
	    // SAME  FOR SNPs
//	    console.log(line)
            var key = line.split(" ")[3];

	    var candidate = line.split(" ")[4]+"_"+line.split(" ")[5]+"_"+line.split(" ")[7].split("_").slice(0,2).join("_");
	    var id = line.split(" ")[3]

	    if (key in ltable){

		if (ltable[key].indexOf(candidate) == -1){
		    ltable[key].push(candidate);
		    //console.log(ltable);
		}
	    }else{	    
		ltable[key] = [candidate];

	    }
	    
	}catch(err){
	    console.log(err)

	}
	
    });

    rd.on('close',function(){

	fs.writeFile("MAP_INDEX/PDB_SNPMapping.json", JSON.stringify(ltable), function(err) {
    	    if(err) {
		return console.log(err);
    	    }

	}); 
	console.log("Index created!");
	
    });

    rd.on('error',function(){
	console.log('error Reading file');

    });
}

// Uni generates both!
function generate_indexUni(){


    var basePATH = "Wholebase.txt";
    var keys = new Object();
    var rd = readline.createInterface({
	input: fs.createReadStream(basePATH),
	output: process.stdout,
	terminal: false
    });

    var totalEntries = 0;
    var ltable = new Object();
    rd.on('line', function(line) {	    

	//protein names

	try{
	var key = line.split(" ")[0];
	var candidate = line.split(" ")[4]+"_"+line.split(" ")[5]+"_"+line.split(" ")[7].split("_").slice(0,2).join("_");
	
	if (key in ltable){

	    if (ltable[key].indexOf(candidate) == -1){
		ltable[key].push(candidate);
		//totalEntries++;
		//console.log(ltable);
	    }
	}else{	    
	    ltable[key] = [candidate];
	    totalEntries++;

	}

	// SAME FOR GENES
	var key = line.split(" ")[1];
	
	if (key in ltable){

	    if (ltable[key].indexOf(candidate) == -1){
		ltable[key].push(candidate);
		//console.log(ltable);
	    }
	}else{	    
	    ltable[key] = [candidate];

	}
	// if (totalEntries % 200 == 0){
	//     console.log(totalEntries+" UniProt entries found!");
	    // }
	}catch(err){

	    console.log(err)
	}

    });

    rd.on('close',function(){

	fs.writeFile("MAP_INDEX/PDB_UniGenMapping.json", JSON.stringify(ltable), function(err) {
    	    if(err) {
		return console.log(err);
    	    }

	}); 
	console.log("Index created!");
	
    });

    rd.on('error',function(){
	console.log('error Reading file');

    });
}


// function generate_indexGen(){

//     var basePATH = "WholeBase.txt";
//     var keys = new Object();
//     var rd = readline.createInterface({
// 	input: fs.createReadStream(basePATH),
// 	output: process.stdout,
// 	terminal: false
//     });

//     var totalEntries = 0;
//     var ltable = new Object();
//     rd.on('line', function(line) {	    


	
// 	// SAME FOR GENES
// 	var key = line.split("\t")[1];
// 	var candidate = line.split("\t")[4]+"_"+line.split("\t")[5]+"_"+line.split("\t")[7].split("_").slice(0,2).join("_");
	
// 	if (key in ltable){

// 	    if (ltable[key].indexOf(candidate) == -1){
// 		ltable[key].push(candidate);
// 		//console.log(ltable);
// 	    }
// 	}else{	    
// 	    ltable[key] = [candidate];

// 	}

//     });

//     rd.on('close',function(){

// 	fs.writeFile("MAP_INDEX/PDB_GenMapping.json", JSON.stringify(ltable), function(err) {
//     	    if(err) {
// 		return console.log(err);
//     	    }

// 	}); 
// 	console.log("Index created!");
	
//     });

//     rd.on('error',function(){
// 	console.log('error Reading file');

//     });



// }


if (process.argv[2] == "snp"){
    generate_indexSNP()
    
}else if (process.argv[2] == "uni"){
    generate_indexUni()


}else if (process.argv[2] == "gen"){
    generate_indexGen()

}
else{
    console.log("Please provide the script argument! (uni, gen or snp)")
}
