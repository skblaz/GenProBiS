// this searches through the index and finds the correct entries!


var readline = require('readline');
var fs = require('fs');

var basePATH = "WholeBase.txt";
var keys = new Object();
var rd = readline.createInterface({
    input: fs.createReadStream(basePATH),
    output: process.stdout,
    terminal: false
});

var ltable = new Object();
rd.on('line', function(line) {	    

    ltable[line.split("\t")[4]] = line.split("\t")[0]

});

rd.on('close',function(){

    fs.writeFile("PDB_UniMapping.json", JSON.stringify(ltable), function(err) {
	if(err) {
            return console.log(err);
	}

    }); 
    console.log("Index created!");
    
});

rd.on('error',function(){
    console.log('error Reading file');

});
