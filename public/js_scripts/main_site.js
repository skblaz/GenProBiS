//this javascript code handles the main site along with serverside requests!
//this script should send the ajax request to server with the pdb query.

//global data structure for current view. - grows when possible.

var tempadress = "tyr.cmm.ki.si/gp";
url_main = "http://"+tempadress+"/get_data";
url_script = "http://"+tempadress+"/get_global";
url_index = "http://"+tempadress+"/get_index";
url_pdb = "http://"+tempadress+"/get_pdb";
url_bs = "http://insilab.org/api/probisdb?pdbid=PROTEIN&chain=CHAIN&key=bresi"
	url_buffer = "http://insilab.org/api/probisdb?pdbid=PROTEIN&chain=CHAIN&key=surf"
		url_buffer2 = "http://"+tempadress+"/get_buffer";
url_ligand = "http://insilab.org/api/probisdb?pdbid=PROTEIN&chain=CHAIN&key=ligands";
url_ligand2 = "http://"+tempadress+"/get_ligands";
url_examples = "http://"+tempadress+"/get_examples";
url_dbinfo = "http://"+tempadress+"/get_dbinfo";
url_seqplot = "http://"+tempadress+"/get_seqplot";

var enterPressed = false;
colorScheme = {"SNP": [179,67,146,255], "label": [255, 255, 0], "active": "#ff9900", "inactive": "#464646", "inactive_rgb": "rgb(70, 70, 70)", "textColor": "#f5f5f5", "surfaceColors": {"Predicted": [0,255,0,130], "Predicted_SNP": [0,255,0,70], "protein" : [255,255,0,130], "compound" : [0,255,0,130], "nucleic" : [64,224,208,130], "ion" : [255,128,0,130], "noinfo": [0,255,0,130]}, "seqLegend": {"ion": "#FF8000", "compound": "#00d300", "nucleic": "#40d7c7", "protein": "#e6e100"}, "surfaceToColorWord": {"protein": "yellow", "compound": "green", "nucleic": "turquoise", "ion": "brown"}};
tries = 0;
data_recieved = false;
surface_type = true; //which surface will be displayed, either false: none, true: simple or "detailed": detailed.
var searchHistory = [];
var maxSearchNo = 5;
var data_examples = [];
var out_json = new Object();

var three2one = {"ALA" : "A", "ARG" : "R","ASN" : "N","ASP" : "D","ASX" : "B", "CYS" : "C","GLU" : "E","GLN" : "Q","GLX" : "Z", "GLY" : "G","HIS" : "H","ILE" : "I","LEU" : "L","LYS" : "K","MET" : "M","PHE" : "F", "PRO" : "P","SER" : "S","THR" : "T","TRP" : "W","TYR" : "Y","VAL" : "V","BSER" : "S","BASP" : "N"}
//dynamic table searching!


var alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];

//custom sorting for binding sites x
function sorting(a, b){
	a1 = a.split("_")[1];
	b1 = b.split("_")[1];
	if (a1<b1)
		return -1;
	else if (a1>b1)
		return 1;
	else {
		a = a.split("_")[0];
		b = b.split("_")[0];
		return eval(a.replace( /^\D+/g, '')) - eval(b.replace( /^\D+/g, ''));
	}
}

//custom sorting for ligands
function ligandSort(a, b) {
	try {
		if (a.ltype == "protein" || a.lytpe == "nucleic")
			textA = (a.lig_id).split("_")[2]+(a.lig_id).split("_")[3];
		else
			textA = (a.lig_id).split("_")[4];
		if (b.ltype == "protein" || b.lytpe == "nucleic")
			textB = (b.lig_id).split("_")[2]+(b.lig_id).split("_")[3];
		else
			textB = (b.lig_id).split("_")[4];
	}
	catch(e) {
		textA = a;
		textB = b;
	}
	if (/^\d+$/.test(textB) && !/^\d+$/.test(textA))
		return 1;
	else if (/^\d+$/.test(textA) && !/^\d+$/.test(textB))
		return -1;
	else
		return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
}

function initDsearch(what,where) {

	$("."+what).keyup(function () {
		var searchTerm = $("."+what).val();
		var listItem = $("[id='"+where+"'] tbody").children('tr');
		var searchSplit = searchTerm.replace(/ /g, "'):containsi('");

		$.extend($.expr[':'], {'containsi': function(elem, i, match, array){
			return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
		});

		$("[id='"+where+"']"+" tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
			$(this).attr('visible','false');
		});

		$("[id='"+where+"']"+" tbody tr:containsi('" + searchSplit + "')").each(function(e){
			$(this).attr('visible','true');
		});

		//console.log($('#'+where));
		var jobCount = $("[id='"+where+"']"+" tbody tr[visible='true']").length;
		$('.counter').text(jobCount + ' results found');

		if(jobCount == '0') {$('.no-result').show();}
		else {$('.no-result').hide();}
	});
}

function restartDsearch(what,where) {
	$('.no-result').hide();
	$('.counter').text("");
	$("."+what).val("");

	$("[id='"+where+"']"+" tbody tr").each(function(e){
		$(this).attr('visible','true');
	});
}

function draw_snp_dist(objekt){

	var absolute_locations = objekt.seq_info.validated_locations;
	// draw the seqplot with the absolute locations, previously mapped in the seqview module
	var difference = function (a, b) { return Math.abs(a - b) };

	// get surface points
	var surfacepoints = (function change(input){
		var tmplist = [];
		input.forEach(function(ent){
			tmplist.push(ent.resi)
		});

		return tmplist;

	})(objekt.newSurface);
	//	var surfacepoints = (function change(input){
	//	var tmplist = [];
	//	input.forEach(function(ent){
	//	if (ent.indexOf("_") != -1){
	//	if (isNumber(ent.split("_")[2])){
	//	tmplist.push(ent.split("_")[2]);
	//	}else{
	//	tmplist.push(ent.split("_")[3]);
	//	}
	//	}else{
	//	tmplist.push(ent);
	//	}	 
	//	});

	//	return tmplist;

	//	})(objekt.surface);

	// compute the cluster coefficient
	var cluster_coefficient = function(objektek) {
		//DESCRIPTION OF THIS PART>
		//USE THE FORMULA FROM THE ARTICLE.
		var lokiji = objektek.snpdata;
		var surfacepoints = [];

		var combi = 0;
		var cnum = 0;
		for(var k = 0; k< lokiji.length;k++){
			for (var j = (k+1);j<lokiji.length;j++){
				combi += difference(lokiji[k].split("_")[6],lokiji[j].split("_")[6]);
				cnum += 1;
			}
		}
		var mval = (function getmax(arr){
			var currmax = 0
			arr.forEach(function(entry){
				if (entry > currmax){ currmax = entry;}
			});

			return currmax

		})(lokiji);
		//		console.log(mval);
		return (combi/(cnum)).toFixed(2);
	};  

	// call the cluster coefficient

	$('#cluster_num').html("&Phi; cluster coefficient: "+cluster_coefficient(objekt));


	// map SNPs to the surface data..

	var PPISNPs = (function getPPISNP(i1,i2){

		pisnps = [];

		i2.forEach(function(snip){

			if (i1.indexOf(snip) != -1 && pisnps.indexOf(snip) == -1){
				pisnps.push(snip);
			}

		});

		return pisnps

	})(absolute_locations, surfacepoints); 

	//prepare the plot..

	var trace1 = {
			x: absolute_locations,
			y: absolute_locations,
			mode: 'markers',
			name : 'Sequence Variant'
	};


	var trace2 = {
			x: surfacepoints,
			y: surfacepoints,
			mode: 'markers',
			name : 'AA in predicted interaction',
			marker : {
				size : '8',
				color: 'rgb(0,135,0)'
			}
	};

	var trace3 = {
			x: PPISNPs, //objekt.piSNP
			y: PPISNPs,
			mode: 'markers',
			name : 'Sequence Variant in interaction region',
			marker : {
				size : '16',
				symbol : 'star-open',
				color: 'rgb(200,100,100)'
			}	
	};

	var data = [trace1, trace2, trace3];

	var layout = {
			title:'Sequence Variant region distribution',
			xaxis: {
				title: 'Amino acid position',
				showgrid: false,
				zeroline: false
			},
			yaxis: {
				title: 'Amino acid position',
				showline: false
			}
	};

	Plotly.newPlot('snp_viz', data, layout);
}

function change_events(){

	$('#menu_main').toggle();
	$('#header').toggle();
	$('#footer').toggle();
	$('#results_page').toggle();
	$('#draggable').toggle();
	$('#main_results_menu_button').toggle();
	$('#main_results_legend_button').toggle();
	$('#DatabaseList').modal('hide');
	document.getElementById("bodymain").style.overflow = "hidden";

}

function change_events_main(){

	closeNav(); 
	closeLegend();
	$('#draggableTable').css('display','none');
	$('#draggable').css('display','none');
	$('#draggableLigandPlayer').css('display','none');
	$('#results_page').css('display','none');
	$('#main_results_menu_button').css('display','none');
	$('#main_results_legend_button').css('display', 'none');
	$('#menu_main').toggle();
	$('#header').toggle();
	$('#footer').toggle();
	document.getElementById("bodymain").style.overflow = "";
}

window.mobilecheck = function() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};

if (window.mobilecheck()) {
	var link = document.getElementById("mobileCSS");

	link.setAttribute("href", "css/coverMobile.css"); 
}

function newSurfaceFun(obj,jsonSurf) {
	try {
		newObj = JSON.parse(jsonSurf);
	}
	catch(e) {
		console.log("no bs on insilab, taking bs from SNPobject")
		newObj = [];
		for (var i=0; i<obj.length; i++) {
			oldInfo = obj[i].split("_");
			if (oldInfo.length==4)
				newObj.push({"bs_id":oldInfo[1],"ltype":oldInfo[2],"resi":oldInfo[3]});
			else
				newObj.push({"bs_id":oldInfo[1],"ltype":"noinfo","resi":oldInfo[2]});
		}
	}
	return newObj
}

function send_query(u_input){
	u_input = u_input.substring(0,u_input.length-1).toLowerCase()+u_input.substring(u_input.length-1).toUpperCase();
	data_recieved="finished";
	tries++;

	//get the data from the input tab, encode it as the data, send to server.

	//var url_main = "http://localhost:3000/get_data";  //get_data is default
	var pdb_id = u_input;
	//	$.ajax({
	//		url: "./grid_"+pdb_id.slice(0, -1)+"_"+pdb_id.slice(-1)+".pdb",
	//		success: function (data) {
	//			var struc = load_surf_pdb(data, pdb_id);
	//			generateSurfaces_pdb(struc.chains[0]);
	//},
	//		error: function (xhr, status, error) {	    
	//			$("#customAlerts").html("<div class='alert alert-danger' role='alert'><strong>Warning!</strong> Inputted PDB doesn't exist! <strong> Generating the file </strong>, if this screen doesn't close in a few seconds, unfortunately no results were found</div><img src='slike/Lscreen.gif' alt='Lscrean' style='width:100px;height:70px;'>");  
	//			if(tries < 9){
	//				console.log("Query resent!");
	//			}else{		
	//				$("#customAlerts").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> No SNPs were found for this PDB.</div>");
	//				throw new Error ('SNPs non-existing..');
	//
	//			}
	//		},
	//	});

	if (window.mobilecheck()) {
		$.when(ajax2(), ajax4()).done(function(a2, a4){
			$.when(UpdateStatus()).done(function() {
				try{

					var currentResult = document.getElementById('currentResult');

					// replace text in HTML string:
					currentResult.innerHTML = u_input;
					var SNPobject = {"snpdata": JSON.parse(a2[0])};	
					SNPobject["newSurface"] = newSurfaceFun(SNPobject["surface"],a4[0]);
					
					// console.log(SNPobject);
					if(SNPobject.snpdata != "undefined" && SNPobject.snpdata.length > 0 && SNPobject.snpdata != null){
						$("#customAlerts").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> No Sequence Variants were found for this PDB.</div>");

						try {
							console.log('Data obtained, beginning plotting: ');
							data_recieved=true;

							var cresult = init_SNP_mobile(SNPobject, pdb_id);

							if (cresult == 0){    
								$("#customAlerts").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> Sequence Variants were only found for <strong>different chains! </strong>");
								tries=8;   
							}
							else{
								console.log("table generated..");
								change_events();

								restartDsearch("searchSNP", "genTab");
								initDsearch("searchSNP", "genTab");
								openNav(); // po defaultu odpre menu
							}


							//reloadViewer(pdb_id, a2[0]); //reload viewer here.
							$('#loadmodal').modal('hide'); // hide loading dialog.
							//genome view shall display here!
							$('#loadmodal').modal('hide'); // hide loading dialog.
							$("#customAlerts").html("");
							tries = 0;
						} 
						catch(e1){
							//console.log("Table couldn't be generated.");
						} 
					}else{
						$("#customAlerts").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> No Sequence Variants were found for this PDB.</div>");
						throw new Error('Sequence Variants non-existing..');
						setTimeout(hide_loading_screen(), 3000);
					}
				}catch(e){
					$("#customAlerts").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> No Sequence Variants were found for this PDB.");

				}
			});
		});
	}

	else {
		$.when(ajax1(), ajax2(), ajax3(), ajax4()).done(function(a1, a2, a3, a4){
			$.when(UpdateStatus()).done(function(){

				$("#displaySurfaceBtn").removeClass('active');
				$("#displaySticksBtn").removeClass('active');
				
				try {

					global_buffers = JSON.parse(a1[0]);

					for (var i=0; i<global_buffers.length; i++) {
						global_buffers[i]["surf"].vertexBuffer = new Float32Array(global_buffers[i]["surf"]["vertexBuffer"]);
						global_buffers[i]["surf"].indexBuffer = new Uint32Array(global_buffers[i]["surf"]["indexBuffer"]);
						global_buffers[i]["surf"].vertexBuffer8 = new Uint8Array(global_buffers[i]["surf"].vertexBuffer.buffer);
					}
					surface_type = "detailed";
					global_buffer = global_buffers[0];
					document.getElementById('displaySurfaceBtn').value = "Surface";
					$('#displaySurfaceBtn').prop('disabled', false);	

				}
				catch(e1) {
					console.log("no surface");
					global_buffer = null;
					global_buffers = [];
					surface_type = true;
					document.getElementById('displaySurfaceBtn').value = "No surface available";
					$('#displaySurfaceBtn').prop('disabled', true);			
				}

				(function (cb) {
					try {
						ligandObject = JSON.parse(a3[0]).sort(ligandSort);

					}
					catch(e1) {
						console.log("no ligands were found for this PDB");
						ligandObject = [];
					}

					cb(ligandObject);
				})(initiateSNPobject);


				function initiateSNPobject(ligandObject) {
				    try{
						SNPobject = {"snpdata": JSON.parse(a2[0])};
						//NEW CODE
						if (SNPobject.snpdata == "undefined"){
							$("#customAlerts").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> No Sequence Variants were found for this PDB.</div>");
							throw new Error('SNPs non-existing..');
							setTimeout(hide_loading_screen(), 3000);
						}
						else if(SNPobject.snpdata.length == 0){
							$("#customAlerts").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> No Sequence Variants were found for this PDB.</div>");

						}
					
						else {
							try {
								SNPobject["newSurface"] = JSON.parse(a4[0]||"[]");

								if (ligandObject.length == 0 || (SNPobject["newSurface"]).length == 0) {
									SNPobject["surface"] = [];
									SNPobject["newSurface"] = [];
									//document.getElementById('ligandPlayerTitle').title = "No surface available";
								}

								console.log('Data obtained, beginning plotting: ');
								data_recieved=true;

								try{
									reloadViewer(pdb_id, SNPobject, function(){
										document.getElementById("main_results_menu_button").value = pdb_id;
										$('#loadmodal').modal('hide'); // hide loading dialog.
										$('#loadmodal').modal('hide'); // hide loading dialog.
										$("#customAlerts").html("");
										tries = 0;
									}); //reload viewer here.
									//draw_snp_dist(SNPobject);

									//document.getElementById("compound").checked = true;
									//document.getElementById("ion").checked = true;
									//document.getElementById("nucleic").checked = true;
									//document.getElementById("protein").checked = true;

									var pdbid = pdb_id.substring(0, 4).toLowerCase();
									var chainid = pdb_id.substring(4, 5).toUpperCase();
									var link = "http://genprobis.insilab.org/gp/api?pdb="+pdbid+"&chain="+chainid+"&type=csv"
									$('#readable_download_link').attr("href",link);

									//added callback with correct residue locations!
								}catch(e2){
									console.log(e2);
									console.log('SNP dist couldnt be generated');
									reloadViewer(pdb_id, SNPobject); //reload viewer here.
									//reloadViewer(pdb_id, a2[0]); //reload viewer here.
									document.getElementById("main_results_menu_button").value = pdb_id;
									$('#loadmodal').modal('hide'); // hide loading dialog.

									$('#loadmodal').modal('hide'); // hide loading dialog.
									$("#customAlerts").html("");
									tries = 0;

								}

							} 
							catch(e1){
								//console.log("Table couldn't be generated.");
							} 
						}
					}catch(e){
						$("#customAlerts").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> No Sequence Variants were found for this PDB.");

					}
				}

			});
		});
		function ajax1() {
			return $.ajax({
				url: url_buffer.replace("PROTEIN",pdb_id.slice(0, -1)).replace("CHAIN",pdb_id.slice(-1)),
				type : 'GET',
				success : function(data) {
					if ((JSON.parse(data||"[]")).length>0)
						$($("#customAlerts div")[0]).html($($("#customAlerts div")[0]).html()+"<br>Buffer information");
					//WaitForBuffer()

				}
			});
		}

		function ajax3() {
			return $.ajax({
				url: url_ligand.replace("PROTEIN",pdb_id.slice(0, -1)).replace("CHAIN",pdb_id.slice(-1)),
				type : 'GET',
				success : function(data) {
					if ((JSON.parse(data||"[]")).length>0)
						$($("#customAlerts div")[0]).html($($("#customAlerts div")[0]).html()+"<br>Ligand information");

				}
			});
		}
	}	
	function ajax2() {
		return $.ajax({
			url: url_main,
			data: {'pdbid': pdb_id},
			type: 'POST',
			success : function(data) {
				if ((JSON.parse(data||"[]")).length != 0)
					$($("#customAlerts div")[0]).html($($("#customAlerts div")[0]).html()+"<br>Sequence Variants information");
				//WaitForBuffer()

			}

		});
	}
	function ajax4() {
		return $.ajax({
			url: url_bs.replace("PROTEIN",pdb_id.slice(0, -1)).replace("CHAIN",pdb_id.slice(-1)),
			type : 'GET',
			success : function(data) {
				if ((JSON.parse(data||"[]")).length>0)
					$($("#customAlerts div")[0]).html($($("#customAlerts div")[0]).html()+"<br>Binding sites information");
				//WaitForBS()

			}
		});
	}
}

function UpdateStatus(){
	$($("#customAlerts div")[0]).html($($("#customAlerts div")[0]).html()+"<br><br><strong>Drawing the models..</strong>");
	//$("#customAlerts").html("<div class='alert alert-success' role='alert'><strong> </strong></div><img src='slike/loadclique.gif' alt='Lscrean' style='width:200px;height:200px;'></div>");
}

function FetchData(){
	var u_input = $('#main_input').val();
	data_recieved==true ? console.log("data came."): send_query(u_input);

	//console.log("tried: "+tries+" times, "+" data is recieved: "+data_recieved);    
	//rabi cca 1.6s da zracuna za velike proteine!

	if (data_recieved == false){
		if (tries < 12){
			if (tries == 1){
				setTimeout(FetchData, 2000);
			}else if (tries >1 && tries < 7){
				setTimeout(FetchData, 2000);
			}else{
				setTimeout(FetchData, 1000);
			}

		}
	}
}

function obtain_pdb(pdbName,pdbChain,callback){

	$.ajax({
		url: url_pdb,
		data: {'pdbNAME': pdbName, 'pdbCHAIN': pdbChain},
		success: function (data) {
			(function(cb=callback){
				//console.log(data);
				canvas.molmilViewer.loadStructureData(data,4,pdbName);
				//canvas.molmilViewer.loadStructureData(data,4,pdbName);
				callback(canvas.molmilViewer.structures[0]);
			})();

		},
		error: function (xhr, status, error) {

			throw new Error ('PDB non existing..');

		}
	})
}

function hide_loading_screen(){
	$('#customAlerts').modal('hide'); // hide loading dialog.
}

function check_INPUT(directTrigger){   

	//to je zdaj narjen samo za iskat po PDB bazi, v prihodnosti bo tukej neka funkcija
	// npr. convertinputtoPDB(), ki bo vzela lah tut ime gena ali pa Uniprot Acc, pa ga konvertirala v PDB - to bo posebej baza s temi vnosi - zaenkrat je overkill.

	$('#main_input').val(($('#main_input').val()).replace(" ",""));
//	window.location.hash = "#"+$('#main_input').val();
	var uinput = $('#main_input').val();    

	data_recieved=false;
	//clientside check, v prihodnosti bo sam spoznal inpute!

	//tukej dodamo za rs input! Tukej pride se ohoho kriterijev..
	// if (directTrigger == true){
	// 	$('#loadmodal').modal('show');  //display loading dialog.
	// 	$("#customAlerts").html("<div class='alert alert-success' role='alert'><strong>Calculating properties! </strong> Please wait a moment.</div><img src='slike/Lscreen.gif' alt='Lscrean' style='width:100px;height:70px;'>");
	// 	FetchData(); // tukej more dobit input v global var SNPS.
	// }

	//if (uinput.startsWith('rs') || uinput.startsWith('P') ||  /^[A-Z][A-Z][A-Z]/.test(uinput)  ){
	if (/^[A-Za-z]/.test(uinput)){
		//#Databaselist je modal, kjer se to isce!
		$('#DatabaseList').modal('show');
		$('#global_input').val(uinput);
		get_global_results(uinput);
	}else{
		//		if (uinput.length != 5){
		//console.log(uinput.length);
		//		alert("Please enter PDB chain along with accession number!");
		//		}else{
		$('#loadmodal').modal('show');  //display loading dialog.

		$("#customAlerts").html("<div class='alert alert-success' role='alert'><strong>Calculating properties!</strong><br><br><strong>Obtained:</strong></div><img src='slike/loadclique.gif' alt='Lscrean' style='width:200px;height:200px;'><div id='myProgress'><div id='myBar'></div></div>");
		move();
		FetchData(); // tukej more dobit input v global var SNPS.
		//		}
	}
}
/* Set the width of the side navigation to 250px */
function openNav() {
	if (window.mobilecheck()) {
		document.getElementById("mySidenavTable").style.width = "100%";
		document.getElementById("SNPsLegend").style.display = "none";
	}
	document.getElementById("mySidenav").style.width = "250px";
}

function openLegend() {
	document.getElementById("LegendInfo").style.height = "120px";
}

function closeLegend() {
	document.getElementById("LegendInfo").style.height = "0";
}

/* Set the width of the side navigation to 0 */
function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
}

function openMainNav() {
	if (window.mobilecheck()) {
		document.getElementById("StructureContainer").style.width = "100%";
		document.getElementById("StructureContainer").style.height = "20%";
		document.getElementById("StructureContainer").style.overflow = "hidden";
		document.getElementById("menu_main").style["padding-top"] = "40%";
		document.getElementById("searchHistory").style.display = "none";
	}
	else document.getElementById("StructureContainer").style.width = "250px";
}

function closeMainNav() {
	document.getElementById("StructureContainer").style.width = "0px";
}

//those functions toggle menu opening!
function openNavTable() {
	//needs to check here if table exists, else throws some sort of warning.
	var width = ($( window ).width())/2.5;
	document.getElementById("mySidenavTable").style.width = "40%";
}

/* Set the width of the side navigation to 0 */
function closeNavTable() {
	if (window.mobilecheck()) {
		change_events_main();
		openMainNav();
	}
	else {
		restartDsearch("searchSNP", "genTab");
	}
	document.getElementById("mySidenavTable").style.width = "0";
	//tukej bo iz jsona dodal tudi html, bo zgenerirana koda..
}

function openNavTableBS() {
	//needs to check here if table exists, else throws some sort of warning.
	var width = ($( window ).width())/2.5;
	document.getElementById("mySidenavTableBS").style.width = "40%";
}

/* Set the width of the side navigation to 0 */
function closeNavTableBS() {
	document.getElementById("mySidenavTableBS").style.width = "0";
	var bsCurrent = $('.BS-tab .active > a').attr('href');
	if (bsCurrent != undefined) {
		bsCurrent = $(bsCurrent+' .active > a').attr('href');
		bsCurrent = "BSSNPTab"+bsCurrent.substring(1);
		restartDsearch("searchBS", bsCurrent);
	}
	//tukej bo iz jsona dodal tudi html, bo zgenerirana koda..
}

function openNavTableLig() {
	var width = ($( window ).width())/2.5;
	document.getElementById("mySidenavTableLig").style.width = "40%";
}

function closeNavTableLig() {
	document.getElementById("mySidenavTableLig").style.width = "0";
	var bsLigands = $('.lig-tab .active > a').attr('href');
	if (bsLigands != undefined) {
		bsLigands = bsLigands.substring(1);
		bsLigands = $('#'+bsLigands+' .active > a').attr('href');
		bsLigands = "LigSNPTab"+bsLigands.replace("#ligands ","");
		restartDsearch("searchLig", bsLigands);
	}
}

function closeDraggableTable() {
	$('#draggableTable').toggle();
}

function openLigandPlayer() {

	if ($('#draggableLigandPlayer').css("display") == "none") {
		var bsLigands = $('.lig-tab .active > a').attr('href');
		bsLigands = bsLigands.substring(1);
		bsLigands = $('#'+bsLigands+' .active > a').attr('href');
		bsLigands = bsLigands.replace("#ligands ","");

		var ligLen = 0;

		for (var i=0; i<ligandObject.length; i++) {
			if ("Binding Site #"+(ligandObject[i]).bs_id+"_"+(ligandObject[i]).ltype == bsLigands)
				ligLen += 1;
		}

		for (var i=0; i<ligandObject.length; i++) {
			if ("Binding Site #"+(ligandObject[i]).bs_id+"_"+(ligandObject[i]).ltype == bsLigands && !ligandObject[i]["visible"]) {
				showLigand(i,"ligand","(1/"+ligLen+")");
				break;
			}
		}		
	}
	else {
		hideLigand("ligand");
	}

	$("#ligandPlayer_PLAY").removeClass("active");
	ligandPlayerPlaying = false;

	canvas.renderer.initBuffers(surface_type); 	
	canvas.update = true;

	$('#draggableLigandPlayer').toggle();
}

function closeLigandPlayer() {
	$('#draggableLigandPlayer').css("display","none");
	hideLigand("ligand");
	ligandPlayerPlaying = false;
	$("#ligandPlayer_PLAY").removeClass("active");
	canvas.renderer.initBuffers(surface_type); 	
	canvas.update = true;
}

//function send_uni_directly(uniID){


//var PDBfromUni = uniID.value;
//if (PDBfromUni.length <5){
//var chain = prompt("Please enter the chain for this PDB!");
//var PDBwChain = PDBfromUni+chain;	
//$('#DatabaseList').modal('hide');
//$("#main_input").val(PDBwChain);
//}else{
//console.log("weird entry.");
//}


//}

//function Get_uniprot_results(){

//tires=0;
//data_recieved=false; 
//$("#Uniprot_results").html("<div class='alert alert-success' role='alert'><strong>Fetching and preprocessing your entry! </strong> Please wait a moment.</div><img src='slike/Lscreen.gif' alt='Lscrean' style='width:100px;height:70px;'>");
//var queryString = $("#Uniprot_input").val();
//url_script = "http://localhost:3000/get_uni";    
//$.ajax({
//url: url_script,
//data: {'UniprotID': queryString},
//type: 'POST',
//success: function (data) {
//console.log("data came!");
//if (data.length == 0 || data == ""){
//data = "Sorry, no results found for this entry! \n your accession might not be in UniProt format or non-existing currently!.";
//}

//var htmlUniTable = "";	    
//var UniIDs = String(data).split(";");

//UniIDs.forEach(function(ent){

//htmlUniTable += "<input value='"+ent+"' type='button' class='btn btn-success' onClick='send_uni_directly(this);'></input>";
//});
////tukej skonstruira tabelo hitov!
////za vsak entry naj nardi gumbek al neki, sicer bo velik gumbkov ampak bo kul.
//$("#Uniprot_results").html("<center><label for='comment'>Associated PDBs:</label></center>"+htmlUniTable);
//},
////<textarea class='form-control' rows='5' id='comment'>"+data+"</textarea>"
//error: function (xhr, status, error) {
//$("#Uniprot_results").html("<div class='alert alert-danger' role='alert'><strong>Ooooops.! </strong>Couldn't process your input. please check if inputted accession really exists. (If nothing else works, server is too busy - please try again later!)</div><img src='slike/Lscreen.gif' alt='Lscrean' style='width:100px;height:70px;'>");
//console.log("Mapping not possible..");

//},
//});

//}

function send_to_input(geneID){

	$('#DatabaseList').modal('hide');
	$("#main_input").val(geneID.value.split("(")[0]);
	setTimeout("$('#main_input').focus()", 400);
	setTimeout(check_INPUT, 400);
}

function get_global_results(value){

	tires2 = 0;
	data_recieved=false;

	$("#global_results").html("<div class='alert alert-success' role='alert'><strong>Fetching and preprocessing your entry! </strong> Please wait a moment.</div><img src='slike/loadclique.gif' alt='Lscrean' style='width:200px;height:200px;'><div id='myProgress'><div id='myBar'></div></div>");	var queryString = value //$("#global_input").val();
	//url_script = "http://localhost:3000/get_global";
	move();

	$.ajax({
		url: url_script,
		data: {'globalID': queryString},
		type: 'POST',
		success: function (data) {
			console.log("data came!");
			if (data.length == 0 || data == ""){
				data = "Sorry, no results found for this entry.";
				$("#global_results").html("<div class='alert alert-danger' role='alert'><strong>Ooooops.! </strong>Couldn't process your input.\n please check if inputted accession really exists. (If nothing else works, server is too busy - please try again later!)</div><img src='slike/loadclique.gif' alt='Lscrean' style='width:200px;height:200px;'>");
			}else{
				var htmlGeneTable = "";
				//console.log(data);
				var tmpButton = "";
				//console.log(data);
				for (var sub = 0;sub < data.length;sub++){
					console.log(data[sub]);
					tmpButton = data[sub][0]+data[sub][1]+"("+data[sub][2].split("_")[0]+" "+data[sub][2].split("_")[1]+")";  //.join("");
					htmlGeneTable += "<input value='"+tmpButton+"' type='button' class='btn btn-success' onClick='send_to_input(this);'></input>";
				}	    

				$("#global_results").html("<center><label for='comment'>Associated PDBs:</label></center>"+htmlGeneTable);
			}
		},
		//<textarea class='form-control' rows='5' id='comment'>"+data+"</textarea>"
		error: function (xhr, status, error) {
			$("#global_results").html("<div class='alert alert-danger' role='alert'><strong>Ooooops.! </strong>Couldn't process your input.\n please check if inputted accession really exists. (If nothing else works, server is too busy - please try again later!)</div><img src='slike/loadclique.gif' alt='Lscrean' style='width:200px;height:200px;'>");
			console.log("Mapping not possible..");

		},
	});

}

function init_bindingSites(bindingSitesDataFull) {
	var outerTableTabs = "<ul class='tab-links'>";
	var outerTableTabsContent = "<div class='tab-content'>";
	var counter = 0;

	for (var i=0; i<molmil_snps["sort"]["bsType"].length; i++) {
		var key = molmil_snps["sort"]["bsType"][i];
		if (Object.keys(bindingSitesDataFull[key]).length>0) {
			var bgcolor = colorScheme["surfaceColors"][key];
			var rgbabgcolor = "rgba("+bgcolor[0]+","+bgcolor[1]+","+bgcolor[2]+","+bgcolor[3]/255+");";
			if (counter == 0) {
				outerTableTabs += "<li class='active' ><a href='#BS-tab-"+key+"'style='background: "+rgbabgcolor+"'>"+key[0].toUpperCase()+key.substring(1)+"</a></li>";
				outerTableTabsContent += "<div id='BS-tab-"+key+"' class='tab active'></div>";
				restartDsearch("searchBS", "BSSNPTab"+molmil_snps["sort"][key][0]+"_"+key);
				initDsearch("searchBS", "BSSNPTab"+molmil_snps["sort"][key][0]+"_"+key);
				counter++;
			}
			else {
				outerTableTabs += "<li><a href='#BS-tab-"+key+"'style='background: "+rgbabgcolor+"'>"+key[0].toUpperCase()+key.substring(1)+"</a></li>";
				outerTableTabsContent += "<div id='BS-tab-"+key+"' class ='tab'></div>";
			}
		}
	}

	outerTableTabs += "</ul>";
	outerTableTabsContent += "</div>";

	$("#BS-tab").html(outerTableTabs+outerTableTabsContent);

	//main loop to obtain data
	for (var typeOfSurface in bindingSitesDataFull) {

		if (Object.keys(bindingSitesDataFull[typeOfSurface]).length>0) {
			var tableTabs = "<ul class='tab-links'>";
			var tableTabsContent = "";

			var bindingSitesData = bindingSitesDataFull[typeOfSurface];

			var bgcolor = colorScheme["surfaceColors"][typeOfSurface];
			var rgbabgcolor = "rgba("+bgcolor[0]+","+bgcolor[1]+","+bgcolor[2]+","+bgcolor[3]/255+");";

			for (var i=0; i<Object.keys(bindingSitesData).length; i++) {
				var key = molmil_snps["sort"][typeOfSurface][i];
				key = key;
				var templist = [];
				var finallist = [];
				var bindingSiteCols = bindingSitesData[key];

				//initiate tabs
				if (i == 0) tableTabs += "<li class='active' ><a href='#"+key+"_"+typeOfSurface+"' style='background: "+rgbabgcolor+"'>"+key+"</a></li>";
				else tableTabs += "<li><a href='#"+key+"_"+typeOfSurface+"' style='background: "+rgbabgcolor+"'>"+key+"</a></li>";

				for (var el=0;el<bindingSiteCols.length;el++){
					// 	    //razbije na 6 delni array
					var resultArray = bindingSiteCols[el]["data"];
					var SNP = bindingSiteCols[el]["SNP"];
					var accNo = "";
					if (SNP) {
						//				templist.push(String(resultArray[0])+"<input type='button' id = 'SNPbuttonBS_SNP_"+resultArray[1]+"' style=' margin: 5px; background: rgb("+colorScheme["SNP"][0]+","+colorScheme["SNP"][1]+","+colorScheme["SNP"][2]+");' name='button SNPButton' class='button SNPButton' value='SNP' />");
						//				for (var j=1; j<resultArray.length; j++) {
						//				if (j==(resultArray.length)) accNo += String(resultArray[j]);
						//				//else if (j%5 != 0) accNo += String(resultArray[j])+", ";
						//				else accNo += "<a href = 'http://www.ncbi.nlm.nih.gov/projects/SNP/snp_ref.cgi?rs="+String(resultArray[j])+"'  target='_blank'>"+String(resultArray[j])+",</a><br>";
						//				}
					}
					else templist.push(String(resultArray[0]));
					templist.push(accNo);
					if (!SNP)
						finallist.push(templist);
					templist=[];
				}

				if (i == 0) tableTabsContent += "<div id='"+key+"_"+typeOfSurface+"' class='tab active'>";
				else tableTabsContent += "<div id='"+key+"_"+typeOfSurface+"' class='tab'>";

				var resultTable =
					"<table id='BSTab' class='resultsTableBS'>"+
					"<thead style='center'><tr>"
					+"<th>Other amino acids in this surface region</th>"
					//+"<th>Accession number(s)</th>"
					+"</tr></thead><tbody>";

				for (var h = 0; h<finallist.length;h++){
					resultTable += "<tr>";
					for (var p = 0;p<2;p++){
						resultTable += "<td>"+finallist[h][p]+"</td>";			 		     		     
					}
					resultTable += "</tr>";
				}

				resultTable += "</tbody></table>";

				resultTable = "<table id='BSSNPTab"+key+"_"+typeOfSurface+"' class='resultsTable'></table>"+resultTable;

				tableTabsContent += resultTable;
				tableTabsContent += "</div>";
			}

			tableTabs += "</ul><div class='tab-content'>";
			tableTabs += tableTabsContent;
			tableTabs += "</div>";

			$("#BS-tab-"+typeOfSurface).html(tableTabs);

			for (var key in bindingSitesData) {
				$("#genTab .SNPBStableRowHeader").clone().appendTo("[id='BSSNPTab"+key+"_"+typeOfSurface+"']");
				$("[id='BSSNPTab"+key+"_"+typeOfSurface+"']").append("<tbody>");
				$("#genTab .SNPBStableRow"+key.replace(/ /g,"_").replace("#","")+"_"+typeOfSurface).clone().appendTo("[id='BSSNPTab"+key+"_"+typeOfSurface+"']");
				$("[id='BSSNPTab"+key+"_"+typeOfSurface+"']").append("</tbody>");

				$("[id='BSSNPTab"+key+"_"+typeOfSurface+"']").find('[id]').each(function() { 
					//Perform the same replace as above
					var $th = $(this);
					if ($th.attr('id') == "selectAll_SNP") var newID = "selectAll_"+key+"_"+typeOfSurface;
					else var newID = $th.attr('id').replace("SNP", "BS");

					$th.attr('id', newID);
				});
				$("[id='BSSNPTab"+key+"_"+typeOfSurface+"'] tr").find('td:eq(2),th:eq(2)').remove();

				if (document.getElementById("BSSNPTab"+key+"_"+typeOfSurface).rows.length == 1) {
					(document.getElementById("BSSNPTab"+key+"_"+typeOfSurface).rows[0]).remove();
				}
			}

		}
	}

};

//to je skopirano iz ProBiS-a, ne vem ce dela, je treba mal spremenit kodo.
function init_SNP(SNPdata, pdbid, mobile=false){
	
	var gridArrayData = [];
	var rcounter = 0;
	var aaresults = []; // array for amino acid visualization
	//console.log(SNPobject.snpdata)
	//snpdata sestoji iz> p.Leu5Phe_rs778853131_2ojx_E_5_homo_sapiens_variation28

	// $("#list1").jqGrid({
	// 	datatype: "local", 
	// 	colNames:['SNPview', 'Amino acid change', 'RS number', 'Locus', 'Species'], 
	// 	colModel: [
	// 	    {label: 'SNPview',name: 'SNPview',width: 60, align: 'center', sortable:false},
	// 	    {label: 'AminoChange',name: 'AminoChange',width: 60, align: 'center',sortable:false},
	// 	    {label: 'RS_number', name: 'RS_number',width: 60, align: 'center',sortable:false},
	// 	    //{label: 'PDB',name: 'PDB',width: 35,align: 'center',sortable:false},
	// 	    //{label: 'Chain',name: 'Chain',width: 35,align: 'center',sortable:false},
	// 	    {label: 'Locus',name: 'Locus',width: 60,align: 'center',sortable:true},
	// 	    {label: 'Species',name: 'Species',width: 100,align: 'center',sortable:false}
	// 	],
	// 	viewrecords: true, 	
	// 	width: '600px',	
	// 	height: '350px',
	// 	rowNum: 150,
	// 	pager: "#pager1",
	// 	pginput: false,
	// 	pgbuttons: false,
	// 	altRows: true,
	// 	altclass: 'even',
	// 	multiselect: false,
	// 	caption: "<a id='download_csv_tableSNP' title='Download Table as CSV File' href='#'><img style='border:none' src='slike/fileicons/csv.png'></img> Download Table</a>",
	// 	viewsortcols: [true,'vertical',true],
	// });

	//  $("#list1")[0].grid.beginReq();

	// //first get the data and then parse it

	if (SNPdata.length == 0){
		var resultTable = ""
	}

	var entrycounter = 0; //individual entries
	var resultArray = new Array();
	var rcounter = 0;
	var templist = [];
	var finallist = [];

    //main loop to obtain data

	for (var i=0; i<SNPdata.length; i++) {
		var SNPcols = SNPdata[i]["data"];
		var bindingSite = SNPdata[i]["bindingSite"]

		for (var el=0;el<SNPcols.length;el++){
			// 	    //razbije na 6 delni array
		    if (!mobile){
			    var resultArray = molmil_snps["snps"][SNPcols[el]];
			//console.log(resultArray)
		    }else {
				var resultArray = SNPcols[el];
//				var resultArray = {};
//				var splitElement = SNPcols[el].split("_");
//				resultArray["species"] = splitElement[7]+" "+splitElement[8];
//				resultArray["id"] = splitElement[3];
//				resultArray["change"] = splitElement[2].split(".")[1];
//				resultArray["locus"] = (resultArray["change"]).replace(/[^0-9]/g, '');			
			}
			
		    rcounter++;
		    //		    try{			
		    
		    var species = resultArray["species"];
//		    }catch(err){
			//console.log(resultArray['species'])
		    //}
		   // console.log(resultArray)

			// 		// This selection loop enables specific link creation.
			if (resultArray["id"].substring(0,2)=="rs"){
				var mutationlink = "<a class ='SNPlink' href='http://www.ncbi.nlm.nih.gov/projects/SNP/snp_ref.cgi?rs="+resultArray["id"]+"'  target='_blank'>"+resultArray["id"];

				var questionmark = "";//"<sub class='questionmark'> link to database</sub></a>";
				//var questionmark = "<img id='quickSNPintel' src='slike/qmark.png' class='question_mark' title='View SNP NCBI query'></img></a>";
			}else if (resultArray["id"].substring(0,4)=="COSM"){
				var mutationlink = "<a class ='SNPlink' href='http://cancer.sanger.ac.uk/cosmic/mutation/overview?id="+
				resultArray["id"].substring(4,resultArray["id"].length)+"'target='_blank'>"+resultArray["id"];
				var questionmark = ""//"<sub class='questionmark'> link to database</sub></a>";
					//var questionmark = "<img id='quickSNPintel' src='slike/qmark.png' class='question_mark' title='View SNP NCBI query'></img></a>";
			}else // if (resultArray[1].substring(0,3) == "vcZ" || resultArray[1].substring(0,4) == "ENSV" || resultArray[1].substring(0,3) == "PZE" || resultArray[1].substring(0,3) == "TBG")
			{
				if (resultArray["species"].split(" ")[0] != "aedes" && resultArray["species"].split(" ")[0] != "anopheles" && resultArray["species"].split(" ")[0] != "ixodes"){
					var mutationlink = resultArray["id"];

					var mutationlink = "<a class ='SNPlink' href='http://plants.ensembl.org/Multi/Search/Results?species=all;idx=;q="+resultArray["id"]+";site=ensemblunit' target='_blank'>"+mutationlink+"</a>";

					var questionmark = "";

				}else{


					var mutationlink = "<a class ='SNPlink' href='http://metazoa.ensembl.org/Multi/Search/Results?species=all;idx=;q="+resultArray["id"]+";site=ensemblunit' target='_blank'>"+mutationlink+"</a>";
				}
			}
			// else{

			// 	var mutationlink = "<a class ='SNPlink' href='ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/variants/README' target='_blank'>"+resultArray[1]+"</a>";

			// }
			//construct image link

			if (resultArray["change"].substring(0,3).toLowerCase() == "ter"){

				ilink1 = "<div  id ='fAA"+rcounter+"' style='display:none;'><p><b>termination</b></p></div>";
			}
			else if(resultArray["change"].substring(0,3).toLowerCase() == "unk"){

				ilink1 = "<div  id ='fAA"+rcounter+"' style='display:none;'><p><b>Unknown</b></p></div>";

			}else{
				ilink1 = "<div id ='fAA"+rcounter+"' style='display:none;'><img class='SNPamino' height='60' width='80' src = 'slike/"+resultArray["change"].substring(0,3).toLowerCase()+
				".gif'></img></div>";
			}
			if (resultArray["change"].substring(resultArray["change"].length-3).toLowerCase()=="ter"){

				ilink2 = "<div  id ='sAA"+rcounter+"' style='display:none;'><p><b>termination (X)</b></p></div>";	
			}

			else if(resultArray["change"].substring(resultArray["change"].length-3).toLowerCase() == "unk"){

				ilink2 = "<div  id ='fAA"+rcounter+"' style='display:none;'><p><b>Unknown</b></p></div>";

			}
			else{
				ilink2 = "<div  id ='sAA"+rcounter+"' style='display:none;'><img class='SNPamino' height='60' width='80' src = 'slike/"+
				resultArray["change"].substring(resultArray["change"].length-3).toLowerCase()+
				".gif'></img></div>";			    
			}

			//Specify the chain

			if (!mobile) {
				//resultArray["button_id"] = "SNPbuttonmain_"+resultArray[4]+"_"+resultArray[3]+"_"+resultArray[1]+"_"+resultArray[0];
				resultArray["button_id"] = "SNPbuttonmain_"+SNPcols[el];
				//Ce bomo hotl, da lahko izberes samo en snp na lokusu
				templistbuttons = "";
				if (el>0) {
					templistbuttons = "<input type='button' name='testbuttonSNP' class='button resultButton "+SNPcols[el]+"' value='S' onclick=' return toggleSNPselection(this);' />";						}
				else {
					if (bindingSite.length>0) {
						var classes = "";
						for (var b=0; b<bindingSite.length; b++) {
							classes += " resultButton_"+bindingSite[b].replace(/ /g,"_").replace("#","");
						}
						templistbuttons = "<input type='button' name='testbuttonSNP' class='button resultButton active "+SNPcols[el]+classes+"' value='D' onclick=' return toggleSNPselection(this);' />";
					}
					else {
						templistbuttons = "<input type='button' name='testbuttonSNP' class='button resultButton active "+SNPcols[el]+"' value='D' onclick=' return toggleSNPselection(this);' />";
					}
					resultArray["button_id"] = resultArray["button_id"]+"_first";
					resultArray["visible"] = true;
					molmil_snps["locus"][resultArray["locus"]]["selected"] = SNPcols[el];
					molmil_snps["locus"][resultArray["locus"]]["visible"] = true;
				}

				if (resultArray["label_no"] != null)
					templistbuttons += "<br>"+"<input type='button' name='testbuttonSNP' class='button resultButtonLabel "+SNPcols[el]+"' value='L' onclick=' return toggleSNPselection(this);' />";
				else {
					console.log("no label!");
				}
				templist.push(templistbuttons+"<br>"+"<input type='button' name='testbuttonSNP' class='button resultButtonZoom "+SNPcols[el]+"' value='Z' onclick=' return toggleSNPselection(this);' />");
			}			

			//     		templist.push("<input type='button' id='SNPbuttonmain_"+resultArray[4]+"_"+resultArray[3]+"_"+resultArray[1]+"_"+resultArray[0]+"' name='testbuttonSNP' class='button resultButton' value='Deselect SNP' onclick=' return toggleSNPselection(this);' />"+"<br><br>");

			templist.push(ilink1+ilink2+"<a id ='firstAA"+rcounter+"' style='padding: 0 0 0 0;'>"+resultArray["change"].substring(0,3)+"</a>"+(resultArray["locus"])+"<a id ='secondAA"+rcounter+"' style='padding: 0 0 0 0;'>"+resultArray["change"].substring(resultArray["change"].length-3)+"</a>")

			if (!mobile) {
				var templistbs = [""];

				if (bindingSite.length>0) {
					bindingSite = bindingSite.sort(sorting);
					templistbs = "";
					for (var bs=0; bs<bindingSite.length; bs++) {
						templistbs += " <input type='button' style='background: rgba("+colorScheme["surfaceColors"][(bindingSite[bs].split("#")[1]).split("_")[1]][0]+","+colorScheme["surfaceColors"][(bindingSite[bs].split("#")[1]).split("_")[1]][1]+","+colorScheme["surfaceColors"][(bindingSite[bs].split("#")[1]).split("_")[1]][2]+","+eval(colorScheme["surfaceColors"][(bindingSite[bs].split("#")[1]).split("_")[1]][3]/255)+");' name='button BindingSiteButton' class='button BindingSiteButton' value='"+(bindingSite[bs].split("#")[1]).split("_")[0]+"' />";
					}
					templistbs = [templistbs,bindingSite];
				}

				templistbs.push(SNPcols[el]);
				templist.push(templistbs);
			}
			else {
				var templistbs = [""];
				if (bindingSite) {
					bindingSite = bindingSite.sort(sorting);
					templistbs = "";
					for (var bs=0; bs<bindingSite.length; bs++) {
						templistbs += " <input type='button' style='background: rgba("+colorScheme["surfaceColors"][bindingSite[bs].split("_")[0]][0]+","+colorScheme["surfaceColors"][bindingSite[bs].split("_")[0]][1]+","+colorScheme["surfaceColors"][bindingSite[bs].split("_")[0]][2]+","+eval(colorScheme["surfaceColors"][bindingSite[bs].split("_")[0]][3]/255)+");' name='button BindingSiteButton' class='button BindingSiteButton' value='"+bindingSite[bs].split("_")[1]+"' />";
					}
					templist.push(templistbs);
				}
				else templist.push("");
			}

			templist.push(mutationlink+questionmark);

			templist.push("<a id ='Specieslink' href='http://www.ncbi.nlm.nih.gov/taxonomy/?term="+species+"' target='_blank'>"+"<i>"+species+"<\i></a>");

			finallist.push(templist);
			templist=[];


			aaresults.push(resultArray["change"].substring(0,3));aaresults.push(resultArray["change"].substring(resultArray["change"].length-3));aaresults.push(rcounter);
			resultArray = [];

		    }
		
		
	}

	$("#speciesSNP").html("<a id ='Specieslink' href='http://www.ncbi.nlm.nih.gov/taxonomy/?term="+species+"' target='_blank'>"+"<i>"+species+"<\i></a>");
	//var SNPcols = SNPobject.snpdata; // tukej je array _ separeted vrednosti
	// 	//Additional row trimming needed, when new data arrives..


	
	// 	

	//datatable creation!

	var colNo = 4;

	if(rcounter != 0)
		var resultTable = "<table id='genTab' class='resultsTable'><thead style='center' class = 'SNPBStableRowHeader'><tr>";
	else
		var resultTable = "";

	if (!mobile) {
		
		var snploci = molmil_snps["missing"]||[];

		if(rcounter != 0){
			resultTable += 
				"<th><input type='button' id='selectAll_SNP' name='selectbuttonSNP' class='button selectAllButton active' value='D' onclick=' return toggleSNPselection(this);' /></th>"
				+"<th>Amino acid change</th>"
				+"<th> </th>"
				+"<th>Accession number </th>"
				//+"<th>Residue number</th>"
				+"</tr></thead><tbody>";

			for (var h = 0; h<finallist.length;h++){
				if (finallist[h][2][0] == "")
					resultTable += "<tr class = 'SNPtableRow' id='SNPtable_row_"+finallist[h][2][1]+"'>";
				else {
					var classes = "";
					for (var b=0; b<finallist[h][2][1].length; b++) {
						classes += " SNPBStableRow"+finallist[h][2][1][b].replace(/ /g,"_").replace("#","");
					}
					resultTable += "<tr class = 'SNPtableRow"+classes+"' id='SNPtable_row_"+finallist[h][2][2]+"'>";
				}
				finallist[h][2] = finallist[h][2][0];
				for (var p = 0;p<4;p++){
					resultTable += "<td>"+finallist[h][p]+"</td>";			 		     		     
				}
				resultTable += "</tr>";
			} }

		if (Object.keys(snploci).length > 0) {
			var gridTable =
				"<table class='resultsTable'>"+
				"<thead><tr>"
				+"<th colspan = '4'>No structural data available for the following Sequence Variants:</th></tr>"
				+"<tr><th></th>"
				+"<th>Amino acid change	</th>"
				+"<th></th>"
				+"<th>Accession number </th>"
				//+"<th>Accession number(s)</th>"
				+"</tr></thead><tbody>";

			for (var key in snploci) {
				for (var i=0; i<snploci[key].length; i++) {
					
					//NEW CODE
					var snplociKey = snploci[key][i][1];
					gridTable += "<tr>";
					gridTable += "<td></td>";
					gridTable += "<td>"+snplociKey["change"]+"</td>";
					gridTable += "<td></td>";
					if (snplociKey["id"].substring(0,2)=="rs"){
						var mutationlink = "<a id ='SNPlink' href='http://www.ncbi.nlm.nih.gov/projects/SNP/snp_ref.cgi?rs="+snplociKey["id"]+"'  target='_blank'>"+snplociKey["id"]+"</a>";
					}
					else if (snplociKey["id"].substring(0,4)=="COSM"){
						var mutationlink = "<a id ='SNPlink' href='http://cancer.sanger.ac.uk/cosmic/mutation/overview?id="+
						snplociKey["id"].substring(4,snplociKey["id"].length)+"'target='_blank'>"+snplociKey["id"]+"</a>";
					}
					else {
						if (snplociKey["species"].split(" ")[0] != "aedes" && snplociKey["species"].split(" ")[0] != "anopheles" && snplociKey["species"].split(" ")[0] != "ixodes"){
							var mutationlink = snplociKey["id"];

							var mutationlink = "<a class ='SNPlink' href='http://plants.ensembl.org/Multi/Search/Results?species=all;idx=;q="+snplociKey["id"]+";site=ensemblunit' target='_blank'>"+mutationlink+"</a>";

							var questionmark = "";

						}else{


							var mutationlink = "<a class ='SNPlink' href='http://metazoa.ensembl.org/Multi/Search/Results?species=all;idx=;q="+snplociKey["id"]+";site=ensemblunit' target='_blank'>"+mutationlink+"</a>";
						}
					}
					gridTable += "<td>"+mutationlink+"</td>";
					gridTable += "</tr>";
					
					//OLD CODE
//					var snplociKey = snploci[key][i][1].split("_");
//					gridTable += "<tr>";
//					gridTable += "<td></td>";
//					gridTable += "<td>"+snplociKey[2].split(".")[1]+"</td>";
//					gridTable += "<td></td>";
//					if (snplociKey[3].substring(0,2)=="rs"){
//						var mutationlink = "<a id ='SNPlink' href='http://www.ncbi.nlm.nih.gov/projects/SNP/snp_ref.cgi?rs="+snplociKey[3]+"'  target='_blank'>"+snplociKey[3]+"</a>";
//					}
//					else if (snplociKey[3].substring(0,4)=="COSM"){
//						var mutationlink = "<a id ='SNPlink' href='http://cancer.sanger.ac.uk/cosmic/mutation/overview?id="+
//						snplociKey[3].substring(4,snplociKey[3].length)+"'target='_blank'>"+snplociKey[3]+"</a>";
//					}
//					else {
//						if (snplociKey[7] != "aedes" && snplociKey[7] != "anopheles" && snplociKey[7] != "ixodes"){
//							var mutationlink = snplociKey[3];
//
//							var mutationlink = "<a class ='SNPlink' href='http://plants.ensembl.org/Multi/Search/Results?species=all;idx=;q="+snplociKey[3]+";site=ensemblunit' target='_blank'>"+mutationlink+"</a>";
//
//							var questionmark = "";
//
//						}else{
//
//
//							var mutationlink = "<a class ='SNPlink' href='http://metazoa.ensembl.org/Multi/Search/Results?species=all;idx=;q="+snplociKey[3]+";site=ensemblunit' target='_blank'>"+mutationlink+"</a>";
//						}
//					}
//					gridTable += "<td>"+mutationlink+"</td>";
//					gridTable += "</tr>";
					
					//OLD CODE END
				}
			}

			gridTable += "</tbody></table>";

			resultTable += gridTable;

		}

		else if (resultTable.length == 0)
			resultTable = "No Sequence Variants were found";

	}

	else {
		if(rcounter == 0){
			resultTable = "No Sequence Variants were found";
		}
		else {
			resultTable += 
				"<th>Amino acid change</th>"
				+"<th> </th>"
				+"<th>Accession number </th>"
				//+"<th>Residue number</th>"
				+"</tr></thead><tbody>";

			for (var h = 0; h<finallist.length;h++){
				resultTable += "<tr>";
				for (var p = 0;p<3;p++){
					resultTable += "<td>"+finallist[h][p]+"</td>";			 		     		     
				}
				resultTable += "</tr>";
			}
		}
	}

	resultTable += "</tbody></table>";

	//console.log(aaresults);


	$("#SNP-tab").html(resultTable);
	aminoview(aaresults);

};

function init_SNP_mobile(SNPdata, pdbid){
	var SNPcols_dict = {};
	var SNPcols = [];
	var snpsurf = {};
	var snpsurfnam = [];
	var seqToRSID = {};
	//var abs_locations = SNPdata.seq_info.validated_locations;

	for (var i=0; i<SNPdata.snpdata.length; i++) {
		var snp = SNPdata.snpdata[i];
		seqToRSID[snp["uni"]] = snp["pdb"];
		var rsid = snp["uni"];
		if (snp["snp"] != undefined) {
			snp["snp"]["locus"] = snp["uni"];
			snp["snp"]["change"] = (snp["snp"]["change"]).replace("p.","");
		if (SNPcols_dict[rsid]) 
			SNPcols_dict[rsid].push(snp["snp"]);
		else SNPcols_dict[rsid] = [snp["snp"]];
		}
	}

	for (var i=0; i<SNPdata.newSurface.length; i++) {
		var aa = (SNPdata.newSurface)[i];
		try {
			snpsurf[aa.resi].push(aa.ltype+"_"+aa.bs_id)
		}
		catch(e) {
			snpsurf[aa.resi] = [aa.ltype+"_"+aa.bs_id];
		}
		if (snpsurfnam.indexOf(aa.ltype+"_"+aa.bs_id) == -1)
			snpsurfnam.push(aa.ltype+"_"+aa.bs_id);
		//		var aa = SNPdata.surface[i].split("_");
		//		if (aa.length==1) {
		//		snpsurf[aa[0]] = ["Predicted"];
		//		snpsurfnam = ["Predicted"];
		//		}
		//		else {
		//		try {snpsurf[aa[2]].push(aa[0]+" "+aa[1]);}
		//		catch(e) {snpsurf[aa[2]]=[aa[0]+" "+aa[1]];}
		//		if (snpsurfnam.indexOf(aa[0]+" "+aa[1]) == -1) snpsurfnam.push(aa[0]+" "+aa[1]);
		//		}	
	}

	for (var key in SNPcols_dict) {
		if ((Object.keys(snpsurf)).indexOf(seqToRSID[String(key)]) != -1) {
			SNPcols.push({"bindingSite": snpsurf[seqToRSID[String(key)]], "data": SNPcols_dict[key]});
		}
		else SNPcols.push({"bindingSite": false, "data": SNPcols_dict[key]});
	}
	init_SNP(SNPcols, pdbid, mobile=true);

};

function init_ligands() {

	var outerTableTabs = "<ul class='tab-links'>";
	var outerTableTabsContent = "<div class='tab-content'>";
	var counter = 0;
	for (var indSurf=0; indSurf<molmil_snps["sort"]["bsType"].length; indSurf++) {
		var key = molmil_snps["sort"]["bsType"][indSurf];
		if (molmil_snps["sort"][key].length>0) {
			var bgcolor = colorScheme["surfaceColors"][key];
			var rgbabgcolor = "rgba("+bgcolor[0]+","+bgcolor[1]+","+bgcolor[2]+","+bgcolor[3]/255+");";
			if (counter == 0) {
				outerTableTabs += "<li class='active' ><a href='#lig-tab-"+key+"'style='background: "+rgbabgcolor+"'>"+key[0].toUpperCase()+key.substring(1)+"</a></li>";
				outerTableTabsContent += "<div id='lig-tab-"+key+"' class='tab active'></div>";
				restartDsearch("searchLig", "LigSNPTab"+molmil_snps["sort"][key][0]+"_"+key);
				initDsearch("searchLig", "LigSNPTab"+molmil_snps["sort"][key][0]+"_"+key);
				counter++;
			}
			else {
				outerTableTabs += "<li><a href='#lig-tab-"+key+"'style='background: "+rgbabgcolor+"'>"+key[0].toUpperCase()+key.substring(1)+"</a></li>";
				outerTableTabsContent += "<div id='lig-tab-"+key+"' class ='tab'></div>";
			}
		}
	}

	outerTableTabs += "</ul>";
	outerTableTabsContent += "</div>";

	$("#lig-tab").html(outerTableTabs+outerTableTabsContent);

	for (var indSurf=0; indSurf<molmil_snps["sort"]["bsType"].length; indSurf++) {
		var keySurf = molmil_snps["sort"]["bsType"][indSurf];
		if (molmil_snps["sort"][keySurf].length>0) {
			var tableTabs = "<ul class='tab-links'>";
			var tableTabsContent = "";

			//main loop to obtain data
			for (var i=0; i<molmil_snps["sort"][keySurf].length; i++) {
				var key = molmil_snps["sort"][keySurf][i];
				var templist = [];
				var finallist = [];
				//var bindingSiteCols = bindingSitesData[key];

				ligandsGrid = {};

				for (var el=0; el<ligandObject.length; el++) {
					if (ligandObject[el].bs_id == key.split("#")[1] && ligandObject[el].ltype == keySurf) {
						if (ligandObject[el].ltype == "protein")
							ligandObject[el].lig_id = (ligandObject[el].lig_id).replace("PPP",(ligandObject[el].lig_id).split("_")[2]+(ligandObject[el].lig_id).split("_")[3]);
						else if (ligandObject[el].ltype == "nucleic")
							ligandObject[el].lig_id = (ligandObject[el].lig_id).replace("NNN",(ligandObject[el].lig_id).split("_")[2]+(ligandObject[el].lig_id).split("_")[3]);
						var ligName = ligandObject[el].lig_id;
						ligandObject[el]["visible"] = false;
						//console.log(ligName);
						try {
							ligandsGrid[ligName.split("_")[4]].push(ligName);
						}
						catch(e) {
							ligandsGrid[ligName.split("_")[4]] = [ligName];
						}
					}
				}

				var ligandsGridKeys = Object.keys(ligandsGrid).sort(ligandSort);

				//initiate tabs
				var bgcolor = colorScheme["surfaceColors"][keySurf];
				var rgbabgcolor = "rgba("+bgcolor[0]+","+bgcolor[1]+","+bgcolor[2]+","+bgcolor[3]/255+");";
				if (i == 0) tableTabs += "<li class='active' ><a href='#ligands "+key+"_"+keySurf+"' style='background: "+rgbabgcolor+"'>"+key+"</a></li>";
				else tableTabs += "<li><a href='#ligands "+key+"_"+keySurf+"' style='background: "+rgbabgcolor+"'>"+key+"</a></li>";

				if (keySurf == "protein" || keySurf == "nucleic") {
					for (var k=0; k<ligandsGridKeys.length; k++) {
						var keyLig = ligandsGridKeys[k];
						templist.push("<img class='lig_link' src='http://www.rcsb.org/pdb/images/"+keyLig.toUpperCase().substring(0,keyLig.length-1)+"_bio_r_500.jpg?bioNum=1' alt='"+keyLig.toUpperCase()+"' />");
						templist.push("<a class='SNPlink' href='http://www.rcsb.org/pdb/explore/explore.do?structureId="+keyLig.toLowerCase().substring(0,keyLig.length-1)+"' target='_blank'>"+keyLig+"</a>");
						templist.push(ligandsGrid[keyLig].length);
						if (templist.length>0)
							finallist.push(templist);
						templist=[];
					}
				}
				else {
					for (var k=0; k<ligandsGridKeys.length; k++) {
						var keyLig = ligandsGridKeys[k];
						templist.push("<img class='lig_link' src='http://www.rcsb.org/pdb/images/"+keyLig.toUpperCase()+"_100.gif' alt='"+keyLig.toUpperCase()+"' />");
						templist.push("<a class='SNPlink' href='https://www3.rcsb.org/ligand/"+keyLig.toUpperCase()+"' target='_blank'>"+keyLig+"</a>");
						templist.push(ligandsGrid[keyLig].length);
						if (templist.length>0)
							finallist.push(templist);
						templist=[];
					}
				}

				//		for (var el=0; el<ligandObject.length; el++) {
				//			if ((ligandObject[el][0]).replace(/\$/g, ' ').replace("_","") == key) {
				//				templist.push(ligandObject[el][1]);
				//			}
				//			if (templist.length>0)
				//				finallist.push(templist);
				//			templist=[];
				//		}

				if (i == 0) tableTabsContent += "<div id='ligands "+key+"_"+keySurf+"' class='tab active'>";
				else tableTabsContent += "<div id='ligands "+key+"_"+keySurf+"' class='tab'>";

				var resultTable =
					"<thead style='center'><tr>"
					+"<th><input type='button' id='selectAll_ligands' name='selectbuttonSNP' class='button selectAllLigandsButton' value='S' onclick=' return toggleLigand(this);' /></th>"
					+"<th>Image</th>"
					+"<th>Ligand</th>"
					+"<th>Count</th>"
					//+"<th>Accession number(s)</th>"
					+"</tr></thead><tbody>";

				if (finallist.length>0) {
					for (var h = 0; h<finallist.length;h++){
						resultTable += "<tr class='ligand_row'>";
						resultTable += "<td><input type='button' name='testbuttonSNP' class='button resultButtonLigands' value='S' onclick=' return toggleLigand(this);' /></td>";
						for (var p = 0;p<3;p++){
							resultTable += "<td>"+finallist[h][p]+"</td>";			 		     		     
						}
						resultTable += "</tr>";
					}
					resultTable += "</tbody></table>";
				}

				else {
					resultTable = "No ligands available for this binding site."
				}		

				resultTable = "<table id='LigSNPTab"+key+"_"+keySurf+"' class='resultsTable'>"+resultTable;

				tableTabsContent += resultTable;
				tableTabsContent += "</div>";
			}

			tableTabs += "</ul><div class='tab-content'>";
			tableTabs += tableTabsContent;
			tableTabs += "</div>";

			$("#lig-tab-"+keySurf).html(tableTabs);
		}
	}

};

function load_PDB(data,pdbName) {
	molmil.load_PDB(data,pdbName)
}

//reload molmilViewer when new data is entered
function reloadViewer(uinput, SNPobject, closeLoading) {
	var canvas = document.getElementById("molmilViewer");

	uinput = uinput.substring(0,uinput.length-1)+uinput.substring(uinput.length-1).toUpperCase();

	//var abs_locations = (SNPobject.seq_info).validated_locations;
	var chainsListStr = (uinput.substring(uinput.length-1));
	var snploci = {};
	var snpsurf = {};
	var snpsurfnam = [];
	var SNPcols = [];
	var bindingSiteCols = {"ion": {}, "compound": {}, "nucleic": {}, "protein": {}, "noinfo": {}};
	snplociMolmil = {};
	var snplociMolmilInv = {};
	molmil_snps = {};
	seqToRSID = {};
	seqToUniaa = {};
	//console.log(SNPobject);
	//NEW CODE

	
	//NEW CODE
	//[{"uni":345, "pdb":"60A","resn":"VAL","missing":false,"deleted":false,
	//"snp":{"uniprotid":"P00734","gene":"F2","change":"His605Gln","id":"rs368442575","species":"homo sapiens","variation":"variation60"}},...]
	molmil_snps["sequence"] = {};
	molmil_snps["sort"] = {"bsType": ["compound", "ion", "noinfo", "nucleic", "protein"], "ion": [], "compound": [], "nucleic": [], "protein": [], "noinfo": []};
	
	var newLink = "https://ecrbrowser.dcode.org/xS.php?db=hg19&query=";
	for (var i=0; i<SNPobject.snpdata.length; i++) {
		var snp = SNPobject.snpdata[i];
		seqToRSID[snp["pdb"]] = snp["uni"];
		seqToUniaa[snp["pdb"]] = snp["resn"];
		molmil_snps["sequence"][snp["counter"]] = {"uni": snp["uni"], "resn": snp["resn"], "respdb": snp["respdb"], "missing": snp["missing"], "deleted": snp["deleted"], "pdb": snp["pdb"], "snp": !(snp["snp"]==undefined)};
		if (snp["snp"] != undefined) {
			newLink = "https://ecrbrowser.dcode.org/xS.php?db=hg19&query="+snp["snp"]["gene"];
			//var newLink = "https://ecrbrowser.dcode.org/xS.php?db=hg19&query="+SNPobject.snpdata[0].split("_")[1];
			snp["snp"]["change"] = snp["snp"]["change"].replace("p.","");
			snp["snp"]["respdb"] = snp["respdb"];
			try {
				snploci[snp["uni"]].push([i+1,snp["snp"]]);
			}
			catch(e) {
				snploci[snp["uni"]] = [[i+1,snp["snp"]]];
			}
		}
	}
	$("#geneViewLink").attr("href", newLink);

	molmil_snps["sort"]["sequence_sorted_keys"] = (Object.keys(molmil_snps["sequence"]).sort(sorting));
	
	//get surface data
	molmil_snps["sequence_surf"] = {};
	SNPobject.newSurface.forEach(function(el){
		try {
			molmil_snps["sequence_surf"][el.resi].push(el.ltype+" #"+el.bs_id);
		}
		catch(e){
			molmil_snps["sequence_surf"][el.resi] = [el.ltype+" #"+el.bs_id];
		}
	});


	

	//OLD CODE
	
//	//dictionary of SNPs by residue id key
//	for (var i=0; i<SNPobject.snpdata.length; i++) {
//		var name = SNPobject.snpdata[i].split("_");
////		if (name[2].indexOf("p.") == -1) {
////		console.log("does this ever happen?");
////		SNPobject.snpdata[i] = SNPobject.snpdata[i].slice(0,name[0].length+name[1].length+2)+SNPobject.snpdata[i].slice(name[0].length+name[1].length+2+name[2].length+1);
////		name.splice(2, 1);
////		}		
//
//		if (chainsListStr.includes(name[5])) {
//			try {snploci[name[2].replace(/[^0-9]/g, '')].push([i+1, SNPobject.snpdata[i]]);}
//			catch(e) {
//				snploci[name[2].replace(/[^0-9]/g, '')] = [[i+1, SNPobject.snpdata[i]]];
//			}
//		}
//	}
//
//	for (var i=0; i<Object.keys(snploci).length; i++) {
//		seqToRSID[abs_locations[i]] = Object.keys(snploci)[i];
//	}

	//OLD CODE END
	//    console.log(snplociMolmil);
	// vzame prvega od teh snplociMolmil, pogleda njegov rsid, od SNPlokacije odsteje trenutni PDBid in pristeje rsid, kjer je id=1. Potem odsteje od SNPloc to zacetno Uniloc, ki smo jo ravnokar izracunal. Narise pa snp tisti, kjer je ID enak temu, kar pade ven iz razlike. Zdej bo torej narisano po id-jih!    

	var checkByRSID = {};
	for (var i=0; i<SNPobject.newSurface.length; i++) {
		var aa = (SNPobject.newSurface)[i];
		var fixedName = ("Binding Site #"+aa.bs_id);

		try {snpsurf[aa.resi].push(fixedName+"_"+aa.ltype);}
		catch(e) {
			snpsurf[aa.resi]=[fixedName+"_"+aa.ltype];
		}
		if (snpsurfnam.indexOf(fixedName+"_"+aa.ltype) == -1) {
			snpsurfnam.push(fixedName+"_"+aa.ltype);
			checkByRSID[fixedName+"_"+aa.ltype] = [aa.resi];
		}
		else 
			if ((checkByRSID[fixedName+"_"+aa.ltype]).indexOf(aa.resi) == -1)
				checkByRSID[fixedName+"_"+aa.ltype].push(aa.resi);

		//		var aa = SNPobject.surface[i].split("_");
		//		var fixedName = (aa[0]+aa[1]).replace(/\$/g, ' ');

		//		if (aa.length==3) {
		//		try {snpsurf[aa[2]].push(fixedName+"_noinfo");}
		//		catch(e) {snpsurf[aa[2]]=[fixedName+"_noinfo"];}
		//		if (snpsurfnam.indexOf(fixedName+"_noinfo") == -1) snpsurfnam.push(fixedName+"_noinfo");
		//		}
		//		else if (aa.length==4) {
		//		try {snpsurf[aa[3]].push(fixedName+"_"+aa[2]);}
		//		catch(e) {
		//		snpsurf[aa[3]]=[fixedName+"_"+aa[2]];
		//		}
		//		if (snpsurfnam.indexOf(fixedName) == -1) snpsurfnam.push(fixedName+"_"+aa[2]);
		//		}
	}
	
	snpsurfnam = snpsurfnam.sort(sorting);
	
	checkByRSID = checkByRSID[snpsurfnam[0]]||[];

	for (var i=0; i<snpsurfnam.length; i++) {

		molmil_snps["sort"][snpsurfnam[i].split("_")[1]].push(snpsurfnam[i].split("_")[0]);

		colorScheme["surfaceColors"][snpsurfnam[i]] = colorScheme["surfaceColors"]["Predicted"];
		colorScheme["surfaceColors"][snpsurfnam[i]+"_SNP"] = colorScheme["surfaceColors"]["Predicted_SNP"];
	}

	if (snpsurfnam.length == 0) {
		surface_type = false;
		$("#legendChangeSticks").css("display","");
		$("#legendChangeBuffer").css("display","none");
	}
	else if (global_buffers.length>0) {
		//		console.log(global_buffers);
		//		for (var i=0; i<global_buffers.length; i++) {
		//		//global_buffers[i][0] = global_buffers[i][0].substring(0,global_buffers[i][0].length-1)+snpsurfnam.sort(sorting)[i].substring(snpsurfnam.sort(sorting)[i].length-1);
		//		}

		for (var i=0; i<global_buffers.length; i++) {
			var currBuff = global_buffers[i];
			if ("Binding Site #"+currBuff.bs_id+"_"+currBuff.ltype == snpsurfnam[0]) {
				global_buffer = currBuff.surf;
				$("#legendChangeBuffer").html("<img src='slike/"+currBuff.ltype.toLowerCase()+".png' class = 'instructionsImg'></img> Binding Site Surface <span style='color:"+colorScheme.surfaceToColorWord[currBuff.ltype.toLowerCase()]+"'>("+colorScheme.surfaceToColorWord[currBuff.ltype.toLowerCase()]+" surface)</span>");
				break;
			}
		}
	}

	if (canvas) {
		//create canvas
		canvas.molmilViewer.clear(); canvas.renderer.initBuffers(); canvas.renderer.camera.reset();
		canvas = molmil.createViewer(canvas);

		//load PDB from database; if that doesnt work load it locally from computer
		//two of the same PDBs are loaded; one is for structure, other is for surfaces
		try {
			molmil.loadPDB(uinput.substring(0, 4), null, null, canvas.molmilViewer);
			//molmil.loadPDB(uinput.substring(0, 4), null, null, canvas.molmilViewer);
			var struc = canvas.molmilViewer.structures[0]; cb(struc);	    
		}
		//dissadvantage: locally you have to load the same PDB two times
		catch(e) {
			obtain_pdb(uinput.substring(0, 4),uinput.substring(4, 5),cb);	
			//change_events();
			//canvas.molmilViewer.UI.open("mmJSON",4, null, null, null, cb, chainsListStr = chainsListStr, snploci=snploci, cb2 = create_draggable);
		}
	}

	//edit loaded PDB to show what we want
	function cb(struc, cb2 = initiate_tables) {

		//var struc2 = canvas.molmilViewer.structures[1];
		molmil_snps["locus"] = {};
		molmil_snps["currSel"] = [];
		molmil_snps["missing"] = {};
		molmil_snps["molmilID"] = {};
		molmil_snps["ligands"] = {};

		molmil_surface = {"color": "Structure"};

		//struc2.display = (global_buffer==null);
		for (var i=0; i<snpsurfnam.length; i++) {
			molmil_surface[snpsurfnam[i]] = {"visible": global_buffer==null};
			molmil_surface[snpsurfnam[i]+"_SNP"] = {"visible": global_buffer==null};
			molmil_surface[snpsurfnam[i]]["dictionary"] = {};
			molmil_surface[snpsurfnam[i]+"_SNP"]["dictionary"] = {};
			molmil_surface[snpsurfnam[i]+"_SNP"]["snps"] = [];
			bindingSiteCols[(snpsurfnam[i]).split("_")[1]][(snpsurfnam[i]).split("_")[0]] = [];
		}

		molmil_surface["current"] = {};

		chainsList = [];

		var label_no = 0;

		var snp_temp = {};

		var curr_temp = [];

		//for each chain in PDB do...
		for (var c=0, m, mol, a; c<struc.chains.length; c++) {
			for (var i=0; i<snpsurfnam.length; i++) {
				molmil_surface[snpsurfnam[i]]["dictionary"][String(c)] = [];
				molmil_surface[snpsurfnam[i]+"_SNP"]["dictionary"][String(c)] = [];
			}
			molmil_surface["current"][String(c)] = {};

			//foaces no display of chains is needed

			var chain = struc.chains[c];

			//if chain is not selected, dont show it
			if (chainsListStr.indexOf(chain.authName) == -1) {
				chain.display = 0;
				//struc2.chains[c].display = false;
			}

			else {

				try{
					var partialSurface = [];

					chainsList.push(c);

					//for each molecule in a chain do...
					//console.log(snplociMolmil);
					//console.log(snploci);

					var countNo = 0;
					var oldRSID = chain.molecules[0].RSID||"-1";

					for (m=0; m<chain.molecules.length; m++) {
						var mol = chain.molecules[m];
						
						if (mol.ligand) {
							molmil.displayEntry(mol, molmil.displayMode_None, false, canvas.molmilViewer);
							try {
								molmil_snps["ligands"][mol.name]["list"].push({"id": mol.id, "rsid": mol.rsid, "chain": c, "mol": m});
							}
							catch(e) {
								molmil_snps["ligands"][mol.name] = {"visible": false, "list": [{"id": mol.id, "rsid": mol.rsid, "chain": c, "mol": m}]};
							}
						}
						//console.log(mol);
						var rsid = String(mol.RSID)||"-1";
						var MID = String(mol.id)||"-1";
						var isPartOfSurface = false;

						if (rsid == oldRSID && rsid!="-1")
							countNo += 1;
						else {
							countNo = 0;
							oldRSID = rsid;
						}
						rsidCheck = "-1";

						for (var key in seqToRSID) {
							if (key.replace(/\D+/g, '') == rsid) {
								var RSIDletter = key.replace(/[0-9]/g, '');
								if (RSIDletter.length==0) {
									rsidCheck = key;
								}
								else if (alphabet.indexOf(RSIDletter.toLowerCase())+1==countNo) {
									rsidCheck = key;
								}			
							}
						}

						//						else if ((Object.keys(seqToRSID)).indexOf(MID) != -1) {
						//						rsidCheck = snplociMolmilInv[seqToRSID[MID]];
						//console.log(snploci[snplociMolmil[snplociMolmilInv[seqToRSID[MID]]]]);
						//}

						//if a molecule is one of SNPs show it in different color and style and remember it
						// try{
						// 	if(	snploci[snplociMolmil[rsid]][0][1].split(".")[1].substring(0,3).toLowerCase != mol.name.toLowerCase){
						// 	    console.log("Validity test..")
						// 	}
						// }catch(ex){
						// 	//response
						// 	$("#customAlerts").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> Residue mapping problem. <strong>We are actively developing solution for such cases. </strong>");
						// 	$('#loadmodal').modal('hide'); // hide loading dialog.


						// }

						//tale del bo vzel id-je namest rsid-jev!
						//console.log(snploci);

						if ((Object.keys(snploci)).indexOf(seqToRSID[rsidCheck]) != -1) {
							//NEW CODE
							if ((snploci[seqToRSID[rsidCheck]][0][1])["respdb"].toLowerCase() == mol.name.toLowerCase()) {
//							if (snploci[seqToRSID[rsidCheck]][0][1].split(".")[1].substring(0,3).toLowerCase() == mol.name.toLowerCase()) {
								mol.RSID = rsidCheck;
								mol.rgba = colorScheme["SNP"];
								//$('#loadmodal').modal('show'); // hide loading dialog.

								for (a=0; a<mol.atoms.length; a++) {
									mol.atoms[a].rgba = colorScheme["SNP"]; 						
									mol.atoms[a].displayMode = 2; 						
								} 					
								mol.showSC = true; 					
								mol.chain.twoDcache = null;
								(function(cb = function(snp_on_locus,SNPcols_on_locus,isPartOfSurface) {
									if (isPartOfSurface)
										molmil_snps["locus"][seqToRSID[rsidCheck]] = {"selected": true, "visible": true, "snps": snp_on_locus, "color": null, "style": "Default", "inSurface": true}; 
									else
										molmil_snps["locus"][seqToRSID[rsidCheck]] = {"selected": true, "visible": true, "snps": snp_on_locus, "color": null, "style": "Default", "inSurface": false}; 
									SNPcols.push(SNPcols_on_locus);
									delete snploci[seqToRSID[rsidCheck]];
								}){
									var snp_on_locus = []
									var SNPcols_on_locus = {"data": [], "bindingSite": []};

									//if the molecule is also part of a surface, remember it
									if ((Object.keys(snpsurf)).indexOf(rsid) != -1) {
										isPartOfSurface = true;
										for (var i=0; i<snpsurf[rsid].length; i++) {
											molmil_surface[snpsurf[rsid][i]+"_SNP"]["dictionary"][String(c)].push(m);
											molmil_surface[snpsurf[rsid][i]+"_SNP"]["snps"].push(seqToRSID[rsidCheck]);
											SNPcols_on_locus["bindingSite"].push(snpsurf[rsid][i]);
											var data = [mol.name.charAt(0) + mol.name.slice(1).toLowerCase() + rsidCheck];
											for (var n=0; n<snploci[seqToRSID[rsidCheck]].length; n++) {
												var snp = snploci[seqToRSID[rsidCheck]][n];
												//NEW CODE
												var checkData = (snp[1])["change"];
												//var checkData = (snp[1]).split("_")[2].split(".")[1];
												if (data.indexOf(checkData.slice(0,checkData.length-3)) == -1) data.push(checkData.slice(0,checkData.length-3));
											}
											var splitSurf = (snpsurf[rsid][i]).split("_");
											bindingSiteCols[splitSurf[1]][splitSurf[0]].push({"data": data, "SNP": true});
											//										if (surface_type == true) {
											//										try { molmil_surface["current"][String(c)][snpsurf[rsid][i]+"_SNP"].push(m);}
											//										catch(e) { molmil_surface["current"][String(c)][snpsurf[rsid][i]+"_SNP"]=[m];}
											//										}
										}
									}

									for (var n=0; n<snploci[seqToRSID[rsidCheck]].length; n++) {
										var snp = snploci[seqToRSID[rsidCheck]][n];
										try {
											var CA = mol.CA;
											if (!(typeof CA === 'undefined')) {
												var all = molmil.calcCenter(CA.molecule); 
												//var all_mol = molmil.calcCenter(struc);
												//NEW CODE
												molmil.addLabel(snp[1]["change"]+": "+snp[1]["id"], {xyz: all[0], dz: all[1]+2.0, fontSize: 15, color: colorScheme["label"]}, canvas.molmilViewer); 

//												var name_rsid = snp[1].split("_");
//												molmil.addLabel(name_rsid[2].split(".")[1].substring(0,name_rsid[2].split(".")[1].length)+": "+name_rsid[3], {xyz: all[0], dz: all[1]+2.0, fontSize: 15, color: colorScheme["label"]}, canvas.molmilViewer); 
												var label = canvas.molmilViewer.texturedBillBoards[label_no];
												label.display = false; 
												label.status = false;
											}
											else {
												console.log("following molecule has no CA");
												console.log(mol);
											}
										}
										catch(e) {
											console.log("label couldn't be generated!");
											console.log(e);
										}

										snp_on_locus.push(snp[0]);
										if ((SNPcols_on_locus["data"]).indexOf(snp[0]) == -1) 
											SNPcols_on_locus["data"].push(snp[0]);
										else console.log("double snp detected");

										var locus = seqToRSID[rsidCheck];
										
										//NEW CODE
										if ((checkByRSID).indexOf(rsidCheck) != -1) {
											snp_temp[snp[0]] = {"gene": snp[1]["gene"], "variation": snp[1]["variation"], "uniprotid": snp[1]["uniprotid"], "change": snp[1]["change"], "species": snp[1]["species"], "id": snp[1]["id"], "chain": c, "mol": m, "locus": locus, "label_no": label_no, "visible": eval(n==0), "in_surface": true};
										}
										else {
											snp_temp[snp[0]] = {"gene": snp[1]["gene"], "variation": snp[1]["variation"], "uniprotid": snp[1]["uniprotid"], "change": snp[1]["change"], "species": snp[1]["species"], "id": snp[1]["id"], "chain": c, "mol": m, "locus": locus, "label_no": label_no, "visible": eval(n==0), "in_surface": false};
										}
										
										
										
//										if ((checkByRSID).indexOf(rsidCheck) != -1) {
//											snp_temp[snp[0]] = {"gene": snp[1].split("_")[1], "variation": snp[1].split("_")[9], "uniprotid": snp[1].split("_")[0], "change": (snp[1].split("_")[2]).split(".")[1], "species": snp[1].split("_")[7]+" "+snp[1].split("_")[8], "id": snp[1].split("_")[3], "chain": c, "mol": m, "locus": locus, "label_no": label_no, "visible": eval(n==0), "in_surface": true};
//										}
//										else {
//											snp_temp[snp[0]] = {"gene": snp[1].split("_")[1], "variation": snp[1].split("_")[9], "uniprotid": snp[1].split("_")[0], "change": (snp[1].split("_")[2]).split(".")[1], "species": snp[1].split("_")[7]+" "+snp[1].split("_")[8], "id": snp[1].split("_")[3], "chain": c, "mol": m, "locus": locus, "label_no": label_no, "visible": eval(n==0), "in_surface": false};
//										}
										if (canvas.molmilViewer.texturedBillBoards.length == label_no+1)
											label_no = label_no+1;
										else
											snp_temp[snp[0]]["label_no"] = null;
									}
									cb(snp_on_locus,SNPcols_on_locus,isPartOfSurface);
								})();
							}
							else  {
								//console.log(rsidCheck+"(rsid:"+snplociMolmil[rsidCheck]+", MID:"+MID+")"+snploci[snplociMolmil[rsidCheck]][0][1].split(".")[1].substring(0,3)+" does not match "+rsidCheck+mol.name+" in molmil");
							}
						}

						//if a molecule isnt SNP but is part of surface, remeber it
						else if ((Object.keys(snpsurf)).indexOf(rsid) != -1) {
							for (var i=0; i<snpsurf[rsid].length; i++) {
								molmil_surface[snpsurf[rsid][i]]["dictionary"][String(c)].push(m);
								bindingSiteCols[(snpsurf[rsid][i]).split("_")[1]][(snpsurf[rsid][i]).split("_")[0]].push({"data": [mol.name.charAt(0) + mol.name.slice(1).toLowerCase() + rsid], "SNP": false});
								//							if (surface_type == true) {
								//							try { molmil_surface["current"][String(c)][snpsurf[rsid][i]].push(m);}
								//							catch(e) { molmil_surface["current"][String(c)][snpsurf[rsid][i]]=[m];}
								//							}
							}
						}
					}
				}catch(err){
					console.log("Errors found.");
					console.log(err);
					console.log(rsidCheck);
					console.log(seqToRSID[rsidCheck]);
				}
			}
		}

		molmil_snps["snps"] = snp_temp;
		molmil_snps["missing"] = snploci;

		//bindingSiteCols["TestBindingSites"] = [{"data": ["test1"], "SNP": true}, {"data": ["test2"], "SNP": false}];
		//hide waters and hydrogens
		canvas.molmilViewer.waterToggle(false);
		canvas.molmilViewer.hydrogenToggle(false);

		//fake Test surface
		//molmil_surface["Test"]["dictionary"]["0"] = [];
		//molmil_surface["Predicted_SNP"]["dictionary"]["0"] = [1];

		if (snpsurfnam.length > 0) {
			for (var key in molmil_surface["current"]) {
				molmil_surface["current"][key][snpsurfnam[0]] = molmil_surface[snpsurfnam[0]]["dictionary"][key];
				molmil_surface["current"][key][snpsurfnam[0]+"_SNP"] = molmil_surface[snpsurfnam[0]+"_SNP"]["dictionary"][key];
			}
		}

		var selected = molmil_surface["current"];

		if (surface_type == true) {
			//initiate default surface

			molmil_surface[snpsurfnam[0]]["visible"] = true;

			//molmil.displayEntry(canvas.molmilViewer.structures[1], molmil.displayMode_CustomSurface, false, canvas.molmilViewer, null, []);

			showSurfaceSticks(selected);
			showSurfaceSNPs(selected);
		}
		else if (surface_type) {
			showSurfaceSNPs(selected);
		}

		var all_atoms = []
		var all_chains = [];
		for (var c=0; c<chainsList.length; c++) {
			all_atoms.push.apply(all_atoms, canvas.molmilViewer.structures[0].chains[chainsList[c]].atoms)
			all_chains.push(canvas.molmilViewer.structures[0].chains[chainsList[c]]);
		}

		//console.log(canvas.molmilViewer.structures[0].chains[1].molecules[12]);
		var all =  molmil.calcCenter(all_chains); 
		canvas.molmilViewer.avgXYZ = all[0];
		molmil.selectAtoms(all_atoms, null, canvas.molmilViewer);   
		canvas.renderer.updateSelection();    
		molmil.selectionFocus(canvas.molmilViewer);
		molmil.selectAtoms([], null, canvas.molmilViewer);
		
		var checkboxesHTML = "";
		var bindingSiteLen = 0;
		for (var i=0; i<molmil_snps["sort"]["bsType"].length; i++) {
			var key = molmil_snps["sort"]["bsType"][i];
			for (var j=0; j<molmil_snps["sort"][key].length; j++) {
				var key2 = molmil_snps["sort"][key][j];
				checkboxesHTML += "<a onClick='changeSequence(this);' class='btn btn-success checkboxOption "+key.toLowerCase()+"' href='#' value='"+key.toLowerCase()+" "+key2.split(" ")[2]+"'>"+key.slice(0,1).toUpperCase()+key.slice(1)+" "+key2.split(" ")[2]+"</a>";
				bindingSiteLen += bindingSiteCols[key][key2].length;
			}
		}

		$("#checkboxes").html(checkboxesHTML);

		if (bindingSiteLen == 0) surface_type = false;

		changeSequence(null);
		cb2(bindingSiteLen, success = true);
	}

	function initiate_tables(bindingSiteLen, success) {
		console.log("initiate tables");
		onResizeMolmil();
		canvas.renderer.initBuffers(surface_type);
		canvas.update = true;

		//load table
		var cresult = init_SNP(SNPcols, uinput);
	    
		if (bindingSiteLen > 0) {
			init_bindingSites(bindingSiteCols);
			$("#displaySticksBtn").parent().show();
			if (surface_type == true) {
				$("#displaySticksBtn").addClass('active');
			}
			else if (surface_type) {
				$("#displaySurfaceBtn").addClass('active');
			}
			if(ligandObject.length>0) {
				init_ligands();
			}				
			else {
				$("#lig-tab").html("No ligands were found");
			}
		}
		else {
			$("#displaySticksBtn").parent().hide();
			$("#BS-tab").html("No binding sites were found");
			$("#lig-tab").html("No binding sites and thus ligands were found");
		}
		if (cresult == 0){    
			$("#customAlerts").html("<div class='alert alert-danger' role='alert'><strong>Oooops.</strong> Sequence Variants were only found for <strong>different chains! </strong>");
			tries=6;   
		}
		else{
			console.log("table generated..");
			if (success) {
				change_events();
				create_draggable(uinput, snpsurfnam);

				//default selections
				$("#a_Color_Structure").addClass("active"); 
				$("#a_Style_Cartoon").addClass("active"); 

				restartDsearch("searchSNP", "genTab");
				initDsearch("searchSNP", "genTab");
				openNav(); // po defaultu odpre menu
				openLegend();
				closeMainNav();

				var currentResult = document.getElementById('currentResult');

				// replace text in HTML string:
				currentResult.innerHTML = uinput;

				var index = searchHistory.indexOf(uinput);
				if (index != -1)
					searchHistory.splice(index, 1);
				searchHistory.push(uinput); //add search to history
				searchHistory = searchHistory.slice(Math.max(searchHistory.length - maxSearchNo, 0))

				if (searchHistory.length > 1) {
					var htmlCont = "Or switch back to one of the previous searches:";					
					for (var i=0; i<searchHistory.length-1; i++) {
						var ii = searchHistory.length-2-i;
						htmlCont += "<a href='#'  onClick=\"historyResult('"+searchHistory[ii]+"');\">"+searchHistory[ii]+"</a><br>";
					}
					$("#searchHistory").html(htmlCont);
				}
			}

		}
		closeLoading();
	}
}

//create draggable navigation bar menu
function create_draggable(uinput, snpsurfnam) {

	if (document.contains(document.getElementById("draggable"))) {
		document.getElementById("draggable").remove();
	} 

	var menu = new Menu("#StructureContainer");
	var item1 = new Item(uinput, snpsurfnam);

	menu.add(item1);

	var width = 0;
	$('#myUL li').each(function() {
		width += $(this).outerWidth();
	});
	$('#draggable').css({"width": width});

	if (document.contains(document.getElementById("draggableTable"))) {
		document.getElementById("draggableTable").remove();
	} 

	var tableMenu = new Menu("#StructureContainer");
	var item2 = new Table();

	tableMenu.add(item2);

	var ligandPlayer = new Menu("#StructureContainer");
	var item3 = new LigandPlayer("ASI");

	ligandPlayer.add(item3);
}


//window.onhashchange = function() {
//var hash = window.location.hash ? window.location.hash.substr(1) : "";
//if (hash) {molmil.clear(canvas); cli.environment.console.runCommand(decodeURI(hash));}
//}



function aminoview(resultarray3){ //initiates the listeners for quickview
	//delay in this case is 3 loci, meaning first aa is located at k-3 position!

	for (k=0;k<resultarray3.length;k++){
		if ((k+1)%3==0){
			$("#firstAA" + resultarray3[k]).click((function(k) {
				return function() {
					$("#fAA" + resultarray3[k]).slideToggle();
				}
			})(k));


			$("#secondAA" + resultarray3[k]).click((function(k) {
				return function() {
					$("#sAA" + resultarray3[k]).slideToggle();
				}
			})(k));


		}

	}

};



function toggleSNPselection(buttonID){
	//add SNP name to every options submenu in draggable
	function add_li(textList) {
		textList = textList.reverse()
		var htmlContent = $("#draggableTableCont > tbody").html();
		var TableById = document.getElementById("draggableTableCont");
		if (htmlContent == "No Sequence Variants selected.") $("#draggableTableCont > tbody").html("");
		var noRows = document.getElementById("draggableTableCont").rows.length;
		if (noRows == 0) {
			var r = document.getElementById("draggableTableCont").insertRow(0);
			var noCols = document.getElementById("draggableTableCont").rows[0].cells.length;
			//var c = r.insertCell(0);
		}
		else
			var noCols = document.getElementById("draggableTableCont").rows[noRows-1].cells.length;


		if (noRows+noCols == 0) {
			for (var i=0; i<textList.length; i++) {
				var c = (noCols+i)%5;
				var r= noRows+Math.floor((noCols+i)/5);

				if (c==0 && r>noRows) {
					document.getElementById("draggableTableCont").insertRow(r);
				}
				var newCell=TableById.rows[r].insertCell(c);
				newCell.innerHTML= "";
			}
		}
		else {

			for (var i=1; i<textList.length+1; i++) {
				var c = (noCols+i-1)%5;
				var r= noRows-1+Math.floor((noCols+i-1)/5);

				if (c==0 && r>noRows-1) {
					document.getElementById("draggableTableCont").insertRow(r);
				}
				var newCell=TableById.rows[r].insertCell(c);
				newCell.innerHTML= "";
			}
		}


		if (noRows != 0 || noCols != 0)
			for (var i=0; i<((noRows-1)*5+noCols); i++) {
				j = ((noRows-1)*5+noCols)-i-1;
				var r = Math.floor(j/5);
				var c = j%5;
				var rNew = Math.floor((j+textList.length)/5);
				var cNew = (j+textList.length)%5;

				$('#draggableTableCont tr:eq('+rNew+') td:eq('+cNew+')').html($('#draggableTableCont tr:eq('+r+') td:eq('+c+')').html());
				$('#draggableTableCont tr:eq('+rNew+') td:eq('+cNew+')').attr('id', $('#draggableTableCont tr:eq('+r+') td:eq('+c+')').attr('id'));
				$('#draggableTableCont tr:eq('+rNew+') td:eq('+cNew+')').attr('class', $('#draggableTableCont tr:eq('+r+') td:eq('+c+')').attr('class'));

			}

		for (var i=0; i<textList.length; i++) {
			var r = Math.floor(i/5);
			var c = i%5;

			var text = molmil_snps["snps"][textList[i]]["id"];
			var textid = textList[i];

			var html = text+"<br>"
			html += "<input id= 'a_SNPs_"+textid+"' class= 'button SNPTableButton active' value= 'H' type= 'button'>";
			if (molmil_snps["snps"][textList[i]]["label_no"] != null)
				html += "<input id= 'a_Label_"+textid+"' class= 'button SNPTableButton' value= 'L' type= 'button'>";
			html += "<input id= 'a_Zoom on_"+textid+"' class= 'button SNPTableButton' value= 'Z' type= 'button'>"
			$('#draggableTableCont tr:eq('+r+') td:eq('+c+')').html(html);
			$('#draggableTableCont tr:eq('+r+') td:eq('+c+')').attr('id', 'draggableTable_cell_'+textid);
			$('#draggableTableCont tr:eq('+r+') td:eq('+c+')').attr('class', 'draggableTableCell');

		}
	}


	function remove_li(textList) {
		var htmlContent = $("#draggableTableCont > tbody").html();
		var TableById = document.getElementById("draggableTableCont");
		var noRows =  TableById.rows.length;
		var rows = htmlContent.split("<tr>")
		if ((noRows-1)*5+document.getElementById("draggableTableCont").rows[noRows-1].cells.length == textList.length) $("#draggableTableCont > tbody").html("No Sequence Variants selected.");
		else {
			for (var i=0; i<textList.length; i++) {
				var text = textList[i];
				$("#draggableTable_cell_"+text).remove();
			}
		}

		for (var r=0; r<noRows; r++) {
			for (var c=0; c<5; c++) {
				if ($('#draggableTableCont tr:eq('+r+') td:eq('+c+')').html() == undefined) {
					var jumpFactor = 1;
					var rNew = r+Math.floor((c+1)/5);
					var cNew = (c+1)%5;

					while ($('#draggableTableCont tr:eq('+rNew+') td:eq('+cNew+')').html() == undefined && jumpFactor<textList.length+1) {
						jumpFactor += 1;
						rNew = rNew+Math.floor((cNew+1)/5);
						cNew = (cNew+1)%5;
					}

					if ($('#draggableTableCont tr:eq('+rNew+') td:eq('+cNew+')').html() != undefined) {
						var newCell=TableById.rows[r].insertCell(c);
						newCell.innerHTML= $('#draggableTableCont tr:eq('+rNew+') td:eq('+cNew+')').html();
						$('#draggableTableCont tr:eq('+r+') td:eq('+c+')').attr('id', $('#draggableTableCont tr:eq('+rNew+') td:eq('+cNew+')').attr('id'));
						$('#draggableTableCont tr:eq('+r+') td:eq('+c+')').attr('class', $('#draggableTableCont tr:eq('+rNew+') td:eq('+cNew+')').attr('class'));
						$('#draggableTableCont tr:eq('+rNew+') td:eq('+cNew+')').remove();
					}
				}
			}
		} 

		var rowToDelete = noRows-Math.floor(textList.length/5);

		while ($('#draggableTableCont tr:eq('+rowToDelete+')').length>0) {
			$('#draggableTableCont tr:eq('+rowToDelete+')').remove();
		}

	}

	var SNPinfo = buttonID.id.split("_");
	SNPinfo2 = buttonID.parentNode.parentNode;
	SNPinfo2 = SNPinfo2.id.split("_");
	var obj = canvas.molmilViewer.structures[0];
	// console.log(canvas.molmilViewer.structures[0]);
	//select/deselect all button
	if (SNPinfo[0] == "selectAll"){
		if (SNPinfo[1] == "SNP") var buttons = document.getElementsByClassName("resultButton");
		else {
			//console.log(buttonID.parentNode.parentNode);
			//console.log(getSiblings(buttonID));
			//console.log(buttonID.siblings());
			var buttons = document.getElementsByClassName("resultButton_"+SNPinfo[1].replace(/ /g,"_").replace("#","")+"_"+SNPinfo[SNPinfo.length-1]);
		}

		var buttonsSNP = [];
		for (var i = 0; i<buttons.length; i++) {
			var buttonName = buttons[i];
			buttonName = buttonName.parentNode.parentNode;
			buttonName = buttonName.id.split("_");
			if (buttonsSNP.indexOf(buttonName[2]) == -1)
				buttonsSNP.push(buttonName[2]);
		}

		if (document.getElementById(buttonID.id).value == "D") {
			var molecules = [];
			if (SNPinfo[1] == "SNP") {
				var selectAllButtons = document.getElementsByClassName("selectAllButton");
				for (var i = 0; i<selectAllButtons.length; i++) {
					selectAllButtons[i].value = "S"; 
					selectAllButtons[i].classList.remove("active");
				}
			}
			else {
				buttonID.value = "S"; 
				buttonID.classList.remove("active");
			}

			for (var i=0; i<buttons.length; i++) {
				buttons[i].value = "S";
				buttons[i].removeClass("active");
			}

			if (SNPinfo[1] == "SNP") $("#draggableTableCont > tbody").html("No Sequence Variants selected.");

			var LabelButtonsSelect = document.getElementsByClassName("resultButtonLabel");
			for (var j=0; j<LabelButtonsSelect.length; j++) {
				LabelButtonsSelect[j].removeClass("active");
			}

			for (var i=0; i<buttonsSNP.length; i++) {
				var key = buttonsSNP[i];
				var snp = molmil_snps["snps"][key];

				if (snp["label_no"] != null) {
					var label = canvas.molmilViewer.texturedBillBoards[snp["label_no"]];

					label.display = false; 
					label.status = false;
				}		

				snp["visible"] = false;

				if (molmil_snps["locus"][snp["locus"]]["selected"]) {
					molmil_snps["locus"][snp["locus"]]["selected"] = false;
					molmil_snps["locus"][snp["locus"]]["visible"] = false;
					var mol = obj.chains[snp["chain"]].molecules[snp["mol"]];
					if (molmil_snps["locus"][snp["locus"]]["inSurface"] && surface_type==true) molmil.colorEntry(mol,molmil.colorEntry_Custom, [0,255,0,255], false, molmilViewer);
					else if (molmil_snps["locus"][snp["locus"]]["color"]) molmil.colorEntry(mol, molmil.colorEntry_Custom, molmil_snps["locus"][snp["locus"]]["color"], false, molmilViewer);
					else molmil.colorEntry(mol, molmil.colorEntry_Structure, null, false, molmilViewer);

					var style = molmil_snps["locus"][snp["locus"]]["style"];
					if (molmil_snps["locus"][snp["locus"]]["inSurface"] && surface_type==true) molmil.displayEntry(mol, molmil.displayMode_Stick, false, canvas.molmilViewer);
					if (style == "CartoonRocket") {
						if (! mol.ligand && ! mol.water) {for (a=0; a<mol.atoms.length; a++) mol.atoms[a].displayMode = 0;}
						mol.displayMode = 31;
						mol.showSC = false;		
					}
					else molmil.displayEntry(mol, eval("molmil.displayMode_"+molmil_snps["locus"][snp["locus"]]["style"]), false, canvas.molmilViewer);
					molecules.push(mol);
				}
			}

			if (SNPinfo[1] != "SNP")
				remove_li(buttonsSNP);

			molmil_snps["currSel"] = [];
			//molmil.selectAtoms([], null, canvas.molmilViewer);
		}

		else {

			if (SNPinfo[1] == "SNP") {
				var selectAllButtons = document.getElementsByClassName("selectAllButton");
				for (var i = 0; i<selectAllButtons.length; i++) {
					selectAllButtons[i].value = "D"; 
					selectAllButtons[i].classList.add("active");
				}
			}
			else {
				buttonID.value = "D";
				buttonID.classList.add("active");
			}

			if (SNPinfo[1] == "SNP")
				var snp_keys = Object.keys(molmil_snps["locus"]);
			else {
				var snp_keys = [];
				for (var i=0; i<buttonsSNP.length; i++) {
					if (molmil_snps["snps"][buttonsSNP[i]]["button_id"].slice(-5) == "first") {
						snp_keys.push(molmil_snps["snps"][buttonsSNP[i]]["locus"]);
					}
				}
			}

			//console.log(snp_keys);
			//console.log(Object.keys(molmil_snps["locus"]));

			var textList = [];

			for (var i=0; i<snp_keys.length; i++) {
				var key = snp_keys[snp_keys.length-i-1];
				var snp = molmil_snps["snps"][molmil_snps["locus"][key]["snps"][0]];

				if(!snp["visible"]) {
					var buttonsSelect = document.getElementsByClassName(molmil_snps["locus"][key]["snps"][0]+" resultButton");

					for (var j=0; j<buttonsSelect.length; j++) {
						buttonsSelect[j].value = "D";
						buttonsSelect[j].className += " active";
					}

					molmil_snps["locus"][key]["selected"] = molmil_snps["locus"][key]["snps"][0];
					molmil_snps["locus"][key]["visible"] = true;
					snp["visible"] = true;

					var mol = obj.chains[snp["chain"]].molecules[snp["mol"]];

					mol.rgba = colorScheme["SNP"];
					for (a=0; a<mol.atoms.length; a++) {
						mol.atoms[a].rgba = colorScheme["SNP"];
						mol.atoms[a].displayMode = 2;
					}
					mol.showSC = true;
					mol.chain.twoDcache = null;	

					textList.push(molmil_snps["locus"][key]["snps"][0])
				}
			}

			add_li(textList);
		}
		if (surface_type == true) {
			showSurfaceSticks(molmil_surface["current"]);
			showSurfaceSNPs(molmil_surface["current"]);
		}	
		else if (surface_type)
			showSurfaceSNPs(molmil_surface["current"]);
	}

	//	add label button (toggle)
	else if (buttonID.className.indexOf("resultButtonLabel") != -1) {
		var snp = molmil_snps["snps"][SNPinfo2[2]];
		var label = canvas.molmilViewer.texturedBillBoards[snp["label_no"]];
		var buttonsSelect = document.getElementsByClassName(SNPinfo2[2]+" resultButtonLabel");
		if (label.display) {
			label.display = false; 
			label.status = false;

			for (var j=0; j<buttonsSelect.length; j++) {
				buttonsSelect[j].removeClass("active");
			}
			document.getElementById("a_Label_"+SNPinfo2[2]).removeClass("active");
		}
		else {
			label.display = true; 
			label.status = false;
			for (var j=0; j<buttonsSelect.length; j++) {
				buttonsSelect[j].className += " active";
			}
			try {
				document.getElementById("a_Label_"+SNPinfo2[2]).className += " active";
			}
			catch(e) {
				console.log("This Sequence Variant is not selected!");
			}
		}
	}

	else if (buttonID.className.indexOf("resultButtonZoom") != -1) {

		var snp = molmil_snps["snps"][SNPinfo2[2]];
		var buttonsSelect = document.getElementsByClassName("resultButtonZoom");

		if (!(buttonID.className.match(/(?:^|\s)active(?!\S)/))) {
			if (snp["visible"]) {
				var mol = obj.chains[snp["chain"]].molecules[snp["mol"]];
				atoms = mol.atoms;
				var elements = document.getElementsByClassName("SNPTableButton");
				for (var i = 0; i < elements.length; i++) {
					if (elements[i].id.split("_")[1] == "Zoom on")
						elements[i].removeClass("active");
				}

				for (var j=0; j<buttonsSelect.length; j++) {
					if (buttonsSelect[j].classList.contains(SNPinfo2[2]))
						buttonsSelect[j].className += " active";
					else
						buttonsSelect[j].removeClass("active");
				}
				try {document.getElementById("a_Zoom on_"+SNPinfo2[2]).className += " active";}
				catch(e) {
					console.log("This Sequence Variant was not selected!")
				}

				//$("#"+$(this).attr('id')).removeClass("active");
				//this.className += " active";
				molmil_snps["currSel"] = atoms;

				molmil.selectAtoms(atoms, false, canvas.molmilViewer);  
				canvas.molmilViewer.gotoMol(mol);
				//molmil.selectionFocus(canvas.molmilViewer); 
				canvas.renderer.initBuffers(surface_type);
				canvas.update = true;
				
				molmil.selectAtoms(molmil_snps["currSel"], null, canvas.molmilViewer); 

				canvas.renderer.initBuffers(surface_type);
				canvas.update = true;
			}
		}
		else {
			document.getElementById("a_Focus").click();
			for (var j=0; j<buttonsSelect.length; j++) {
				buttonsSelect[j].removeClass("active");
			}
			try {document.getElementById("a_Zoom on_"+SNPinfo2[2]).removeClass("active");}
			catch(e) {
				console.log("This Sequence Variant was not selected!")
			}


			//document.getElementById(id[1]).classList.toggle("show");

			//oldData = $('#draggable').html();

			//this.removeClass("active");
		}
	}

	//	individual select/deselect buttons
	else{
		var snp = molmil_snps["snps"][SNPinfo2[2]];
		console.log(snp);
		var mol = obj.chains[snp["chain"]].molecules[snp["mol"]];
		var visible = molmil_snps["locus"][snp["locus"]]["selected"];
		var orig_value = buttonID.value;
		atoms = [];

		//if the already visible (if any) SNP on locus is not the same we (de)selected:
		//draw SNP, remember which SNP is currently visible on this locus and add SNP name to draggable submenues
		if (visible != SNPinfo2[2]) {
			molmil_snps["locus"][snp["locus"]]["selected"] = SNPinfo2[2];
			molmil_snps["locus"][snp["locus"]]["visible"] = true;
			var buttonsSelect = document.getElementsByClassName(SNPinfo2[2]+" resultButton");

			for (var j=0; j<buttonsSelect.length; j++) {
				buttonsSelect[j].value = "D";
				buttonsSelect[j].className += " active";
			}

			snp["visible"] = true;
			mol.rgba = colorScheme["SNP"];
			for (a=0; a<mol.atoms.length; a++) {
				mol.atoms[a].rgba = colorScheme["SNP"];
				mol.atoms[a].displayMode = 2;
				atoms.push(mol.atoms[a]);
			}
			mol.showSC = true;
			mol.chain.twoDcache = null;	
			molmil_snps["currSel"] = mol.atoms;
			//molmil.selectAtoms(atoms, null, canvas.molmilViewer);  

			add_li([SNPinfo2[2]]);

		}

		//if there already si a SNP visible on selected SNPs' locus:
		//hide the already visible SNP and remove it from draggable submenues
		if (visible) {

			var buttonsSelect = document.getElementsByClassName(visible+" resultButton");

			for (var j=0; j<buttonsSelect.length; j++) {
				buttonsSelect[j].value = "S";
				buttonsSelect[j].removeClass("active");
			}

			molmil_snps["snps"][visible]["visible"] = false;

			try {remove_li([visible]);}
			catch(e) {console.log("no such Sequence Variant in table");}

			//if the selected SNP was the same as already visible, mark the locus as not visible (aka no SNPs are visible on this locus)
			if (visible == SNPinfo2[2]) {
				molmil_snps["locus"][snp["locus"]]["selected"] = false;
				molmil_snps["locus"][snp["locus"]]["visible"] = false;
			}

			var snp = molmil_snps["snps"][visible];
			var label = canvas.molmilViewer.texturedBillBoards[snp["label_no"]];

			label.display = false; 
			label.status = false;

			var labelButtonsSelect = document.getElementsByClassName(visible+" resultButtonLabel");

			for (var j=0; j<labelButtonsSelect.length; j++) {
				labelButtonsSelect[j].removeClass("active");
			}

			if (orig_value == "D") {
				var mol = obj.chains[snp["chain"]].molecules[snp["mol"]];
				if (molmil_snps["locus"][snp["locus"]]["inSurface"] && surface_type == true) molmil.colorEntry(mol,molmil.colorEntry_Custom, [0,255,0,255], false, molmilViewer);
				else if (molmil_snps["locus"][snp["locus"]]["color"]) molmil.colorEntry(mol, molmil.colorEntry_Custom, molmil_snps["locus"][snp["locus"]]["color"], false, molmilViewer);
				else molmil.colorEntry(mol, molmil.colorEntry_Structure, null, false, molmilViewer);
				if (molmil_snps["locus"][snp["locus"]]["inSurface"] && surface_type == true) molmil.displayEntry(mol, molmil.displayMode_Stick, false, canvas.molmilViewer);
				else if (molmil_snps["locus"][snp["locus"]]["style"] == "CartoonRocket") {
					if (! mol.ligand && ! mol.water) {for (a=0; a<mol.atoms.length; a++) mol.atoms[a].displayMode = 0;}
					mol.displayMode = 31;
					mol.showSC = false;
				}
				else molmil.displayEntry(mol, eval("molmil.displayMode_"+molmil_snps["locus"][snp["locus"]]["style"]), false, canvas.molmilViewer);

				molmil_snps["currSel"] = [];
				//molmil.selectAtoms([], null, canvas.molmilViewer);

				//document.getElementById("cell_"+visible.split("_")[3]).remove();				
			}
		}
		if (surface_type == true) {
			showSurfaceSNPs(molmil_surface["current"]);
			showSurfaceSticks(molmil_surface["current"]);
		}
		else if (surface_type)
			showSurfaceSNPs(molmil_surface["current"]);

	}

	//	update canvas
	canvas.renderer.updateSelection();
	canvas.renderer.initBuffers(surface_type); 	
	canvas.update = true;
	oldData = $('#draggable').html();
}

function toggleLigand(ligandINFO) {

	var currentClasses = $(ligandINFO).attr('class');
	var bsLigands = $(ligandINFO).parent().closest('div').attr("id");
	bsLigands = bsLigands.replace("ligands ","");

	if (ligandINFO.id.split("_")[0] == "selectAll") {

		var rows =$(ligandINFO).closest('table').find('tr');
		rowNames = [];

		if (currentClasses.indexOf("active") == -1) {

			$(ligandINFO).attr("value", "D");
			$(ligandINFO).addClass("active");

			for (var i=0; i<ligandObject.length; i++) {
				if ("Binding Site #"+(ligandObject[i]).bs_id+"_"+(ligandObject[i]).ltype == bsLigands && !ligandObject[i]["visible"]) {
					showLigand(i,(ligandObject[i].lig_id).split("_")[4].toUpperCase(),null);
				}
			}

			for (var r=1; r<rows.length; r++) {
				var cells = $(rows[r]).find('td');
				var buttons = $(cells[0]).find('input');
				for (var b=0; b<buttons.length; b++) {
					buttons[b].value = "D";
					buttons[b].className += " active";
				}
			}
		}
		else {

			$(ligandINFO).attr("value", "S");
			$(ligandINFO).removeClass("active");

			for (var i=0; i<ligandObject.length; i++) {
				if ("Binding Site #"+(ligandObject[i]).bs_id+"_"+(ligandObject[i]).ltype == bsLigands) {
					hideLigand((ligandObject[i].lig_id).split("_")[4].toUpperCase());
				}
			}

			for (var r=1; r<rows.length; r++) {
				var cells = $(rows[r]).find('td');
				var buttons = $(cells[0]).find('input');
				for (var b=0; b<buttons.length; b++) {
					buttons[b].value = "S";
					buttons[b].removeClass("active");
				}
			}

		}
		//var buttons = document.getElementsByClassName("resultButtonLigands");
	}
	else {
		var innerHTML = $(ligandINFO).closest('td').next().next()[0].firstChild.innerHTML;

		if (currentClasses.indexOf("active") == -1) {
			$(ligandINFO).addClass('active');
			$(ligandINFO).attr("value", "D");
			for (var i=0; i<ligandObject.length; i++) {
				if ("Binding Site #"+(ligandObject[i]).bs_id+"_"+(ligandObject[i]).ltype == bsLigands && (ligandObject[i].lig_id).split("_")[4] == innerHTML && !ligandObject[i]["visible"]) {
					showLigand(i,innerHTML,null);
				}
			}
		}
		else {
			$(ligandINFO).removeClass('active');
			$(ligandINFO).attr("value", "S");

			hideLigand(innerHTML);
		}

		var list =  [];
		for (var key in molmil_snps["locus"]) {
			if (molmil_snps["locus"].hasOwnProperty(key)) {
				list.push(molmil_snps["locus"][key]["snps"][0]);
			}
		}

	}

	canvas.renderer.initBuffers(surface_type); 	
	canvas.update = true;

}

function historyResult(value) {
	$("#main_input").val(value);
	check_INPUT();
}

$( document ).ready(function() {


	//    $(document).keydown(function(event) {
	//    	if (event.ctrlKey==true && (event.which == '61' || event.which == '107' || event.which == '173' || event.which == '109'  || event.which == '187'  || event.which == '189'  ) ) {
	//            event.preventDefault();
	//    	}
	//    	// 107 Num Key  +
	//    	// 109 Num Key  -
	//    	// 173 Min Key  hyphen/underscor Hey
	//    	// 61 Plus key  +/= key
	//    });
	//
	//    $(window).bind('mousewheel DOMMouseScroll', function (event) {
	//    	if (event.ctrlKey == true) {
	//    	    event.preventDefault();
	//    	}
	//    });

	db_info();

	$('#example_inpt').val("Loading examples..");
	//console.log (location.search)

	var query = location.search.split("=")[1]
	// if ( window.location.hash != ""){

	if (query != null){
		//$('#main_input').val(window.location.hash.replace("#",""));

		$('#main_input').val(query);

		setTimeout(check_INPUT, 500);
	}

	$.ajax({

		url : url_examples,
		type : 'GET',
		success : function(data) {              

			data_examples = data;

			$('#example_inpt').val("Random example");

		},
		error : function(request,error)
		{

			console.log("Examples not found..");
			$('#example_inpt').val("Server failure");

		}
	});

	if(!window.WebGLRenderingContext) {
		// the browser doesn't even know what WebGL is
		$('#webGL_error').modal('show'); 
	}
	else {
		var canvas = document.getElementById("molmilViewer");
		var context = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
		if(!context) {
			// browser supports WebGL but initialization failed
			//alert("WebGL is supported but here is another problem.");
			$('#webGL_error').modal('show'); 

		}
	}

	var canvas = document.createElement("canvas");
	// Get WebGLRenderingContext from canvas element.
	var gl = canvas.getContext("webgl")
	|| canvas.getContext("experimental-webgl");
	// Report the result.
	if (gl && gl instanceof WebGLRenderingContext) {console.log("WebGL enabled! All requirements fulfilled.")} else {
		$('#webGL_error').modal('show'); 
	}

	var canvasID = 'canvas',
	canvas = document.getElementById(canvasID),
	gl,
	glExperimental = false;

	function hasWebGL() {

		try { gl = canvas.getContext("webgl"); }
		catch (x) { gl = null; }

		if (gl === null) {
			try { gl = canvas.getContext("experimental-webgl"); glExperimental = true; }
			catch (x) { gl = null; }
		}

		if(gl) { return true; }
		else if ("WebGLRenderingContext" in window) { return true; } // not a best way, as you're not 100% sure, so you can change it to false
		else { $('#webGL_error').modal('show');  return false; }
	}

	hasWebGL();

	$.ajax({url: url_index, success: function(result){
		console.log("index obtaiend.");
		if (window.mobilecheck()) {
			var y = document.getElementsByClassName('cover-container');
			y[0].style.padding = "0px";
		}
		keyObject = JSON.parse(result);
		//console.log(keyObject);
		$('#main_input').autoComplete({
			minChars: 3,
			source: function(term, suggest){
				term = term.toLowerCase();
				if ($('#main_input').val().startsWith('rs')==true || $('#main_input').val().startsWith('COSM')==true || $('#main_input').val().startsWith('vcZ')==true){

					var choices = keyObject.rs;
					var rslimit = true;
				}else{
					var choices = keyObject.other;
				}
				var matches = [];
				var rscount = 0;
				var pdbcount = 0;
				if (rslimit == true){
					rscount = 0;
					for (i=0; i<choices.length; i++){
						if (~choices[i].toLowerCase().indexOf(term)){
							rscount++;
							if (rscount < 30){
								matches.push(choices[i].replace(' ','').replace(' ',''));
								suggest(matches);
								//rscount = 0;
							}
						}
					}		    		    
				}else{
					for (i=0; i<choices.length; i++){
						pdbcount++;
						if (~choices[i].toLowerCase().indexOf(term)){
							//if(pdbcount < 100){
							matches.push(choices[i].replace(' ','').replace(' ',''));
							suggest(matches);
							//pdbcount = 0;
							//}
						}
					}
				}
			}
		});
	}});

	$('#main_input').keypress(function(e){
		if(e.keyCode==13) {
			if( ! enterPressed){
				$('#send_data_main').click();
			}
			enterPressed = true;
			setTimeout(function(){
				enterPressed = false;
			}, 1000);
		}

	});

	$('#global_input').keypress(function(e){
		if(e.keyCode==13) {
			if( ! enterPressed){
				$('#global_button_search').click();
			}
			enterPressed = true;
			setTimeout(function(){
				enterPressed = false;
			}, 1000);
		}

	});



});

function changeInstructions(newInst) {
	$("#"+newInst).siblings().removeClass('active');
	$("#"+newInst).addClass('active');
}

function example_INPUT(){


	if (data_examples != null || data_examples.length > 0 ){
		$('#main_input').val(data_examples[Math.floor(Math.random()*data_examples.length)]);
	}else{
		var data = ['4zulF','4zhfC','4zgaA','4zajA','4wnjB'];
		$('#main_input').val(data[Math.floor(Math.random()*data.length)]);
	}

}

function move() {
	console.log("function called..");
	var elem = document.getElementById("myBar");   
	var width = 1;
	var id = setInterval(frame, 1200);

	items = ['#ccff99','#66d9ff','#99ff99','#ff6666','#ffff33'];
	function frame() {
		if (width >= 100) {
			clearInterval(id);
			$("#customAlerts").html("<div class='alert alert-success' role='alert'><strong>Calculating properties! </strong> Please wait a moment.</div><img src='slike/loadclique.gif' alt='Lscrean' style='width:200px;height:200px;'><div id='myProgress'><div id='myBar'></div></div>");
		} else {
			width++; 
			elem.style.width = width + '%';
			if (width % 5 == 0){

				$('#myBar').css('background-color', items[Math.floor(Math.random()*items.length)]);

			}
		}
		if (width >90){
			$("#customAlerts").html("<div class='alert alert-warning' role='alert'><strong>Warning! </strong> Entry will be recopmuted. This process can take up to 15min, depending on the server load. We kindly ask you to come back later (or stay on this page), when the entry is processed, you will obtain the result in a matter of seconds. </div><img src='slike/loadclique.gif' alt='Lscrean' style='width:200px;height:200px;'></div>");

		}
	}
}

function db_info(){

	$.ajax({

		url : url_dbinfo,
		type : 'GET',
		success : function(data) {              

			//console.log(data);
			var species = JSON.parse(data).species;
			if (species != {}){

				//create the table
				var summarytable = " <table class='table table-striped'><thead><tr><th>Species</th><th>Sequence Variant's count</th></tr></thead><tbody>";
				Object.keys(species).forEach(function (key) { 
					var value = species[key];
					summarytable += "<tr>"
						summarytable += "<td>"+key.replace("_"," ")+"</td>";
					summarytable += "<td>"+value+"</td>";
					summarytable += "</tr>"
				});

				summarytable += "</tbody></table>";
				$('#dbinfo_table').html(summarytable);
			}else{
				$('#dbinfo_table').html("<p>Currently no data is available </p>");
			}
		},
		error : function(request,error)
		{

			console.log("Examples not found..");


		}
	});

}

function draw_SNP_seqplot(selectedArray){
	
	var numBreaks = 0;
	var surf = molmil_snps["sequence_surf"];
	for (var key in selectedArray)
		numBreaks+=selectedArray[key].length;
	numBreaks = Math.floor(numBreaks/1.5);

	$('#output_seqplot').html("");

	// When data obtained, use the mapping to obtain new locations according to Uni mapping (this is not prone to errors!)

	var seqview = "<div id='seqplot' style='font-size: 1.2em;'><p class='song show-chords'>";
	lineCounter = 1;
	for (var key in selectedArray) {
		for (var j=0; j<selectedArray[key].length; j++) {
			var bsname = (selectedArray[key][j]).replace(" ","");
			seqview += "<span style='font-weight: bold; position: relative; bottom: -"+lineCounter*2+"em; display:inline-block; width: 0; overflow:visible; font-size: 10px;'>"+(bsname.slice(0,1).toUpperCase()+bsname.slice(1))+"</span>";
			lineCounter++;
		}

	}
	seqview += "<span style='font-weight: bold;' class='pdbSeq'>PDB:</span><span style='font-weight: bold;' class='uniprotSeq'>UniProt:</span><span style='font-size: 10px; padding-right: 45px; font-weight: bold;'>Sequence:</span>";
	var legendseq = "";
	var rcount = 0;
	var misslet = "ABCDEFGHIJKLMNOPRSTUVWZ";
	var maxdelay = 0;
	var ccount = 1;
	// iterate through obtained PDB entry, map SNPs according to increment, surface is supposed to be OK.

	//console.log(data['missing']);console.log(data['lo'][0]);console.log(absLoc;

	//NEW CODE
	for (var i=0; i<molmil_snps["sort"]["sequence_sorted_keys"].length; i++) {
		var count = molmil_snps["sort"]["sequence_sorted_keys"][i];
		var obj = molmil_snps["sequence"][count];
		var RSIDvalue = obj["uni"]||"NaN";
		//console.log(obj);
		//console.log(RSIDvalue);
		var location = obj["pdb"];
		var missing = obj["missing"];
		var deletion = obj["deleted"];
		var value = three2one[obj["respdb"]]||"X";
		
		var rgba = "";
		var lineCSS = "";
		var lineCounter = 0;
		var seqviewTemp = value;
		var title = "PDB: "+location+"\nUniProt: "+RSIDvalue;
		if (obj["respdb"] != obj["resn"] && obj["resn"]!=undefined)
			title = "Missmatch with UniProt "+obj["resn"]+"\n\n"+title;
		
		if (missing) {
			rgba += " background-color: rgba(211,211,211,0.60);";
			seqviewTemp = "<span  style='font-family:monospace; "+rgba+"' data-toggle='tooltip'>"+seqviewTemp+"</span>";
		}

		if ((Object.keys(surf)).indexOf(location) != -1) {
			if (obj["snp"]) {
				title += "\n\nAccession numbers:";
				try{
					for (var j=0; j<molmil_snps["locus"][RSIDvalue]["snps"].length; j++) {
						var snp = molmil_snps["snps"][(molmil_snps["locus"][RSIDvalue]["snps"])[j]];
						title += "\n "+snp["id"]+" ("+snp["change"].slice(snp["change"].length-3)+")";
					}}
				catch(e) {
					try {
					for (var j=0; j<(molmil_snps["missing"][RSIDvalue]).length; j++) {
						var snp = ((molmil_snps["missing"][RSIDvalue])[j][1]);
						title += "\n "+snp["id"]+" ("+snp["change"].slice(snp["change"].length-3)+")";
					}}
					catch(e) {
						console.log("something went wrong");}
				}
				for (var key in selectedArray) {
					for (var j=0; j<(selectedArray[key]).length; j++) {
						if ((surf[location]).indexOf((selectedArray[key][j])) != -1) {
							var lineheight = lineCounter+1;
							var padding = lineCounter+0.6;
							//var rgbaLine = "rgba(255, 0, 0, 1)";
							var rgbaSurface = colorScheme.seqLegend[key.toLowerCase()];
							var percentage = Math.floor(26/(2+padding));
							//lineCSS = "padding-bottom: "+padding+"em; background: linear-gradient(to top, "+rgbaSurface+" "+percentage+"%, rgb(255,255,255) "+percentage+"%); background-position: 0 100%;";
							lineCSS = "padding-bottom: "+padding+"em; border-bottom: 4px solid "+rgbaSurface+";" ;
							seqviewTemp = "<span  style='font-family:monospace; "+rgba+" color: rgba(255, 0, 0, 1); "+lineCSS+"' data-toggle='tooltip' title='"+title+"'>"+seqviewTemp+"</span>";
						}
						lineCounter++;
					}
				}
				if (lineCSS.length == 0) {
					var rgbaSurface = colorScheme.SNP;
					rgba += "color: rgba("+rgbaSurface[0]+", "+rgbaSurface[1]+", "+rgbaSurface[2]+", 1)";
					seqviewTemp = "<span  style='font-family:monospace; "+rgba+"' data-toggle='tooltip' title='"+title+"'>"+seqviewTemp+"</span>";
				}
				seqviewTemp = seqviewTemp.replace("span", "span onClick = \"SequenceZoom('"+RSIDvalue+"');\"");
				seqviewTemp = seqviewTemp.replace("style='", "style='cursor: pointer; ");
			}
			else {
				for (var key in selectedArray) {
					for (var j=0; j<(selectedArray[key]).length; j++) {
						if ((surf[location]).indexOf((selectedArray[key][j])) != -1) {
							var lineheight = lineCounter+1;
							var padding = lineCounter+0.6;
							var percentage = Math.floor(26/(2+padding));
							var rgbaSurface = colorScheme.seqLegend[key.toLowerCase()];
							//lineCSS = "padding-bottom: "+padding+"em; background: linear-gradient(to top, "+rgbaSurface+" "+percentage+"%, rgb(255,255,255) "+percentage+"%); background-position: 0 100%;";
							lineCSS = "padding-bottom: "+padding+"em; border-bottom: 4px solid "+rgbaSurface+";" ;
							seqviewTemp = "<span  style='font-family:monospace; "+rgba+" "+lineCSS+"' data-toggle='tooltip' title='"+title+"'>"+seqviewTemp+"</span>";
						}
						lineCounter++;
					}
				}
				if (lineCSS.length == 0) {
					var rgbaSurface = colorScheme.SNP;
					rgba += "color: black";
					seqviewTemp = "<span  style='font-family:monospace; "+rgba+"' data-toggle='tooltip' title='"+title+"'>"+seqviewTemp+"</span>";
				}
			}
		}
		else if (obj["snp"]) {
			title += "\n\nAccession numbers:";
			try{
				for (var j=0; j<molmil_snps["locus"][RSIDvalue]["snps"].length; j++) {
					var snp = molmil_snps["snps"][(molmil_snps["locus"][RSIDvalue]["snps"])[j]];
					title += "\n "+snp["id"]+" ("+snp["change"].slice(snp["change"].length-3)+")";
				}
			}
			catch(e) {
				try {
					for (var j=0; j<(molmil_snps["missing"][RSIDvalue]).length; j++) {
						var snp = ((molmil_snps["missing"][RSIDvalue])[j][1]);
						title += "\n "+snp["id"]+" ("+snp["change"].slice(snp["change"].length-3)+")";
					}}
				catch(e) {
					console.log("something went wrong2");}
			}
			var rgbaSurface = colorScheme.SNP;
			rgba += "color: rgba("+rgbaSurface[0]+", "+rgbaSurface[1]+", "+rgbaSurface[2]+", 1)";
			seqviewTemp = "<span  style='font-family:monospace; "+rgba+"' data-toggle='tooltip' title='"+title+"'>"+seqviewTemp+"</span>";
			if (!missing && !deletion) {
				seqviewTemp = seqviewTemp.replace("span", "span onClick = \"SequenceZoom('"+RSIDvalue+"');\"");
				seqviewTemp = seqviewTemp.replace("style='", "style='cursor: pointer; ");
			}
		}
		else {
			seqviewTemp = "<span  style='font-family:monospace; "+rgba+"' data-toggle='tooltip' title='"+title+"'>"+seqviewTemp+"</span>";
		}

		if (location%10 == 0) {
			seqviewTemp = "<span class='pdbSeq'>"+location+"</span>"+seqviewTemp;
			seqviewTemp = "<span class='uniprotSeq'>"+RSIDvalue+"</span>"+seqviewTemp;
		}		

		seqview += seqviewTemp;
		//break lines
		if (ccount % 50 == 0 && ccount > 0){
			seqview += "<br><br><br><br>";
			for (var j=0; j<numBreaks; j++) {
				seqview += "<br>";
			}
			lineCounter = 1;
			for (var key in selectedArray) {
				for (var j=0; j<selectedArray[key].length; j++) {
					var bsname = (selectedArray[key][j]).replace(" ","");
					seqview += "<span style='font-weight: bold; position: relative; bottom: -"+lineCounter*2+"em; display:inline-block; width: 0; overflow:visible; font-size: 10px;'>"+(bsname.slice(0,1).toUpperCase()+bsname.slice(1))+"</span>";
					lineCounter++;
				}

			}

			seqview += "<span style='font-weight: bold;' class='pdbSeq'>PDB:</span><span style='font-weight: bold;' class='uniprotSeq'>UniProt:</span><span style='font-size: 10px; padding-right: 45px; font-weight: bold;'>Sequence:</span>";

		}
		ccount++;
		
	}

	seqview += "<br><br><br>";
	for (var i=0; i<numBreaks; i++)
		seqview += "<br>";

	seqview += "</p></div>";
	legendseq += "<div><p><strong>Legend:</strong>";
	legendseq += "<br><span style='color:red;'>Sequence Variant within binding site</span><br>";
	var rgbaSurface = colorScheme.SNP;
	legendseq += "<span style='color: rgb("+rgbaSurface[0]+", "+rgbaSurface[1]+", "+rgbaSurface[2]+");'> Sequence Variant not in binding site</span><br>";
	for (key in selectedArray) {
		if (selectedArray[key].length > 0) {
			var rgbaSurface = colorScheme.seqLegend[key];
			legendseq += "<span style='color: "+rgbaSurface+";'>"+key.slice(0,1).toUpperCase()+key.slice(1)+" binding site</span><br>";
		}
	}
	legendseq += "<span style='color:rgba(211,211,211,0.99);'> Missing in crystal structure</span><br>";

	legendseq += "</p></div>";

	legendseq += "<br>";

	$('#snp_seqplot').html(seqview);
	$('#snp_seqplot_legend').html(legendseq);
	$('#output_seqplot').html("");
	
//	$("#btnSave").unbind().click(function() {
//
//		document.getElementById('hidden_div').innerHTML = document.getElementById('screen_seqplot').innerHTML;
//		document.getElementById('hidden_div').style.display = "block";
//		document.getElementById('btnSave').innerHTML = "Creating Image";
//		
//		var x = $("#screen_seqplot").outerWidth(true);
//		$("#hidden_div").css('width', x + 'px');
//		
////	  $("#hidden_div").css({
////	    'width': ($("#screen_seqplot").outerWidth(true) + 'px')
////	  });
//	  
//		//$("#hidden_div").css("width") = $('#screen_seqplot').outerWidth(true);
//
//		//document.getElementById('hidden_div').style.width = document.getElementById('screen_seqplot').offsetWidth;
//
//		//console.log($('#hidden_div').outerHeight(true));
//		 
//		try{
//			html2canvas(document.getElementById('hidden_div'), {
//				//height: $('#hidden_div').outerHeight(true),
//				width: $("#hidden_div").width(),
//				taintTest: false,
//				onrendered: function(canvas) {
//					console.log("on done do this");
//					var image = canvas.toDataURL("image/png").replace(/^data:image\/png/,'data:application/octet-stream'+'.png');  // here is the most important part because if you dont replace you will get a DOM 18 exception.
//					window.location.download = 'your_pic_name.png';
//					window.location.href=image; // it will save locally
//					document.getElementById('hidden_div').style.display = "none";
//					document.getElementById('hidden_div').innerHTML = "";
//					document.getElementById('btnSave').innerHTML = "Save as image";
//
//				}
//			});}
//		catch(e) {console.log(e);}
//
//	});
//
//}

	$("#btnSave").unbind().click(function() {

		document.getElementById('hidden_div').innerHTML = document.getElementById('screen_seqplot').innerHTML;
		document.getElementById('hidden_div').style.display = "block";
		document.getElementById('btnSave').value = "Creating Image";
		document.getElementById('btnSave').disabled = true;
		
		var x = $("#screen_seqplot").outerWidth(true);
		$("#hidden_div").css('width', x + 'px');
		
//	  $("#hidden_div").css({
//	    'width': ($("#screen_seqplot").outerWidth(true) + 'px')
//	  });
	  
		//$("#hidden_div").css("width") = $('#screen_seqplot').outerWidth(true);

		//document.getElementById('hidden_div').style.width = document.getElementById('screen_seqplot').offsetWidth;

		//console.log($('#hidden_div').outerHeight(true));
		 
		try{
			html2canvas(document.getElementById('hidden_div'), {
				//height: $('#hidden_div').outerHeight(true),
				width: $("#hidden_div").width(),
				taintTest: false,
				onrendered: function(canvas) {
			    $("#download-canvas").prop("href",canvas.toDataURL("image/png"));  // here is the most important part because if you dont replace you will get a DOM 18 exception.;
					document.getElementById('download-canvas').click();
					document.getElementById('hidden_div').style.display = "none";
					document.getElementById('hidden_div').innerHTML = "";
					document.getElementById('btnSave').value = "Save as image**";
					document.getElementById('btnSave').disabled = false;

				}
			});}
		catch(e) {console.log(e);}

	});

}

function reverse(s) {
	return s.split('').reverse().join('');
}

function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}


function highlight (object){


	if (object.id == "unikeyup"){
		var src_str = $(object).text();

		console.log(src_str);
		var term = object.value;

		src_str = src_str


		$('#unilabel').html("<label for='unilabel'>UniProt's sequence for query PDB (length: "+src_str.length+" AA)</label><textarea rows='5' cols='35' id='unilabel'>"+src_str+"</textarea>");


	}else if (object.id == "pdbkeyup"){
		var src_str = $(object).text();
		var term = object.value;
		src_str = src_str

		$('#unilabel').html("<label for='pdblabel'>PDB's sequence (length: "+src_str.length+" AA)</label><textarea rows='5' cols='35'id='pdblabel'>"+src_str+"</textarea>");
	}

}

function changeSequence(buttonInfo) {
	if (buttonInfo != null) {
		if (buttonInfo.classList.contains("active"))
			buttonInfo.classList.remove("active");
		else
			buttonInfo.classList.add("active");
	}
	var selected = document.getElementById("checkboxes").getElementsByClassName("active");
	var selectedArray = {"compound": [], "ion": [], "nucleic": [], "protein": []};
	//var selected = $("#checkboxes").find('input:checkbox:checked');
	for (var i=0; i<selected.length; i++) {
		var value = $(selected[i]).attr("value");
		selectedArray[(value.split(" "))[0]].push(value);
	}
	draw_SNP_seqplot(selectedArray);
}

function copyToClipboard() {
	window.prompt("Copy to clipboard:", "http://genprobis.insilab.org/?q="+$("#myUL li:first").text());
	
}

function SequenceZoom(element) {
	var snp = molmil_snps["snps"][molmil_snps["locus"][element]["snps"][0]];
	var mol = canvas.molmilViewer.structures[0].chains[snp["chain"]].molecules[snp["mol"]];
	
	try {
		molmil.selectAtoms(mol.atoms, false, canvas.molmilViewer);  
		canvas.molmilViewer.gotoMol(mol);
		//molmil.selectionFocus(canvas.molmilViewer); 
		canvas.renderer.initBuffers(surface_type);
		canvas.update = true;
		
		molmil_snps["currSel"] = mol.atoms;
		
		var elements = document.getElementsByClassName("SNPTableButton");
		for (var i = 0; i < elements.length; i++) {
			if (elements[i].id.split("_")[1] == "Zoom on")
				elements[i].removeClass("active");
		}
		
		var elements = document.getElementsByClassName("SNPTableButton");
		for (var i = 0; i < elements.length; i++) {
			if (elements[i].id.split("_")[1] == "Zoom on")
				elements[i].removeClass("active");
		}		
		
	}
	catch(e) {
		console.log("unable to zoom");
	}
}

//function download(text, name, type) {
//var a = document.getElementById("a");
//var file = new Blob([text], {type: type});
//a.href = URL.createObjectURL(file);
//a.download = name;
//}
