 var molmil_settings = {
      src: "",
    };
 
 function onResizeMolmil() {
		var dpr = devicePixelRatio || 1;
		canvas.width = (window.innerWidth || document.documentElement.clientWidth)*dpr;
		canvas.height = (window.innerHeight || document.documentElement.clientHeight)*dpr;
		canvas.style.width = String(window.innerWidth || document.documentElement.clientWidth)+"px";
		canvas.style.height = String(window.innerHeight || document.documentElement.clientHeight)+"px";
		canvas.renderer.resizeViewPort();
		canvas.update = true;
		
		//console.log("on resize");
		//changeSequence();
		};
  
var canvas, cli;
  
function initViewer() {
	if (! window.molmil.configBox || ! molmil.configBox.initFinished) {return setTimeout(initViewer, 100);}
      
	canvas = document.getElementById("molmilViewer");

	canvas.width = (window.innerWidth || document.documentElement.clientWidth);
	canvas.height = (window.innerHeight || document.documentElement.clientHeight);
	canvas = molmil.createViewer(canvas);
	
	window.onresize = onResizeMolmil;
		
	molmil.configBox.BGCOLOR = [0,0,0,1];
		
	}