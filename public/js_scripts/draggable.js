var isDragged = false;

var ligandProteinColor=0;

//stops onclick action if menu is dragged
function dragged(event){
	isDragged = true;
}

//Close the dropdown if the user clicks outside of it
window.onclick = function(e) {
	if (!e.target.matches(".dropdown-toggle")) {

		var dropdowns = document.getElementsByClassName("dropdown-menu");
		for (var d = 0; d < dropdowns.length; d++) {
			var openDropdown = dropdowns[d];
			if (openDropdown.classList.contains('show')) {
				openDropdown.classList.remove('show');
			}
		}
	}
}

//recursive function for making menu and submenues
var makeMenu = function(sez,ul) {
	var li, t, a, at;

	var ul_id = ul.attr("id");

	if (ul_id == "Zoom on") {
		sez.unshift({href: "#", text: "Reset", dropdown: false, drops: []});
	}

	for (var i=0; i<sez.length; i++) {
		li = document.createElement("LI");
		a = document.createElement("A");
		t = sez[i].text;
		at = sez[i].href;
		$(a).attr("href", at);

		if (sez[i].dropdown) {
			$(li).addClass("dropdown");
			$(a).addClass("dropdown-toggle");
			$(a).attr("data-toggle", "dropdown");
			$(a).attr("id", sez[i].text);
			$(a).append(t);

			var span = document.createElement("span");
			$(span).addClass("caret");
			$(a).append(span);

			$(li).append(a);

			var ul2 = document.createElement("UL");
			$(ul2).attr("id", sez[i].text);
			$(ul2).addClass("dropdown-menu");

			//$(a).attr("onclick", "dropMenu('"+sez[i].text+"')");

			makeMenu(sez[i].drops, $(ul2));

			$(li).append(ul2);
		}

		else {
			$(a).append(t);
			$(li).append(a);
		}   

		if (ul_id == "myUL") {
			$(a).attr("id", "a_"+((t == "Sequence Variants") ? "SNPs" : t));
		}
		else {
			$(a).attr("id", "a_"+ul_id+"_"+sez[i].text);
			$(a).addClass("options");
		}

		ul.append(li);
	} 
}

//construction of menues and submenues
class Item {
	constructor(pdbid, snpsurfnam) {
		this.$element = $(document.createElement("div"));
		this.$element.addClass("item");
		$(this.$element).attr("id", "draggable");
		$(this.$element).attr("style", "position: absolute");
		$(this.$element).attr("ondrag", "dragged(event)");

		var sez = [{href: "#", text: pdbid, dropdown: false, drops: []},
		           {href: "#", text: "Color", dropdown: true, drops:[
		                                                             {href: "#", text: "Structure", dropdown: false, drops: []},
		                                                             {href: "#", text: "CPK", dropdown: false, drops: []},
		                                                             {href: "#", text: "Group", dropdown: false, drops: []}, //v probisu ni
		                                                             {href: "#", text: "Custom", dropdown: false, drops: []}
		                                                             ]},
 		                                                             {href: "#", text: "Style", dropdown: true, drops:[
		                                                                                                               {href: "#", text: "Cartoon", dropdown: false, drops: []},
		                                                                                                               {href: "#", text: "Spacefill", dropdown: false, drops: []}, //spheres
		                                                                                                               {href: "#", text: "BallStick", dropdown: false, drops: []}, //balls and sticks
		                                                                                                               {href: "#", text: "Stick", dropdown: false, drops: []},
		                                                                                                               {href: "#", text: "Wireframe", dropdown: false, drops: []}, //lines
		                                                                                                               {href: "#", text: "CaTrace", dropdown: false, drops: []}, //backbone
		                                                                                                               {href: "#", text: "Tube", dropdown: false, drops: []}, //ribbon
		                                                                                                               {href: "#", text: "CartoonRocket", dropdown: false, drops: []}
		                                                                                                               ]}

		                                                             //{href: "#", text: "Toggle", dropdown: true, drops:[
		                                                             //{href: "#", text: "Waters", dropdown: false, drops: []},
		                                                             //{href: "#", text: "Hydrogens", dropdown: false, drops: []}
		                                                             //]}
		                                                             ];

		var snps_drops = []
		for (var key in molmil_snps["locus"]) {
			if (molmil_snps["locus"].hasOwnProperty(key)) {
				snps_drops.push({href: "#", text: molmil_snps["locus"][key]["snps"][0], dropdown: false, drops: []});
			}
		}

		var surface_drops = [
		                     {href: "#", text: "Waters", dropdown: false, drops: []},
		                     {href: "#", text: "Hydrogens", dropdown: false, drops: []}];
		
		for (var key in molmil_snps["ligands"])
			surface_drops.push({href: "#", text: key, dropdown: false, drops: []});

		sez.push({href: "#", text: "Display", dropdown: true, drops:surface_drops});

		sez.push({href: "#", text: "Sequence Variants", dropdown: false, drops: []});

		sez.push({href: "#", text: "Focus", dropdown: false, drops:[]});

		//sez.push({href: "#", text: "Save as", dropdown: true, drops: [{href: "#", text: "png", dropdown: false, drops: []}]});

		var ul = document.createElement("UL");
		$(ul).attr("id", "myUL");
		this.$element.append(ul);
		makeMenu(sez, $(ul));

	}
}

class Table {
	constructor() {
		this.$element = $(document.createElement("div"));
		this.$element.addClass("draggableTable");
		$(this.$element).attr("id", "draggableTable");
		$(this.$element).attr("style", "position: absolute; display: none; max-height: 283px; min-width: 400px");
		$(this.$element).attr("ondrag", "dragged(event)");

		var list =  [];
		for (var key in molmil_snps["locus"]) {
			if (molmil_snps["locus"].hasOwnProperty(key)) {
				list.push(molmil_snps["locus"][key]["snps"][0]);
			}
		}
		//var list = ["snp1", "snp2", "snp3", "snp4", "snp5", "snp6"];

		var d, c, r, t;
		d = $(document.createElement("div"));
		$(d).attr("onmousedown", "(function(){$('#draggableTable').draggable({ disabled: true});})();");
		$(d).attr("onmouseup", "(function(){$('#draggableTable').draggable('enable');})();");
		$(d).attr("style", "max-height: 200px; overflow: auto;");
		t = document.createElement('table');
		$(t).attr("id", "draggableTableCont");
		var no_el = list.length;
		var no_cols = 5;
		var no_rows = Math.ceil(no_el/no_cols);
		for (var i=0; i<no_rows; i++) {
			r = t.insertRow(i);
			for (var j=0; j<no_cols; j++) {
				var templist = "";
				if (list[j+i*no_cols] != 0)
					templist += (list[j+i*no_cols])||"";
				else
					templist += "0";
				if (templist != "") templist = molmil_snps["snps"][templist]["id"];
				templist += "<br>";   		
				if (templist != "<br>") {
					c = r.insertCell(j);
					$(c).attr("id", "draggableTable_cell_"+list[j+i*no_cols]);
					$(c).attr("class", "draggableTableCell");
					templist += "<input type='button' id='a_SNPs_"+list[j+i*no_cols]+"' class='button SNPTableButton active' value='H' />";
					if (molmil_snps["snps"][list[j+i*no_cols]]["label_no"] != null)
						templist += "<input type='button' id='a_Label_"+list[j+i*no_cols]+"' class='button SNPTableButton' value='L'/>";
					templist += "<input type='button' id='a_Zoom on_"+list[j+i*no_cols]+"' class='button SNPTableButton' value='Z'/>";
					c.innerHTML = templist;
				}
			}
		}
		this.$element.append("<a class='closebtnDraggableTable' href='javascript:void(0)' onclick='closeDraggableTable()'>×</a>");
		this.$element.append("<input type='button' style='opacity: 0.8;'  class='button DraggableLegendButton' value='S' /><input type='button' style='opacity: 0.8;' class='button DraggableLegendButton active' value='H' /> Show/Hide Sequence Variant");
		this.$element.append("<input type='button' style='opacity: 0.8;' class='button DraggableLegendButton' value='L' /><input type='button' style='opacity: 0.8;' class='button DraggableLegendButton active' value='L' /> Show/Hide label<br>");
		this.$element.append("<input type='button' style='opacity: 0.8;' class='button DraggableLegendButton' value='Z' /><input type='button' style='opacity: 0.8;' class='button DraggableLegendButton active' value='Z' /> Zoom in/Zoom out<br><br>");
		$(d).append(t);
		this.$element.append(d);

	}
}

class LigandPlayer {
	constructor(ligandName) {
		this.$element = $(document.createElement("div"));
		this.$element.addClass("ligandPlayer");
		$(this.$element).attr("id", "draggableLigandPlayer");
		$(this.$element).attr("style", "position: absolute; display: none; width: 280px");
		$(this.$element).attr("ondrag", "dragged(event)");

		var ul = document.createElement("UL");
		$(ul).attr("id", "ligandPlayerList");
		this.$element.append(ul);

		var li, t, a, at;

		li = document.createElement("LI");
		a = document.createElement("A");
		t = ligandName;
		$(a).attr("href", "#");
		$(a).attr("style", "width: 100px; padding-left: 0px; padding-right: 0px; text-align: center;");
		$(a).append(t);
		$(li).append(a);
		$(ul).append(li);

		li = document.createElement("LI");
		a = document.createElement("A");
		t = "<span class='glyphicon glyphicon-backward'></span>";
		$(a).attr("href", "#");
		$(a).attr("id", "ligandPlayer_BW");
		$(a).append(t);
		$(li).append(a);
		$(ul).append(li);

		li = document.createElement("LI");
		a = document.createElement("A");
		t = "<span class='glyphicon glyphicon-play'></span>";
		$(a).attr("href", "#");
		$(a).attr("id", "ligandPlayer_PLAY");
		$(a).append(t);
		$(li).append(a);
		$(ul).append(li);

		li = document.createElement("LI");
		a = document.createElement("A");
		t = "<span class='glyphicon glyphicon-stop'></span>";
		$(a).attr("href", "#");
		$(a).attr("id", "ligandPlayer_STOP");
		$(a).append(t);
		$(li).append(a);
		$(ul).append(li);

		li = document.createElement("LI");
		a = document.createElement("A");
		t = "<span class='glyphicon glyphicon-forward'></span>";
		$(a).attr("href", "#");
		$(a).attr("id", "ligandPlayer_FW");
		$(a).append(t);
		$(li).append(a);
		$(ul).append(li);

		li = document.createElement("LI");
		a = document.createElement("A");
		t = "<span class='glyphicon glyphicon-remove'></span>";
		$(a).attr("href", "#");
		$(a).attr("id", "ligandPlayer_EXIT");
		$(a).attr("onclick", "closeLigandPlayer();");
		$(a).append(t);
		$(li).append(a);
		$(ul).append(li);

	}
}

//function to construct whole menu
class Menu {
	constructor(menu) {
		this.$element = $(menu);
		this.size = 0;
	}

	add(item) {
		item.$element.draggable({containment: 'body', scroll: false});
		//item.$element.draggable({ disabled: true , containment: 'body', scroll: false});
		this.$element.before(item.$element);     
	}  
}


//--------------------------------------ACTIONS------------------------------------------
var snp_rgba = [179,67,146,255]; //color of SNPs
var snp_rgba_hover = [0,288,255]; //border color of SNP when hovering over it in the draggable submenues

function hideLigand(name) {
	var i = 1;
	while (i<canvas.molmilViewer.structures.length) {
		if ((name.toUpperCase()).indexOf(canvas.molmilViewer.structures[i].meta.id.toUpperCase()) != -1) {
			canvas.molmilViewer.structures.splice(i, 1);
		}
		else 
			i += 1;
	}
	for (var i=0; i<ligandObject.length; i++) {
		if (((ligandObject[i].lig_id).toUpperCase()).indexOf(name.toUpperCase()) != -1) {
			ligandObject[i]["visible"] = false;
		}
	}
}

function showLigand(i,name,number) {
	if (number != null)
		$($("#ligandPlayerList").children()[0]).children()[0].innerHTML = (ligandObject[i].lig_id).split("_")[4]+" "+number;
	canvas.molmilViewer.load_PDB(ligandObject[i].pdb_file, name, false);
	if (ligandObject[i].ltype == "ion") {
		molmil.displayEntry(canvas.molmilViewer.structures[canvas.molmilViewer.structures.length-1], molmil.displayMode_Spacefill);
		molmil.colorEntry(canvas.molmilViewer.structures[canvas.molmilViewer.structures.length-1], 1, null, false, canvas.molmilViewer);
	}
	else if (ligandObject[i].ltype == "protein") {
		molmil.displayEntry(canvas.molmilViewer.structures[canvas.molmilViewer.structures.length-1], molmil.displayMode_Spacefill);
		molmil.colorEntry(canvas.molmilViewer.structures[canvas.molmilViewer.structures.length-1],molmil.colorEntry_Custom,[255,ligandProteinColor,0,255], false, canvas.molmilViewer);
		ligandProteinColor = (ligandProteinColor+30)%255;
	}
	else {
		molmil.displayEntry(canvas.molmilViewer.structures[canvas.molmilViewer.structures.length-1], 1);
		molmil.colorEntry(canvas.molmilViewer.structures[canvas.molmilViewer.structures.length-1], 1, null, false, canvas.molmilViewer);
	}

	ligandObject[i]["visible"] = true;

	currentLigandIndex = i;

}

function calcNewLigNo(add) {
	var currOf = ($($("#ligandPlayerList").children()[0]).children()[0].innerHTML.split(" ")[1]).split("/");
	var currNo = parseInt(currOf[0].substring(1));
	var len = parseInt(currOf[1].substring(0,currOf[1].length-1));
	if (add == "restart")
		newNo = 1;
	else if (add) {
		var newNo = currNo+1;
		if (newNo>len)
			newNo = 1;
	}
	else {
		var newNo = currNo-1;
		if (newNo<1)
			newNo = len;
	}
	newNo = "("+newNo+"/"+len+")";
	return newNo;
}

function showSurfaceSticks(selected) {
	for (var key in selected) {				
		if (selected.hasOwnProperty(key)) {
			var chain = canvas.molmilViewer.structures[0].chains[key];
			for (var key2 in selected[key]) {
				if (selected[key].hasOwnProperty(key2)) {
					if (selected[key][key2].length > 0) {
						for (var m=0; m<selected[key][key2].length; m++) {
							if (key2.substr(key2.length-3) != "SNP") {
								var mol = chain.molecules[selected[key][key2][m]];
								molmil.colorEntry(mol,molmil.colorEntry_Custom, [0,255,0,255], false, molmilViewer);
								molmil.displayEntry(canvas.molmilViewer.structures[0].chains[key].molecules[selected[key][key2][m]], molmil.displayMode_Stick, false, canvas.molmilViewer);
							}
						}
					}
				}					
			}						
		}
	}
}

function showSurfaceSNPs(selected) {
	for (var key in selected) {				
		if (selected.hasOwnProperty(key)) {
			var chain = canvas.molmilViewer.structures[0].chains[key];
			for (var key2 in selected[key]) {
				if (selected[key].hasOwnProperty(key2)) {
					if (selected[key][key2].length > 0) {
						for (var m=0; m<selected[key][key2].length; m++) {
							if (key2.substr(key2.length-3) == "SNP") {
								for (var m=0; m<selected[key][key2].length; m++) {
									var mol = chain.molecules[selected[key][key2][m]];
									//molmil_snps["locus"][seqToRSID[mol.RSID]]["style"] = "Stick";
									molmil_snps["locus"][seqToRSID[mol.RSID]]["inSurface"] = true;
									if (molmil_snps["locus"][seqToRSID[mol.RSID]]["visible"]) {
										molmil.colorEntry(mol,molmil.colorEntry_Custom, [255,0,0,255], false, molmilViewer);
										molmil.displayEntry(canvas.molmilViewer.structures[0].chains[key].molecules[selected[key][key2][m]], molmil.displayMode_BallStick, false, canvas.molmilViewer);
									}
								}
							}
						}
					}
				}					
			}						
		}
	}
}

function hideSurfaceSticks(selected) {
	var color = $("#Color .active").attr('id').split("_")[2];
	var style = $("#Style .active").attr('id').split("_")[2];

	if (color == "Custom")
		var colorRGBA = molmil_snps["locus"][Object.keys(molmil_snps["locus"])[0]]["color"];

	for (var key in selected) {				
		if (selected.hasOwnProperty(key)) {
			var chain = canvas.molmilViewer.structures[0].chains[key];
			for (var key2 in selected[key]) {
				if (selected[key].hasOwnProperty(key2)) {
					if (selected[key][key2].length > 0) {
						if (key2.substr(key2.length-3) != "SNP") {
							for (var m=0; m<selected[key][key2].length; m++) {
								var mol = chain.molecules[selected[key][key2][m]];
								if (color == "Custom")
									molmil.colorEntry(mol,molmil.colorEntry_Custom, colorRGBA, false, molmilViewer);
								else
									molmil.colorEntry(mol,eval("molmil.colorEntry_"+color), null, false, molmilViewer);
								molmil.displayEntry(mol, eval("molmil.displayMode_"+style), false, canvas.molmilViewer);
							}
						}
					}
				}					
			}						
		}
	}
}

function hideSurfaceSNPs(selected) {
	var color = $("#Color .active").attr('id').split("_")[2];
	var style = $("#Style .active").attr('id').split("_")[2];

	if (color == "Custom")
		var colorRGBA = molmil_snps["locus"][Object.keys(molmil_snps["locus"])[0]]["color"];

	for (var key in selected) {				
		if (selected.hasOwnProperty(key)) {
			var chain = canvas.molmilViewer.structures[0].chains[key];
			for (var key2 in selected[key]) {
				if (selected[key].hasOwnProperty(key2)) {
					if (selected[key][key2].length > 0) {
						if (key2.substr(key2.length-3) == "SNP") {
							for (var m=0; m<selected[key][key2].length; m++) {
								var mol = chain.molecules[selected[key][key2][m]];
								//molmil_snps["locus"][seqToRSID[mol.RSID]]["style"] = "Default";
								molmil_snps["locus"][seqToRSID[mol.RSID]]["inSurface"] = false;
								if (molmil_snps["locus"][seqToRSID[mol.RSID]]["visible"]) {
									molmil.colorEntry(mol,molmil.colorEntry_Custom, colorScheme["SNP"], false, molmilViewer);
									molmil.displayEntry(canvas.molmilViewer.structures[0].chains[key].molecules[selected[key][key2][m]], molmil.displayMode_BallStick, false, canvas.molmilViewer);
								}
								else {
									if (color == "Custom")
										molmil.colorEntry(mol,molmil.colorEntry_Custom, colorRGBA, false, molmilViewer);
									else
										molmil.colorEntry(mol,eval("molmil.colorEntry_"+color), null, false, molmilViewer);
									molmil.displayEntry(mol, eval("molmil.displayMode_"+style), false, canvas.molmilViewer);
								}									
							}
						}
					}
				}					
			}						
		}
	}
}

//on click actions
$('body').on('click', '.options, #a_SNPs, .SNPTableButton, #image_Save, #a_Focus', function(){ 
	var id = $(this).attr('id');

	function color_snps(){

		var molecules = [];
		for (key in molmil_snps["snps"]) {
			var snp = molmil_snps["snps"][key];
			var mol = canvas.molmilViewer.structures[0].chains[snp["chain"]].molecules[snp["mol"]];
			molmil_snps["locus"][snp["locus"]]["color"] = mol.rgba;
			if (snp["visible"]) {
				molecules.push(mol);
			}
		}		
		molmil.colorEntry(molecules, molmil.colorEntry_Custom, snp_rgba, false, molmilViewer);


		if (surface_type == true) {
			var selected = molmil_surface["current"];
			showSurfaceSticks(selected);
			showSurfaceSNPs(selected);
		}
		else if (surface_type)
			showSurfaceSNPs(molmil_surface["current"]);
		canvas.renderer.initBuffers(surface_type);
		canvas.update = true;
	}

	if (id == "a_SNPs") {
		$('#draggableTable').toggle();
		var width = Math.round(1.05*document.getElementById("draggableTableCont").clientWidth);
		$('#draggableTable').css({"width": width});
	}

	else {
		id = id.split("_");
		var obj = canvas.molmilViewer.structures[0];
		//var obj_surface = canvas.molmilViewer.structures[1];

		var uinput = $('#main_input').val();

		var list = [];

		for (var i=0; i<chainsList.length; i++) {
			list.push(obj.chains[chainsList[i]]);}	

		if (id[1] == "Color") {

			//function for coloring snps after the whole chain has been colored; needed for callback!

			if (id[2] == "Custom") {
				obj.ref = true;
				obj.GenProBis = true;
				canvas.molmilViewer.UI.colorEntry(obj, 6, function (rgba, callback = color_snps) {new molmil.colorEntry(list, eval("molmil.colorEntry_"+id[2]), rgba, false, canvas.molmilViewer); callback();});
			}
			else {
				molmil.colorEntry(list, eval("molmil.colorEntry_"+id[2]), null, false, molmilViewer);
				color_snps();
			}

			molmil_surface["color"] = id[2];

			$("#Color li a").removeClass('active');	 
			$("#"+$(this).attr('id')).addClass('active');

			document.getElementById(id[1]).classList.toggle("show");

			//oldData = $('#draggable').html();

		}

		else if (id[1] == "Style") {

			molmil.displayEntry(list, eval("molmil.displayMode_"+id[2]), false, canvas.molmilViewer);

			for (var key in molmil_snps["snps"]) {
				var snp = molmil_snps["snps"][key];
				var mol = canvas.molmilViewer.structures[0].chains[snp["chain"]].molecules[snp["mol"]];
				if (!snp["in_surface"]) molmil_snps["locus"][snp["locus"]]["style"] = id[2];
				if (snp["visible"]) {
					for (a=0; a<mol.atoms.length; a++) mol.atoms[a].displayMode = 2;
				}
				mol.showSC = true;
				mol.chain.twoDcache = null;	
			}

			if (surface_type == true) {
				showSurfaceSticks(molmil_surface["current"]);
				showSurfaceSNPs(molmil_surface["current"]);
			}
			else if (surface_type)
				showSurfaceSNPs(molmil_surface["current"]);

			canvas.renderer.initBuffers(surface_type);
			canvas.update = true;

			clicked = true;

			$("#Style li a").removeClass('active');	 
			$("#"+$(this).attr('id')).addClass('active');

			document.getElementById(id[1]).classList.toggle("show");

			//oldData = $('#draggable').html();
		}

		else if (id[1] == "Display" && id[2] != "Surface") {
			if (id[2] == "Waters" || id[2] == "Hydrogens") {
				if (eval("canvas.molmilViewer.hide"+id[2])) {
					eval("canvas.molmilViewer."+id[2].toLowerCase().slice(0, -1)+"Toggle(true)");	
					$("#"+$(this).attr('id')).addClass("active"); 
				}
				else {
					eval("canvas.molmilViewer."+id[2].toLowerCase().slice(0, -1)+"Toggle(false)");
					$("#"+$(this).attr('id')).removeClass("active");
				}
			}
			else {
				if ((Object.keys(molmil_snps["ligands"])).indexOf(id[2]) != -1) {
					var visible = molmil_snps["ligands"][id[2]]["visible"];
					if (visible)
						$("#"+$(this).attr('id')).removeClass("active");
					else
						$("#"+$(this).attr('id')).addClass("active"); 
					for (var i=0; i<molmil_snps["ligands"][id[2]]["list"].length; i++) {
						var ligand = molmil_snps["ligands"][id[2]]["list"][i];
						var mol = canvas.molmilViewer.structures[0].chains[ligand["chain"]].molecules[ligand["mol"]];
						if (visible)
							molmil.displayEntry(mol, molmil.displayMode_None, false, canvas.molmilViewer);
						else {
							mol.display = 1;
							for (var j=0; j<mol.atoms.length; j++)
								mol.atoms[j].display = 1;
						}
						molmil_snps["ligands"][id[2]]["visible"] = !visible;
					}
				}				
			}
			canvas.renderer.initBuffers(surface_type);
			canvas.update = true;
		}

		else if (id[1] == "SNPs") {
			var snp = molmil_snps["snps"][id[2]];
			var obj = canvas.molmilViewer.structures[0].chains[snp["chain"]].molecules[snp["mol"]];
			if (snp["visible"]) {
				if (molmil_snps["locus"][snp["locus"]]["color"]) molmil.colorEntry(obj, molmil.colorEntry_Custom, molmil_snps["locus"][snp["locus"]]["color"], false, molmilViewer);
				else molmil.colorEntry(obj, molmil.colorEntry_Structure, null, false, molmilViewer);
				if (molmil_snps["locus"][snp["locus"]]["style"] == "CartoonRocket") {
					if (! obj.ligand && ! obj.water) {for (a=0; a<obj.atoms.length; a++) obj.atoms[a].displayMode = 0;}
					obj.displayMode = 31;
					obj.showSC = false;
				}
				else molmil.displayEntry(obj, eval("molmil.displayMode_"+molmil_snps["locus"][snp["locus"]]["style"]), false, canvas.molmilViewer);
				snp["visible"] = false;
				canvas.renderer.initBuffers(surface_type);
				canvas.update = true;

				$("#"+$(this).attr('id')).val("S");
				$("#"+$(this).attr('id')).removeClass("active");
				var label = canvas.molmilViewer.texturedBillBoards[snp["label_no"]];
				label.display = false; 
				label.status = false;
				//document.getElementById(snp["button_id"].replace('_first', "")+"_label").style.background = colorScheme["inactive"];

				if (molmil_snps["currSel"] == obj.atoms) molmil_snps["currSel"]=[];
				molmil_snps["locus"][snp["locus"]]["visible"] = false;
			}
			else {
				obj.rgba = snp_rgba;
				for (a=0; a<obj.atoms.length; a++) {
					obj.atoms[a].rgba = snp_rgba;
					obj.atoms[a].displayMode = 2;
				}
				obj.showSC = true;
				obj.chain.twoDcache = null;	
				snp["visible"] = snp["button_id"];
				canvas.renderer.initBuffers(surface_type);
				$("#"+$(this).attr('id')).val("H");
				$("#"+$(this).attr('id')).addClass("active");
				molmil_snps["currSel"] = obj.atoms;
				molmil_snps["locus"][snp["locus"]]["visible"] = true;
			}

			if (surface_type == true) {
				showSurfaceSticks(molmil_surface["current"]);
				showSurfaceSNPs(molmil_surface["current"]);
			}
			else if (surface_type)
				showSurfaceSNPs(molmil_surface["current"]);

			canvas.renderer.initBuffers(surface_type);
			canvas.update = true;
		}

		else if (id[1] == "Label") {
			var snp = molmil_snps["snps"][id[2]];
			var label = canvas.molmilViewer.texturedBillBoards[snp["label_no"]];
			var labelButtonsSelect = document.getElementsByClassName(id[2]+" resultButtonLabel");

			if (label.display) {
				label.display = false; 
				label.status = false;
				for (var j=0; j<labelButtonsSelect.length; j++) {
					labelButtonsSelect[j].removeClass("active");
				}
				$("#"+$(this).attr('id')).removeClass("active");
			}
			else {
				label.display = true; 
				label.status = false;
				for (var j=0; j<labelButtonsSelect.length; j++) {
					labelButtonsSelect[j].className += " active";
				}
				$("#"+$(this).attr('id')).addClass("active");
			}
			canvas.update = true;
		}

		else if (id[1] == "Zoom on") {
			var snp = molmil_snps["snps"][id[2]];
			var zoomButtonsSelect = document.getElementsByClassName("resultButtonZoom");
			
			if (!(this.className.match(/(?:^|\s)active(?!\S)/))) {
				if (snp["visible"]) {
					var mol = obj.chains[snp["chain"]].molecules[snp["mol"]];
					atoms = mol.atoms;
					molmil.selectAtoms(atoms, false, canvas.molmilViewer);  
					canvas.molmilViewer.gotoMol(mol);
					molmil_snps["currSel"] = atoms;
				}
				var elements = document.getElementsByClassName("SNPTableButton");
				for (var i = 0; i < elements.length; i++) {
					if (elements[i].id.split("_")[1] == "Zoom on")
						elements[i].removeClass("active");
				}
				for (var j=0; j<zoomButtonsSelect.length; j++) {
					if (zoomButtonsSelect[j].classList.contains(id[2]))
						zoomButtonsSelect[j].className += " active";
					else
						zoomButtonsSelect[j].removeClass("active");
				}
				$("#"+$(this).attr('id')).removeClass("active");
				this.className += " active";
				
				
				//molmil.selectionFocus(canvas.molmilViewer); 
				canvas.renderer.initBuffers(surface_type);
				canvas.update = true;

				molmil.selectAtoms(molmil_snps["currSel"], null, canvas.molmilViewer); 

				canvas.renderer.initBuffers(surface_type);
				canvas.update = true;
			}
			else {
				document.getElementById("a_Focus").click();
//				atoms = [];
//
//				for (var c=0; c<list.length; c++) {
//					atoms.push.apply(atoms,list[c].atoms);
//				}
				for (var j=0; j<zoomButtonsSelect.length; j++) {
					zoomButtonsSelect[j].removeClass("active");
				}
//
//				//document.getElementById(id[1]).classList.toggle("show");
//
//				//oldData = $('#draggable').html();

				this.removeClass("active");
			}

		}

		else if (id[1] == "Focus") {
			atoms = [];

			for (var c=0; c<list.length; c++) {
				atoms.push.apply(atoms,list[c].atoms);
			}

			molmil.selectAtoms(atoms, null, canvas.molmilViewer);   
			canvas.renderer.updateSelection();    
			molmil.selectionFocus(canvas.molmilViewer);
			molmil.selectAtoms(molmil_snps["currSel"], null, canvas.molmilViewer); 

			var elements = document.getElementsByClassName("SNPTableButton");
			for (var i = 0; i < elements.length; i++) {
				if (elements[i].id.split("_")[1] == "Zoom on")
					elements[i].removeClass("active");
			}

			canvas.renderer.initBuffers(surface_type);
			canvas.update = true;
		}

		else if (id[1] == "Save as") {
			canvas.molmilViewer.UI.savePNG()
		}
	}
}
);


//on hoover actions
$('body').on({
	mouseenter: function() {
		var id = $(this).attr('id').split("_");
		var obj = canvas.molmilViewer.structures[0];

		//oldData = $('#draggable').html();

		var uinput = $('#main_input').val();

		if (id[0] == "SNPbuttonBS" | id[0] == "draggableTable" | id[0] == "SNPtable" | id[0] == "BStable") {
			if (id[0] == "SNPbuttonmain") {
				var snp = molmil_snps["snps"][id[3]];  			
			}
			else var snp = molmil_snps["snps"][id[2]];

			var mol = obj.chains[snp["chain"]].molecules[snp["mol"]]||[];
			atoms = mol.atoms;
			molmil.selectAtoms(atoms, null, canvas.molmilViewer);
			canvas.renderer.updateSelection(snp_rgba_hover);
			canvas.update = true;
		}

		//else if (id[1] == "Color" | id[1] == "Style" | id[1] == "Surface") 
		//$("#"+id[1]+" li a").css({"background-color": ""});
	},
	mouseleave: function() {

		//$('#draggable').html(oldData);

		var id = $(this).attr('id').split("_");
		if (id[0] == "SNPbuttonBS" | id[0] == "draggableTable" | id[0] == "SNPtable" | id[0] == "BStable") {
			molmil.selectAtoms(molmil_snps["currSel"], null, canvas.molmilViewer);
			canvas.renderer.updateSelection();
			canvas.update = true;
		}
	}
}, '.SNPButton, .draggableTableCell, .SNPtableRow');

function toggleCheckbox() {

	var allLabels = canvas.molmilViewer.texturedBillBoards;
	var link = document.getElementById("themeCSS");

	if (molmil.configBox.BGCOLOR[0] == "1") {
		molmil.configBox.BGCOLOR = [0,0,0,1];

		for (var i=0; i<allLabels.length; i++) {
			allLabels[i].settings.color = [255, 255, 0];
		}

		link.setAttribute("href", "css/coverBlack.css"); //Change its href attribute
	}
	else {
		molmil.configBox.BGCOLOR = [1,1,1,1];

		for (var i=0; i<allLabels.length; i++) {
			allLabels[i].settings.color = [0,0,0];
		}

		link.setAttribute("href", "css/coverWhite.css");
	}
	canvas.update = true;
}

//changing tables in binding side view
$('body').on('click', '.BS-tab .tab-links a', function(e)  {
	var currentAttrValue = $(this).attr('href');
	
	var siblings = $(this).parent('li').addClass('active').siblings();
	
	for (var i=0; i<siblings.length; i++) {
		if ((siblings[i].className).indexOf("active") >= 0)
			attrValue = siblings[i].getAttribute("href");
	}
	
	if (attrValue != currentAttrValue) {

//	Show/Hide Tabs
	$("[id='"+currentAttrValue.substr(1)+"']").fadeIn(400).siblings().hide();

	// Change/remove current tab to active
	$(this).parent('li').addClass('active').siblings().removeClass('active');
	$("[id='"+currentAttrValue.substr(1)+"']").addClass('active').siblings().removeClass('active');

	if ((["ion","nucleic","compound","protein","noinfo"]).indexOf(currentAttrValue.split("-")[2]) != -1) {
		if (surface_type)
			$("#legendChangeBuffer").html("<img src='slike/"+currentAttrValue.split("-")[2]+".png' class = 'instructionsImg'></img> Binding Site Surface <span style='color:"+colorScheme.surfaceToColorWord[currentAttrValue.split("-")[2]]+"'>("+colorScheme.surfaceToColorWord[currentAttrValue.split("-")[2]]+" surface)</span>");
		var ligHrefType = document.querySelectorAll("a[href='#lig"+currentAttrValue.substring(3)+"']");
		ligHrefType[0].click();
		($($($("[id='"+currentAttrValue.substr(1)+"']").children()[0]).children()[0]).children()[0]).click();
	}
	else {
		restartDsearch("searchBS", "BSSNPTab"+currentAttrValue.substr(1));
		initDsearch("searchBS", "BSSNPTab"+currentAttrValue.substr(1));
		var ligHref = document.querySelectorAll("a[href='#ligands "+(currentAttrValue.substring(1))+"']");
		ligHref[0].click();
		
		hideSurfaceSticks(molmil_surface["current"]);
		hideSurfaceSNPs(molmil_surface["current"]);

		for (var key in molmil_surface) {
			if (key != "current")
				molmil_surface[key]["visible"] = false;
		}
		for (var key in molmil_surface["current"]) {
			molmil_surface["current"][key] = {};
			molmil_surface["current"][key][currentAttrValue.substr(1)] = molmil_surface[currentAttrValue.substr(1)]["dictionary"][key];
			molmil_surface["current"][key][currentAttrValue.substr(1)+"_SNP"] = {};
			molmil_surface["current"][key][currentAttrValue.substr(1)+"_SNP"] = molmil_surface[currentAttrValue.substr(1)+"_SNP"]["dictionary"][key];
		}
		molmil_surface[currentAttrValue.substr(1)]["visible"] = true;
		molmil_surface[currentAttrValue.substr(1)+"_SNP"]["visible"] = true;

		try {
			for (var i=0; i<global_buffers.length; i++) {
				if ("#"+(global_buffers[i][0]).replace(/\$/g, ' ').replace("_","") == currentAttrValue) {
					global_buffer = global_buffers[i][1];
					break;
				}
			}
		}
		catch(e) {
			for (var i=0; i<global_buffers.length; i++) {
				if ("#Binding Site #"+global_buffers[i]["bs_id"]+"_"+global_buffers[i]["ltype"] == currentAttrValue) {
					global_buffer = global_buffers[i]["surf"];
					break;
				}
			}
		}
		

		if (surface_type == true) {
			showSurfaceSticks(molmil_surface["current"]);
			showSurfaceSNPs(molmil_surface["current"]);
		}
		else if (surface_type)
			showSurfaceSNPs(molmil_surface["current"]);

		canvas.renderer.initBuffers(surface_type);
		canvas.update = true;
	}
	}
	e.preventDefault();
});

//switching between surface types
$('body').on('click', '.btn-bs', function(e)  {
	var currentAttrValue = $(this).attr('value');
	var currentClasses = $(this).attr('class');

	if (currentClasses.indexOf("active") != -1) {
		$(this).removeClass('active');
		if (surface_type == true)
			hideSurfaceSticks(molmil_surface["current"]);

		hideSurfaceSNPs(molmil_surface["current"]);
		surface_type = false;
	}
	else {
		// Change/remove current tab to active
		$(this).addClass('active').siblings().removeClass('active');

		if (currentAttrValue == "Sticks") {
			surface_type = true;
			for (var key in molmil_surface)
				if (key != "current") molmil_surface[key]["visible"] = true;
			showSurfaceSticks(molmil_surface["current"]);
			showSurfaceSNPs(molmil_surface["current"]);

			$("#legendChangeSticks").css("display","");
			$("#legendChangeBuffer").css("display","none");
		}
		else if (currentAttrValue == "Surface") {
			for (var key in molmil_surface)
				if (key != "current") molmil_surface[key]["visible"] = false;
			surface_type = "detailed"; 

			//canvas.molmilViewer.structures[1].display = false;
			hideSurfaceSticks(molmil_surface["current"]);
			showSurfaceSNPs(molmil_surface["current"]);

			$("#legendChangeBuffer").css("display","");
			$("#legendChangeSticks").css("display","none");
		}
	}

	canvas.renderer.initBuffers(surface_type);
	canvas.update = true;

	e.preventDefault();
});

$('body').on('click', '.lig-tab .tab-links a', function(e)  {

	var currentAttrValue = $(this).attr('href');
	
//Show/Hide Tabs
	$("[id='"+currentAttrValue.substr(1)+"']").fadeIn(400).siblings().hide();

	// Change/remove current tab to active
	$(this).parent('li').addClass('active').siblings().removeClass('active');
	$("[id='"+currentAttrValue.substr(1)+"']").addClass('active').siblings().removeClass('active');

	if ((["ion","nucleic","compound","protein","noinfo"]).indexOf(currentAttrValue.split("-")[2]) != -1) {
		var bsHref = document.querySelectorAll("a[href='#BS"+currentAttrValue.substring(4)+"']");
		bsHref[0].click();
		($($($("[id='"+currentAttrValue.substr(1)+"']").children()[0]).children()[0]).children()[0]).click();
	}
	else {
		restartDsearch("searchLig", "LigSNPTab"+currentAttrValue.replace("#ligands ",""));
		initDsearch("searchLig", "LigSNPTab"+currentAttrValue.replace("#ligands ",""));
		var bsHref = document.querySelectorAll("a[href='"+currentAttrValue.replace("ligands ","")+"']");
		bsHref[0].click();

		canvas.molmilViewer.structures = [canvas.molmilViewer.structures[0]];

		var buttonsAll = document.getElementsByClassName("selectAllLigandsButton");

		for (var i=0; i<buttonsAll.length; i++) {
			buttonsAll[i].removeClass("active");
			buttonsAll[i].value = "S";
		}

		var buttons = document.getElementsByClassName("resultButtonLigands");

		for (var i=0; i<buttons.length; i++) {
			buttons[i].removeClass("active");
			buttons[i].value = "S";
		}

		if ($("#draggableLigandPlayer").css("display") != "none") {
			hideLigand("ligand");

			var ligLen = 0;

			for (var i=0; i<ligandObject.length; i++) {
				if ("Binding Site #"+(ligandObject[i]).bs_id+"_"+(ligandObject[i]).ltype == currentAttrValue.replace("#ligands ",""))
					ligLen += 1;
			}

			for (var i=0; i<ligandObject.length; i++) {
				if ("Binding Site #"+(ligandObject[i]).bs_id+"_"+(ligandObject[i]).ltype == currentAttrValue.replace("#ligands ","")) {
					showLigand(i,"ligand","(1/"+ligLen+")");
					break;
				}
			}
			$("#ligandPlayer_PLAY").removeClass("active");
			ligandPlayerPlaying = false;
		}

		canvas.renderer.initBuffers(surface_type);
		canvas.update = true;
	}

	e.preventDefault();
});

$('body').on('click', '#ligandPlayerList li a', function(e)  {
	var currentId = $(this).attr('id').split("_")[1];
	var bsLigands = $('.lig-tab .active > a').attr('href');
	bsLigands = bsLigands.substring(1);
	bsLigands = $('#'+bsLigands+' .active > a').attr('href');
	bsLigands = bsLigands.replace("#ligands ","");

	if (currentId == "BW") {

		hideLigand("ligand");
		console.log(ligandObject.length);
		console.log(currentLigandIndex);
		for (var i=ligandObject.length-1; i>-1; i = i-1) {
			ii = (i + currentLigandIndex) % ligandObject.length;
			console.log(ii);
			if ("Binding Site #"+(ligandObject[ii]).bs_id+"_"+(ligandObject[ii]).ltype == bsLigands) {
				showLigand(ii,"ligand",calcNewLigNo(false));
				break;
			}
		}

	}
	else if (currentId == "PLAY") {

	function delayedLoop() {
		setTimeout( function() {
			if (ligandPlayerPlaying) {
				$('#ligandPlayer_FW').click();
				delayedLoop();
			}
		},1000);
	}
		

		if (!this.classList.contains("active")) {
			ligandPlayerPlaying = true;

			$(this).addClass("active");

			var i=0;

			delayedLoop(i);
		}
		else {
			ligandPlayerPlaying = false;

			$(this).removeClass("active");
		}


	}
	else if (currentId == "STOP") {
		$("#ligandPlayer_PLAY").removeClass("active");
		ligandPlayerPlaying = false;

		hideLigand("ligand");

		for (var i=0; i<ligandObject.length; i++) {
			if ("Binding Site #"+(ligandObject[i]).bs_id+"_"+(ligandObject[i]).ltype == bsLigands) {
				showLigand(i,"ligand",calcNewLigNo("restart"));
				break;
			}
		}

	}
	else if (currentId == "FW") {

		hideLigand("ligand");

		for (var i=0; i<ligandObject.length; i++) {
			ii = (i + currentLigandIndex+1) % ligandObject.length;
			if ("Binding Site #"+(ligandObject[ii]).bs_id+"_"+(ligandObject[ii]).ltype == bsLigands) {
				showLigand(ii,"ligand",calcNewLigNo(true));
				break;
			}
		}

	}

	canvas.renderer.initBuffers(surface_type); 	
	canvas.update = true;

});

function saveAsPng() {
	canvas.molmilViewer.UI.savePNG()
}

