# How to install node:

Simply run the:

```
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```

and 

```
nvm install node
```

as the user for which the node will be run.